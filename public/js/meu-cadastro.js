digito9('#celular',{
    'ddd':'#dddcel',
    'tipotel':null
});

$('#pedidos > tbody > tr').hover(function(){
    $(this).addClass('over');
},function(){
    $(this).removeClass('over');
});

// validação do form
if(CONTROLLER=='cadastro'){
    $('#senha, #senhac').data('validate',true).data('errmsg','Preencha a senha corretamente');
}

$('#meus-dados .bt-red-whole a').click(function(){
    validadeForm('#meus-dados form');
});

// mostra/esconde campo de bairro
/*if($.trim($('input#bairro').val()) == ''){
    $('input#bairro').parents('li').hide();
} else {
    $('select#local_id').val('__none__');
}*/

// mostra/esconde campo de bairro com base no combo
$('select#local_id').change(function(){
    if($(this).val() == '__none__'){
        $(this).blur();
        $('input#bairro').parents('li').show().find('input#bairro').focus();
    } else {
        $('input#bairro').val('').parents('li').hide();
    }
});

// paginação
$('.pagination a').each(function(){
    var $t = $(this);
    $t.attr('href',$t.attr('href')+'#meus-pedidos');
});

head.js(JS_PATH+'/Cep.js',function(){
    $('#cep').blur(function(){
        Cep.search($(this).val());
    });
});

// if(APPLICATION_ENV=='development') TEST.formCadastro();



