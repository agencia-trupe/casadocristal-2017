SwitchBg = function(elm1,elm2){
    var img = typeof(elm1)=='string' ? elm1 :
              SwitchBg.getBgImg(elm1.css("background-image")),
        params = SwitchBg.getBgParams(elm2.css("background-image")),
        newImg = img+"?"+params;
    
    elm2.css("background-image",newImg);
}

SwitchBg.getBgImg = function(css){
    return css.split("?")[0];
}
SwitchBg.getBgParams = function(css){
    return css.split("?")[1];
}

SwitchBg.preload = function(elm,params,tagimg){
    tagimg = tagimg || false;
    
    if(typeof(elm.size) != 'undefined'){
        if(elm.size() > 1){
            for(var i = 0;i<elm.size();i++){
                SwitchBg.preload(elm[i],params,tagimg);
            }
            return;
        }
    }
    
    elm = $(elm);
    var img = new Image(),
        src = tagimg ?
              (elm.hasAttr('src') ? elm.attr('src') : elm.data('src')) :
              SwitchBg.getBgImg(SwitchBg.getUrl(elm.css('background-image')));
    img.src = src+((params) ? '?'+params : '');
}

SwitchBg.getUrl = function(input){
    return input.replace(/"/g,"").replace(/url\(|\)$/ig, "");
}