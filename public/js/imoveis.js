head.js(JS_PATH+'/jquery.bxslider.min.js',function(){
	// vitrine do imóvel
	$('#imovel-slideshow').bxSlider({
		mode: 'vertical',
		minSlides: 3,
		maxSlides: 3,
		slideHeight: 113,
		slideMargin: 0
	});

	$('#imovel-slideshow .imovel a').live('click',function(e){
		e.preventDefault();

		$('.imovel-foto img').attr('src',$(this).attr('href'));

		return false;
	});

	// sugestões de imóveis
	$('#imoveis').bxSlider({
		minSlides: 3,
		maxSlides: 4,
		slideWidth: 220,
		slideMargin: 40
	});

	_btShadow($('.bx-prev').addClass('bt-shadow').addClass('bt-shadow-r').html('&laquo;'));
	_btShadow($('.bx-next').addClass('bt-shadow').html('&raquo;'));
	
	$('#imovel-fotos .bx-next').addClass('bt-shadow-r');
});