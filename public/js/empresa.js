/*head.js(JS_PATH+'/jquery.scrollTo-min.js',function(){
	var filters = $('.filter > ul > li > a'),
		subfilters = $('.subfilter > ul > li > a'),
		isFiltered = false;

	filters.click(function(e){
		var $this = $(this);

		filters.removeClass('active');
		$this.addClass('active');

		$.scrollTo($($this.attr('href')),1000,{
			axis:'y'
			// ,margin:true
			,offset: {top:-120,left:0}
			// ,over:-0.5
		});
	});
});*/

var hasPush = supports_history_api(), isPushed = false;

var filters = $('.filter > ul > li > a'),
	pages = $('.empresa-page');

filters.click(function(e){
	var $this = $(this),
		filter = $this.attr('href').split('#')[1].replace('empresa-','');

	filters.removeClass('active');
	$this.addClass('active');

	_show($this.attr('href'));

	var filterType = 'page', psData = {};
		psData[filterType] = filter;
	_push(psData,filterType,'/empresa/'+filter);

	/*pages.slideOut('fast',function(){
		$($this.attr('href')).slideIn('slow');
	});*/

	/*pages.fadeOut(200,function(){
		$($this.attr('href')).delay(200).fadeIn('slow');
	});*/

	/*pages.animate({'height':0},1500,function(){
		$($this.attr('href')).animate({'height':100%},1500);
	});*/
});

// filtro inicial
if(location.href != URL+'/'+CONTROLLER){
	var params = location.href.replace(URL+'/'+CONTROLLER+'/','').split('/'),
		filter = params[0];

	_show('#empresa-'+filter);
	filters.filter('.empresa-'+filter).addClass('active');
}

function _show(elm){
	pages.removeClass('show');
	$(elm).addClass('show');
}

function supports_history_api() {
	return !!(window.history && history.pushState);
}

function _push(data,name,url){
	if(hasPush){
		history.pushState(data,name,url);
		isPushed = true;
	}
}