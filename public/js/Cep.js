Cep = {
    clean: function(cep){
        return cep.replace(/[a-zA-Z_\-]/g,"");
    },
    search: function(cep){
        cep = Cep.clean(cep);
        if(cep.length > 0){
            if(cep.length == 8){
                var campos = [
                    "#logradouro",
                    "#bairro",
                    "#cidade",
                    "#numero"
                ];
				var uf = document.getElementById('uf');
				
                $(campos.join()).addClass("load").val("Aguarde...").attr('disabled',true);
				if(uf.tagName == "SELECT"){
					uf.options[0].selected = true;
				} else {
					$("#"+uf.getAttribute('id')).addClass("load").val("Aguarde...").attr('disabled',true);
				}
				
                $.getJSON(URL+"/ajax/cep/busca.json?cep="+cep,function(data){
                    if(data.err == 1){
                        $(campos.join()).val("");
                        alert("CEP não encontrado, preencha os dados manualmente.")
                    } else {
                        $(campos[0]).val(URLDecode(data.end));
                        $(campos[1]).val(URLDecode(data.bai));
                        $(campos[2]).val(URLDecode(data.cid));

                        // atualiza combo de locais de entrega
                        var locais = $('#local_id > option'), hasLocal = false;

                        for(i in locais){
                        	if(locais[i].text == data.bai){
                        		$(locais[i]).attr('selected','selected');
                        		$('#bairro').val('').parents('li').hide();
                        		hasLocal = true;
                        	}
                        }

                        if(!hasLocal){
                        	$('#local_id > option[value="__none__"]').attr('selected','selected');
                        	$('#bairro').parents('li').show();
                        }
                        // console.log(hasLocal);
                    }
                    $(campos.join()).removeClass("load").attr('disabled',false);
                    
					if(uf.tagName == "SELECT"){
						//for(var x in uf.options){
						for(var x=0;x < uf.options.length;x++){
							if(data.est == uf.options[x].text){
								uf.options[x].selected = true;
							}
						}
					} else {
						$("#"+uf.getAttribute('id')).removeClass("load").val(data.est).attr('disabled',false);
					}

					$(campos[3]).val("").focus();
                });
            } else {
                alert("Preencha o cep corretamente.");
            }
        }
    }
    
}

function URLDecode(url){ //function decode URL
	// Replace + with ' '
	// Replace %xx with equivalent character
	// Put [ERROR] in output if %xx is invalid.
	var HEXCHARS = "0123456789ABCDEFabcdef";
	var encoded = url;
	var plaintext = "";
	var i = 0;
	while (i < encoded.length) {
		var ch = encoded.charAt(i);
		if (ch == "+") {
			plaintext += " ";
			i++;
		} else if (ch == "%") {
			if (i < (encoded.length-2)
			&& HEXCHARS.indexOf(encoded.charAt(i+1)) != -1
			&& HEXCHARS.indexOf(encoded.charAt(i+2)) != -1 ) {
				plaintext += unescape( encoded.substr(i,3) );
				i += 3;
			} else {
				alert( 'Bad escape combination near ...' + encoded.substr(i) );
				plaintext += "%[ERROR]";
				i++;
			}
		} else {
			plaintext += ch;
			i++;
		}
	} // while

	return plaintext;
}
