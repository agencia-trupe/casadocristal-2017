head.js(JS_PATH+"/cycle.min.js",function(){
	$('#slideshow').before('<ul id="banner-nav">').cycle({
		fx:'fade',
		speed:2000,
		// speedIn:4000,
		// speedOut:100,
		pager:"#banner-nav"
	});
	$('#noticias-slideshow').cycle({
		fx:'fade',
		speed:10000,
		pause:1
	});
});

// load_bxslider(function(){
// 	// últimas novidades
// 	$('#noticias-slideshow').bxSlider({
// 		mode: 'vertical',
// 		minSlides: 2,
// 		maxSlides: 2,
// 		slideWidth: 355,
// 		slideHeight: 185,
// 		slideMargin: 5,
// 		infiniteLoop: false
// 	});

// 	$('.bx-prev').attr('title','Anterior');
// 	$('.bx-next').attr('title','Próximo');
// });