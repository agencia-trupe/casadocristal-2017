head.js(JS_PATH+'/is.simple.validate.js',function(){
    // enviando cadastre-seu-imovel
    $('form.frm-cadastre-seu-imovel').submit(function(e){
        e.preventDefault();
        var $form = $(this),
            $status = $form.find('p.status').removeClass('error').html('');
        $status.html('Enviando...');
        
        if($form.isSimpleValidate()){
            var url  = URL+'/'+CONTROLLER+'/enviar.json',
                data = $form.serialize();
            
            $.post(url,data,function(json){
                if(json.error){
                    $status.addClass('error').html('* '+json.error);
                    return false;
                }
                
                $status.html(json.msg);
            },'json');
        } else {
            $status.addClass('error').html('* Preencha todos os campos corretamente');
        }
    });
});