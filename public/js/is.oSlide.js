(function ($) {
    var oSlideTO;
    var oSlide = {
        elm : undefined,

        scroll : function(s,t){
            var c = oSlide.elm.find('.slider-container'),
                m = (parseInt(c.css('margin-left'),10)-s),
                slideR = oSlide.elm.find('.slide-right').removeClass('disabled'),
                slideL = oSlide.elm.find('.slide-left').removeClass('disabled');
            
            c.css('margin-left',m+'px');
            
            if(parseInt(c.css('margin-left'),10) >= 0){
                c.css('margin-left','0px');
                slideL.addClass('disabled');
                oSlide.clear();
                return;
            }
            
            var m2 = (parseInt(oSlide.elm.find('.slider-container').css('width'),10)) -
                     (parseInt(oSlide.elm.find('.slider-wrapper').css('width'),10));
            if(parseInt(c.css('margin-left'),10) <= -m2 && s > 0){
                c.css('margin-left','-'+(m2+2)+'px');
                slideR.addClass('disabled');
                oSlide.clear();
                return;
            }
            //console.log($('.sobremesas .slider-container2').scrollLeft());
            
            oSlideTO = window.setTimeout(function(){
                oSlide.scroll(s,t||50);
            },t||50);
        },
        
        clear : function(){
            window.clearTimeout(oSlideTO);
        }
    }

    $.fn.oSlide = function(opt){
        var $this = $(this),
            opts = {
                'scroll':1,
                'speedMin':50,
                'speedMax':10
            };
        
        oSlide.elm = $this;
        var o = $.extend(opts,opt||{}); //console.log(o);

        $this.find('.slide-right').mouseover(function(){
            oSlide.scroll(o.scroll,o.speedMin);
        }).mouseout(function(){
            oSlide.clear();
        }).mousedown(function(){
            oSlide.scroll(o.scroll,o.speedMax);
        }).mouseup(function(){
            oSlide.clear();
        });

        $this.find('.slide-left').mouseover(function(){
            oSlide.scroll(-o.scroll,o.speedMin);
        }).mouseout(function(){
            oSlide.clear();
        }).mousedown(function(){
            oSlide.scroll(-o.scroll,o.speedMax);
        }).mouseup(function(){
            oSlide.clear();
        });
    }
})(jQuery);