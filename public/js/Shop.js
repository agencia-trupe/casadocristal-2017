// Shop Object
var Shop = {
    carrinho : null,
    valorTotal : 0,
    status : null,

    defaultOpt: {
        messageAddToCart: "Produto adicionado ao carrinho!",
        url: URL+"/meu-carrinho/",
        addUrl: URL+"/meu-carrinho/add.json",
        removeUrl: URL+"/meu-carrinho/remove.json",
        countUrl: URL+"/meu-carrinho/count-itens.json"
    },

    init : function(data){
        Shop.carrinho = $('.carrinho');
        Shop.status = Shop.carrinho.find('.status');

        Shop.carrinho.find('.finalizar').live('click',function(e){
            e.preventDefault();
            var $this = $(this);

            Shop.valorTotal = 1; //Number(Shop.carrinho.find('.valor-total').data('valor'));

            if(Shop.valorTotal > 0){
                if(!fancyboxLoaded){
                    $.getCss(CSS_PATH+'/fancybox/jquery.fancybox-1.3.4.css');
                    head.js(JS_PATH+'/jquery.fancybox-1.3.4.pack.js',function(){
                        $this.fancybox();
                    });
                } else {
                    $this.fancybox();
                }
            } else {
                alert('Nenhum item adicionado para orçamento');
            }
        });
    },

    message : function(msg,error,delay){
        error = error || false;
        delay = delay || false;
        var delayTime = 3000;

        Shop.status.removeClass('error').stop().show().html(msg);

        if(error) Shop.status.addClass('error');
        if(delay) Shop.status.delay(delayTime).fadeOut();

        return Shop;
    },

    addToCart : function(data,callback){
        Shop.message('<i>Aguarde...</i>');

        var $this = this;
        
        $.getJSON($this.defaultOpt.url+'add.json',data,function(json){
            if(json.error){
                Shop.message(json.error,true);
                return false;
            }
            
            var html = '<div class="pedido" id="cart_'+data.id+'">'+
                        '<span class="bull">&bull;</span>'+
                        '<span class="title">'+
                        data.titulo+' - '+
                        data.formato+' &bull; '+
                        data.gramatura+' &bull; '+
                        data.qtde+' '+
                        '<a href="#" class="del" data-id="'+data.id+'">(excluir)</a>'+
                        '</span>'+
                        'Observações: '+data.obs+
                        '</div>';

            $(html).appendTo('#pedidos');
            Shop.count();
            // Shop.total();

            // Shop.message(json.msg,false,true);
            Shop.countItems(1,true);

            if(typeof(callback)=='function') callback();
        });
    },

    removeFromCart : function(id,callback){
        if(confirm('Deseja remover o item selecionado?')){
            var $this = this,
                data = {
                    'id':id
                };
            Shop.message('<i>Aguarde...</i>');

            $.getJSON($this.defaultOpt.url+'remove.json',data,function(json){               
                if(json.error){
                    Shop.message(json.error,true);
                    return false;
                }

                $('#cart_'+id).fadeOut(function(){
                    $(this).remove();
                    Shop.count();
                    // Shop.total();

                    Shop.message(json.msg,false,true);

                    if(typeof(callback)=='function') callback();
                });
            });
            return $this;
        }
    },

    total : function(){
        var total = 0.00;
        
        $('.carrinho table tbody tr').each(function(i,v){
            total = parseFloat(total) + parseFloat($(this).data('valor'));
        });

        $('.carrinho .valor-total').html(currency(total.toFixed(2))).data('valor',total);
    },

    count : function(){
        var n = Shop.carrinho.find('.n'), 
            n2 = $('.top-aguardando > .n'), 
            c = 0;

        c = $('#pedidos > .pedido').size();

        if(c < 10) c = '0'+String(c);

        n.html(String(c));
        n2.html(String(c));

        return c;
    },

    countItems: function(count,noUpdate){
        var $counter = $('#cart-counter'),
            val = parseFloat($.trim($counter.text()));
        // console.log(val);
        val = val > 0 ? val : 0;
        
        var add = val === 0 && noUpdate;
        //if(val === 0 && noUpdate){ return this; }
        
        val+= count;
        if(val < 10) val = '0'+val;
        // console.log(val);
        $counter.html(add ? ''+val+'' : val);
        return this;
    }
}

Shop.init();