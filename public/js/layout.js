var pf_arr = [], fancyboxLoaded = false, bxsliderLoaded = false;

$(document).ready(function(){
    if(APPLICATION_ENV == 'development') head.js(JS_PATH+'/less.js',function(){
        // less.watch();
        intLess = window.setInterval(function(){
            localStorage.clear();
            less.refresh();
        },1500);
    }); // load less
    if(!$.browser.msie && !html5SupportTag('header')) head.js(JS_PATH+'/html5.js'); // load html5 compatibility
    if(APPLICATION_ENV=='development') head.js(JS_PATH+'/tests.js');
    
    // máscaras padrões
    $("input.mask-cpf").mask("999.999.999-99");
    $("input.mask-cep").mask("99999-999");
    $("input.mask-data,input.mask-date").mask("99/99/9999");
    $("input.mask-tel").mask("(99)9999-9999");
    $("input.mask-cel").mask("(99)9?9999-9999");
    $("input.mask-ddd").mask("99");
    $("input.mask-tel-no-ddd").mask("9999-9999");
    $("input.mask-currency").maskCurrency();
    _onlyNumbers('.mask-int');
    
    $(".prevalue").each(function(){ $(this).prevalue(); }); // prevalue em campos com a class .prevalue
    
    $("a[href^='#']").live("click",function(e){ // remove click de links vazios (que começam por #)
        e.preventDefault();
    });
    $("a.js-back").live("click",function(e){ // adiciona ação de voltar a links com a classe js-back
        history.back();
    });

    // ativa menus
    var menus_ativar = {
        '/news/' : 3,
        '/portfolio/' : 2
    };
    for(i in menus_ativar){
        if(location.href.indexOf(i)!=-1){
            var $li = $('#menu nav ul li').eq(menus_ativar[i]);
            $li.addClass('active').find('a').addClass('active');
        }
    }

    $('#mobile-menu').click(function(e){
        $('#menu-top').toggleClass('show');
    });
    $('#menu-top').parents().click(function(e){
        var isNotOpener = ['menu-lines','menu-text','menu-top','mobile-menu']
                          .indexOf(e.target.id)==-1;
        if(isNotOpener) $('#menu-top').removeClass('show');
    });
    
    // arruma links paginação
    if($(".pagination").size()){ // se tiver paginação
        $(".pagination a:not(.navigation)").addClass('link');
        $(".pagination span").addClass('etc');
        $(".pagination .navigation.left").html("&laquo;").attr('title','Página anterior').removeClass('link'); // arruma texto da navegação
        $(".pagination .navigation.right").html("&raquo;").attr('title','Próxima página').removeClass('link');
        if($(".pagination a").size() <= 1){
            $(".pagination").hide(); // se tiver somente 1 ou nenhuma página esconde a div
        }
    }
    if($(".paginacao").size()){ // se tiver paginação
        if($(".paginacao a").size() >= 100){
            $(".paginacao").hide(); // se tiver somente 1 ou nenhuma página esconde a div
        }
    }

    if(SCRIPT) head.js(JS_PATH+'/'+SCRIPT+'.js'); // carrega scripts de páginas
    
    // newsletter
    $('.newsletter-form').submit(function(e){
        e.preventDefault();
        var $form = $(this), $status = $form.find('.status').removeClass('error').html('Enviando...');
        
        $.post(URL+'/index/newsletter.json',$form.serialize(),function(json){
            if(json.error) $status.addClass('error');
            $status.html(json.message);
        },'json');
    });
    $('.newsletter-form input.newsletter-telefone').focus(function(){
        $(this).removeClass('pre');
    }).blur(function(){
        var $t = $(this);
        if($t.val() == ''){
            $t.addClass('pre');
        }
    });
    
    // flash-messages
    if($("#flash-messages").size()){
        var $flash = $("#flash-messages");
        var y = (document.documentElement.scrollTop) ? document.documentElement.scrollTop : document.body.scrollTop;
        $flash.css({top:y+10}).delay(1000).show().click(function(){
            $(this).fadeOut("slow");
        });
    }

    // form busca
    $('#frm-busca-top').submit(function(e){
        e.preventDefault();

        var $this = $(this),
            $busca = $this.find('#busca'),
            busca = $.trim($busca.val());

        if(busca.length < 4 || busca == $busca.attr('placeholder')){
            alert('Digite algo para buscar');
            $busca.focus();
        } else {
            window.location = URL+'/busca/'+busca;
        }

        return false;
    });

    // slider produtos em destaque
    if(document.getElementById('produtos-em-destaque')){
        load_bxslider(function(){
            $('#produtos-slideshow').bxSlider({
                minSlides: 4,
                maxSlides: 4,
                slideWidth: 165,
                slideHeight: 230,
                slideMargin: 15,
                infiniteLoop: false
            });
        });
    }

    // form login
    $('.menu-user-login').toggle(function(){
        $(this).addClass('active');
        $('#frm-login-top').slideDown('fast');
        $('#login_email').focus();
    },function(){
        $(this).removeClass('active');
        $('#frm-login-top').slideUp('fast');
    })

    // ordenação de produtos
    $('#produto_ordem').change(function(){
        var url = window.location.href;
        url = url.indexOf('?') == -1 ? url : url.substring(0,url.indexOf('?'));
        url+= '?ordem='+$(this).val();

        // console.log(url);
        window.location = url;
    });

    // compartilhar por email
    if(document.getElementById('share-mail')){
        $.getCss(CSS_PATH+'/fancybox/jquery.fancybox-1.3.4.css');
        head.js(JS_PATH+'/jquery.fancybox-1.3.4.pack.js',function(){
            $('#share-mail').fancybox({
                'titleShow':false
                ,'centerOnScroll':true
                ,'padding':0
            });
        });

        $('#frm-share-mail').submit(function(e){
            e.preventDefault();

            var $this = $(this),
                url = $this.attr('action'),
                // data = $this.serialize(),
                status = $this.find('.status').removeClass('error').html(''),
                submit = $this.find('.submit').attr('disabled',true),
                body = $('#mail_body');

            if(body.val() == body.data('prevalue')) body.val('');
            var data = $this.serialize();

            if(simpleValidate($this.find('.txt.required'))){
                status.html('<i>Aguarde...</i>');

                $.post(url,data,function(json){
                    // console.log([json,data,url]);
                    submit.attr('disabled',false);

                    if(json.error){
                        status.addClass('error').html(json.error);
                        return false;
                    }

                    status.html(json.msg);
                },'json');
            } else {
                status.addClass('error').html('Preencha os campos corretamente');
                submit.attr('disabled',false);
            }

            return false;
        });
    }

    // compartilhar por twitter / facebook
    if(document.getElementById('share-twitter')){
        $('#share-twitter,#share-facebook').live('click',function(e){
            e.preventDefault();

            var width  = 535, 
                height = 450,
                top    = (screen.height / 2) - (height / 2),
                left   = (screen.width / 2) - (width / 2);
            
            window.open($(this).attr('href'),'share','width='+width+',height='+height+',top='+top+',left='+left+',menubar=no,resizable=no,status=no,titlebar=no,toolbar=no');
        });
    }

    // scroll to
    var $scrollTos = $('a.scroll-to');
    if($scrollTos.length){
        head.js(JS_PATH+'/jquery.scrollTo-min.js',function(){
            $scrollTos.live('click',function(e){
                var $this = $(this);
                $this.attr('href',$this.attr('href').replace(URL,''));
                $.scrollTo($($this.attr('href')),500,{axis:'y'});
            });
        });
    }

    // controllers / actions
    switch(CONTROLLER){
        case 'produtos':
            
            break;
    }

    // old IE's hacks
    if($.browser.msie && (parseInt($.browser.version) < 9)){
        
    }
    if($.browser.msie && (parseInt($.browser.version) >= 9)){
        
    }

    // checando features
    if(!head.placeholder){
        var $elm;

        $('[placeholder]').each(function(){
            $elm = $(this);

            $elm.addClass('prevalue')
                .data('prevalue',$elm.attr('placeholder'))
                .prevalue(); 
        });
    }
});

// Color Hex <-> RGB
function hexToR(h){return parseInt((cutHex(h)).substring(0,2),16);}
function hexToG(h){return parseInt((cutHex(h)).substring(2,4),16);}
function hexToB(h){return parseInt((cutHex(h)).substring(4,6),16);}
function cutHex(h){return (h.charAt(0)=="#") ? h.substring(1,7):h;}
function hexToRGB(h){return {'R':hexToR(h),'G':hexToG(h),'B':hexToB(h)};}
function RGBToHex(r,g,b){var dec = r+256*g+65536*b;return dec.toString(16);}

// Math
function getRand(n){n=n||0;return Math.floor(Math.random()*(n+1));}

// ValidateForm
function validadeForm(f){var $f=$(f),err=0;$f.find("input").each(function(i,v){if($(this).data("validate")&&$.trim($(this).val())==""){alert($(this).data("errmsg"));$(this).focus();err++;return false}});if(err==0){$f.submit()}};

// nl2br
function nl2br(str,is_xhtml){var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '' : '<br>';return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');}

// Currency
function currency(val,s){s=s||false;return (!s?'R$ ':'')+(val+'').replace('.',',');}

function oldIE(){
    return $.browser.msie && (parseInt($.browser.version) < 9);
    // return false;
}

function getVideoUrl(code,type){
    var ifr = {
        'vimeo':function(c){return 'http://player.vimeo.com/video/'+c+'?title=0&byline=0&portrait=0&color=9c9797&autoplay=1';},
        'youtube':function(c){return 'http://www.youtube.com/embed/'+c+'?autoplay=1';}
    };

    return ifr[type](code);
}

function simpleValidate(inputs){
    var valid = true;

    inputs.each(function(){
        var $v = $(this), v = $.trim($v.val());

        if(v=='' || v == $v.data('prevalue')){
            $v.focus();
            valid = false;
            return false;
        }
    });

    return valid;
}

function _log(v){
    if(APPLICATION_ENV=='development') console.log(v);
}

// head.js features
head.feature('placeholder', function() {
    var t = document.createElement('textarea'); // #=> <textarea></textarea>
    return (t.placeholder !== undefined); // #=> true || false
});

/* onlyNumbers */
function _onlyNumbers(selector){
    $(selector).keypress(function(e){
        return (e.which >= 48 && e.which <= 57) || ($.inArray(e.which,[0,13,8]) != -1);
    });
}

// loaders
function load_bxslider(callback){
    if(bxsliderLoaded){
        (callback)();
    } else {
        head.js(JS_PATH+'/jquery.bxslider.min.js',function(){
            bxsliderLoaded = true;
            (callback)();
        });
    }
}

// valida campo vazio
function isEmpty(elm) {
    var v = $.trim(elm.val());

    return v=='' || v==elm.data('prevalue') || v=='__none__';
}

// 9 digitos
function digito9(elm,options){
    var opts = {
            'premask' : false,
            'ddd'     : 'input.mask-ddd',
            'tipotel' : 'select.tipotel'
        },
        opts2 = options || {},
        $this = $(elm),
                dddEmpty = ['','__','ddd: *','ddd','ddd:'],
                // ddd's e tipos que precisam de 9 dig
                ddd9digit = ['11'],
                tp9digit  = ['cel'];

    var opt = $.extend(opts,opts2);
    
    var dddField      = $(opt.ddd),
        ddd           = dddField.val(),
        tipotelField  = opt.tipotel ? $(opt.tipotel) : null,
        tipotel       = tipotelField ? tipotelField.val() : 'cel';
    
    // função que checa se precisa adicionar o dig
    var has9digit = function(){
        if($.inArray(dddField.val(),ddd9digit)!=-1){
            if(tipotelField){
                if($.inArray(tipotelField.val(),tp9digit)!=-1){
                    return true;
                }
            } else {
                if($.inArray(tipotel,tp9digit)!=-1){
                    return true;
                }
            }
        }
            
        return false;
    };

    if(opt.premask) $this.mask(has9digit()?'99999-9999':'9999-9999');

    $this.live('focus',function(){
            // console.log([$.trim(dddField.val().toLowerCase()),opt.tipotel]);
            $this.unmask();

            if($.inArray($.trim(dddField.val().toLowerCase()),dddEmpty)!=-1) {
                    $this.blur();
                    dddField.focus();
                    alert('Digite seu DDD');
                    return false;
            }

            $this.mask(has9digit()?'99999-9999':'9999-9999');
    });
}