head.js(JS_PATH+'/jquery.bxslider.min.js',function(){
	// linha do tempo
	$('#linhadotempo-slide').bxSlider({
		minSlides: 3,
		maxSlides: 4,
		slideWidth: 210,
		slideMargin: 40,
		infiniteLoop: false
	});

	_btShadow($('.bx-prev').addClass('bt-shadow').addClass('bt-shadow-r').html('&laquo;'));
	_btShadow($('.bx-next').addClass('bt-shadow').html('&raquo;'));
});