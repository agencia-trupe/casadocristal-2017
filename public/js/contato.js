head.js(JS_PATH+'/is.simple.validate.js',function(){
    $('#submit-frm-contato').click(function(e){
        $('#frm-contato').submit();
    });

    // enviando contato
    $('#frm-contato').submit(function(e){
        e.preventDefault();
        var $form = $(this),
            $status = $form.find('p.status').removeClass('error').html('');
        $status.html('Enviando...');
        
        if($form.isSimpleValidate()){
            var url  = URL+'/'+CONTROLLER+'/enviar.json',
                data = $form.serialize();
            
            $.post(url,data,function(json){
                if(json.error){
                    $status.addClass('error').html('* '+json.error);
                    return false;
                }
                
                $status.html(json.msg);
            },'json');
        } else {
            $status.addClass('error').html('* Preencha todos os campos corretamente');
        }
    });
});

$('#frm-contato .status').live('click',function(e){
    $(this).fadeOut('slow',function(){
        $(this).html('').removeClass('error').show();
    });
});

// habilitando zoom no mapa ao clicar
$('#mapa').click(function () {
    $('#mapa iframe').css("pointer-events", "auto");
});

$( "#mapa" ).mouseleave(function() {
    $('#mapa iframe').css("pointer-events", "none"); 
});