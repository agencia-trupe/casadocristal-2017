// var MAX_FOTOS = 6, MAX_SIZE = 5242880, allow_photos=false;

$(document).ready(function(){
    $("#search-by").change(function(){
        if($(this).val() == "role"){
            var $combo = $('<select></select>');
            $combo.attr({
                'name'  : 'search-txt',
                'id'    : 'search-txt',
                'style' : 'width: 180px;'
            }).addClass('txt');
            
            if(roles){
                for(i in roles){
                    $combo.append('<option value="'+i+'">'+roles[i]+'</option>');
                };
            }
            
            $("#search-txt").replaceWith($combo);
        } else if($("#search-txt").attr("type").indexOf('select') != -1){
            var $combo = $('<input/>');
            $combo.attr('name','search-txt').attr('id','search-txt').addClass('txt');
            $("#search-txt").replaceWith($combo);
        }
    });
});