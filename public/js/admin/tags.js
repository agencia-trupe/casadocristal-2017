$(document).ready(function(){
    for(i in TAGS){
        TAGS[i].value = TAGS[i].tag;
        TAGS[i].alias = TAGS[i].alias;
    }
    //console.log(SUGESTOES);
    $("#tags-search").prevalue("Adicionar...").autocomplete({
        source:TAGS,
        select:function(event,ui){
            if(ui.item){
                $("#tags-id").val(ui.item.id);
            }
        }
    });
    
    $("#tags-submit").click(function(e){
        e.preventDefault();
        var sugestao_id = $("#tags-id").val().length > 0 ? $("#tags-id").val() : false;
        
        // if(sugestao_id){
            var data = "portfolio_id="+$("#id").val()+"&tag_id="+sugestao_id;
            if(!sugestao_id) data+= "&tag_tag="+$("#tags-search").val();

            $.getJSON(URL+"/admin/portfolio/tags-add.json?"+data,function(json){
                if(!json.error){
                    var tmpl_data = {id:json.id,value:$("#tags-search").val()};
                    $("#tmplTags").tmpl(tmpl_data).appendTo(".tags-list");
                    $("#tags-search,#tags-id").val("");
                } else {
                    $("#tags-search,#tags-id").val("");
                    alert(json.error);
                }
            });
        // }
    });
    
    $(".tags-delete").live('click',function(){
        if(confirm('Deseja remover a tag?')){
            var $row = $(this).parent(),
                data = "portfolio_id="+$("#id").val()+"&tag_id="+$row.find(".tags-id").val();
            $.getJSON(URL+"/admin/portfolio/tags-del.json?"+data,function(json){
                if(!json.error){
                    $row.fadeOut("fast",function(){
                        $row.remove();
                    });
                } else {
                    alert(json.error);
                }
            });
        }
    });
});