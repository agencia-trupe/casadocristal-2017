// var MAX_FOTOS = 6, MAX_SIZE = 5242880, allow_photos=false;

$(document).ready(function(){
    $comboRoles = $('#form-new #role');

    // mostra oculda dados de usuário simples
    if($comboRoles.length > 0){
        $comboRoles.change(function(){
            if($comboRoles.val()=='2'){
                $('#form-new .data-user').slideDown();
            } else {
                $('#form-new .data-user').slideUp();
            }
        });

        var _tocr = window.setInterval(function(){
            if($comboRoles.val()=='2' && !$('#form-new .data-user').is(':visible')){
                // $('#form-new .data-user').show();
                $('#form-new .data-user').show();
                $('#form-new .data-user.edit').hide();
            } else {
                window.clearInterval(_tocr);
            }
        },200);
    }

    $('.escolaridades select').change(function(){
        var opt = $(this).find('option:selected');
        if(!$(this).hasClass('sub')) $('.escolaridades select.sub').hide();
        $('#escolaridades_'+opt.val()).show();
        $('#escolaridade_id').val(opt.val());

        if(opt.val()==9){
            $(this).blur();
            $('<input type="text" name="escolaridade_outros" id="escolaridade_outros" class="txt" />')
                .appendTo('.escolaridades-list').focus();
        } else {
            $('#escolaridade_outros').remove();
        }
    });

    $('.atividades ul li').each(function(i,v){
        var $this = $(this);

        if($this.has('ul').length){
            $this.find('ul').hide();
            $this.prepend('<span class="expand">&nbsp;&raquo;&nbsp;&nbsp;</span>')
            .find('.expand').click(function(){
                var ul = $(this).parent('li').find('ul:eq(0)');
                if(ul.is(':visible')) ul.slideUp();
                    else ul.slideDown();
            });
        } else {
            var id = $this.data('id'),
                html = '<input type="checkbox" class="cb" name="atividades[]" id="atividade_'+id+'" value="'+id+'" /> '+
                       '<label for="atividade_'+id+'" class="lb atividade-label">'+$this.html()+'</label>'+
                       (id==6?'<input type="text" name="atividade_profissional_outros" id="atividade_outros" class="txt" />':'');
            $this.html(html);
        }
    });

    $('.atividades input[type=checkbox]').click(function(e){
        var vals = [];

        $('.atividades input[type=checkbox]:checked').each(function(i,v){
            vals.push($(this).val());
        });

        $('#atividade_profissional_id').val(vals.join(','));
    });

    $('.expand-list').click(function(e){
        var sel = $(this).hasClass('e') ?
                    '.escolaridades-list,.escolaridade-completo':
                    '.atividades-list,.atividades-completo';

        if(!$(sel).is(':visible')) $(sel).slideDown();
            else $(sel).slideUp();
    });

    $('#uf').change(function(){
        var $this = $(this),
            cidade = $('#cidade'),
            url = URL+'/cadastro/cidades.json',
            data = {'uf':$this.val()};

        cidade.find('option').remove();
        cidade.append('<option value="">Aguarde...</option>');

        $.post(url,data,function(json){
            // console.log(json);
            if(json.error){
                alert(json.error);
                return false;
            }

            var options = '';

            for(i in json.cidades){
                options+= '<option value="'+i+'">'+json.cidades[i]+'</option>';
            }

            cidade.find('option').remove();
            cidade.append(options);
        },'json');
    });
});