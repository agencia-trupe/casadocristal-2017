$(document).ready(function(){
    $('.search-result table tr td a.ver').live('click',function(){
        var $this = $(this);
        $this.text($this.text() == 'Ocultar' ? 'Mostrar' : 'Ocultar');
        $('#detalhe_'+$this.data('id')).toggle();
    });
    
    $("#search-by").change(function(){
        if($(this).val() == "orcamento_status_id"){
            var $combo = $('<select></select>');
            $combo.attr('name','search-txt').attr('id','search-txt').addClass('txt');
            
            if(pedidos_status){
                for(i in pedidos_status){
                    $combo.append('<option value="'+i+'">'+pedidos_status[i]+'</option>');
                };
            }
            
            $("#search-txt").replaceWith($combo);
        // } else if($("#search-txt").attr("type").indexOf('select') != -1){
        } else if(document.getElementById('search-txt').type.indexOf('select') != -1){
            var $combo = $('<input/>');
            $combo.attr('name','search-txt').attr('id','search-txt').addClass('txt');
            $("#search-txt").replaceWith($combo);
        }
        if($(this).val() == "cliente_cpf"){
            $("#search-txt").mask('999.999.999-99');
        } else {
            $("#search-txt").unmask();
        }
    });
    
    if($('select#search-by').val() == 'cliente_cpf'){
        $('input#search-txt').mask('999.999.999-99');
    }
    
    $("input.salvar").click(function(e){
        e.preventDefault();
        
        var $this = $(this).attr('disabled',true).addClass('disabled'),
            id = $this.data('id'),
            rastreio = $('#rastreio_'+id),
            $status = $('#status_text_'+id).removeClass('error'),
            data = {
                'id':id,
                'status':$('#status_'+id).val(),
                'rastreio': isEmpty(rastreio) ? '' : rastreio.val(),
                'rastreio_mail': $('#chk_rastreio_mail_'+id).attr('checked') ? 1 : 0
            },
            items_id = [],
            items_valor = [],
            items_qtde = [];
        
        $status.html('Aguarde...');

        // adicionando valores do orçamento
        $('#table_detalhes_'+id).find('.item_valor').each(function(){
            var $this = $(this);

            items_id.push($this.data('produto-id'));
            items_valor.push($this.val().replace(',','.'));
            items_qtde.push(parseInt($('#item_qtde_'+id+'_'+$this.data('produto-id')).text()));
        });

        data.total = $('#total_'+id).text().replace(',','.');
        data.items_id = items_id;
        data.items_valor = items_valor;
        data.items_qtde = items_qtde;
        // return console.log(data);

        $.getJSON(URL+'/admin/acompanhamento/pedidos-salvar.json',data,function(json){
            if(data.status == '7' || data.status == '6') { // entregue
                $('#status_'+id).attr('disabled',true).addClass('disabled');
            } else {
                $this.attr('disabled',false).removeClass('disabled');
            }

            if(json.error){
                Msg.add(json.error,"erro").show(5000);
                // $status.addClass('error').html(json.error);
                $status.html('');
                alert(json.error);
                return false;
            }
            
            $('#row_status_'+$this.data('id')).html($('#status_'+$this.data('id')+' option:selected').text());
            Msg.add(json.msg).show(5000);
            $status.html(json.msg);
        });
    });

    $('.detalhes-status > form > select').change(function(e){
        var $this = $(this),
            rastreio_container = $this.parent('form').find('.rastreio-container');

        ($this.val()=='9') ? 
            rastreio_container.removeClass('hidden') : 
            rastreio_container.addClass('hidden');
    });

    $('.item_valor').blur(function(){
        var $this = $(this),
            pedido = $this.data('pedido-id'),
            produto = $this.data('produto-id'),
            $table = $('#table_detalhes_'+pedido),
            $qtde = $('#item_qtde_'+pedido+'_'+produto),
            $total = $('#item_total_'+pedido+'_'+produto),
            val = parseFloat($this.val().replace(',','.')),
            qtde = parseInt($qtde.text()),
            total = parseFloat($total.text().replace(',','.')),
            total_item = parseFloat(val * qtde).toFixed(2),
            total_pedido = 0;

        $total.text(currency(total_item,true));

        $table.find('.item_total').each(function(){
            total_pedido+= parseFloat($(this).text().replace(',','.'));
        });

        $('#total_'+pedido).text(currency(parseFloat(total_pedido).toFixed(2),true));
    }).keypress(function(e){
        var whichCode = (window.Event) ? e.which : e.keyCode;
        if(whichCode == 13) $(this).blur();
    });
});