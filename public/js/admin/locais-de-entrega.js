/*global $ */
var MsgRow = Messenger;
//MsgRow.container = $(".row-tools");
MsgRow.elm       = ".row-tools .row-status";

$(document).ready(function(){	
	$('.row-status_id').live('click',function(){
        var row = $(this).parent();
        row.find('input[name="status_id[]"]').val(($(this).is(':checked')?"1":"0"));
    });
    
    $(".row-list li").live("mouseover",function(){
		$(this).find("a.row-action").show();
	}).live("mouseout",function(){
		$(this).find("a.row-action").hide();
	});
	
	$(".row-add").click(function(){
		Rows.add();
	});
	$(".row-action-del").live('click',function(){
		Rows.del($(this).parent());
	});
    $(".frm-"+DIR+" .bt").click(function(){
        Rows.save();
    });
});

Rows = {
	add: function(){
		var newRow = $("#tmplRow").tmpl().appendTo(".row-list");
		//newRow.find('input').val("");
		newRow.find('input.row-descricao').focus();
	},
	
	del: function(row){
		if(confirm("Deseja remover o registro?")){
			var id = row.find(".row-id");
			
			if($.trim(id.val()).length > 0){
				var url = URL+"/admin/"+DIR+"/del.json?id="+id.val();
				$.getJSON(url,function(data){
					if(data.error){
						alert(data.error);
					} else {
						Rows.remove(row);
					}
				});
			} else {
				Rows.remove(row);
			}
		}
	},
	
	remove: function(row){
		row.remove();
		MsgRow.add("Registro removido.").show(null,3000);
	},
	
	save: function(){
		$('.frm-'+DIR).submit();
	}
}