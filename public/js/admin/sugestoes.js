$(document).ready(function(){
    for(i in SUGESTOES){
        SUGESTOES[i].value = SUGESTOES[i].titulo;
    }
    //console.log(SUGESTOES);
    $("#sugestoes-search").prevalue("Adicionar...").autocomplete({
        source:SUGESTOES,
        select:function(event,ui){
            if(ui.item){
                $("#sugestoes-id").val(ui.item.id);
            }
        }
    });
    
    $("#sugestoes-submit").click(function(e){
        e.preventDefault();
        var sugestao_id = $("#sugestoes-id").val().length > 0 ? $("#sugestoes-id").val() : false;
        
        if(sugestao_id){
            var data = "produto_id="+$("#id").val()+"&sugestao_id="+sugestao_id;
            $.getJSON(URL+"/admin/produtos/sugestoes-add.json?"+data,function(json){
                if(!json.error){
                    var tmpl_data = {id:sugestao_id,value:$("#sugestoes-search").val()};
                    $("#tmplSugestao").tmpl(tmpl_data).appendTo(".sugestoes-list");
                    $("#sugestoes-search,#sugestoes-id").val("");
                } else {
                    $("#sugestoes-search,#sugestoes-id").val("");
                    alert(json.error);
                }
            });
        }
    });
    
    $(".sugestao-delete").live('click',function(){
        if(confirm('Deseja remover a sugestão?')){
            var $row = $(this).parent(),
                data = "produto_id="+$("#id").val()+"&sugestao_id="+$row.find(".sugestao-id").val();
            $.getJSON(URL+"/admin/produtos/sugestoes-del.json?"+data,function(json){
                if(!json.error){
                    $row.fadeOut("fast",function(){
                        $row.remove();
                    });
                } else {
                    alert(json.error);
                }
            });
        }
    });
});