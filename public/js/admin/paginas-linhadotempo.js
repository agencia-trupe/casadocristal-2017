/*global $ */
var MsgLinha = Messenger;
//MsgLinha.container = $(".linha-tools");
MsgLinha.elm       = ".linha-tools .linha-status";

$(document).ready(function () {
	//$("input.mask-year").mask("9999");
	
	$(".linha-list li").live("mouseover",function(){
		$(this).find("a.linha-action").show();
	}).live("mouseout",function(){
		$(this).find("a.linha-action").hide();
	});
	
	$(".linha-add").click(function(){
		Linha.add();
	})
	$(".linha-action-del").live('click',function(){
		Linha.del($(this).parent());
	})
	$(".linha-action-save").live('click',function(){
		Linha.save($(this).parent());
	});
	$(".linha-ano").live("keydown",function(e){
        if(e.which == 13 || e.which == 27){
            e.preventDefault();
            var row = $(this).parents("li");
            if(e.which == 13){
                Linha.save(row);
            } else {
                $(this).val(row.find('.linha-ano-original').val()).blur();
            }
        }
    });
	$(".linha-descricao").live("keydown",function(e){
        if(e.which == 13 || e.which == 27){
            e.preventDefault();
            var row = $(this).parents("li");
            if(e.which == 13){
                Linha.save(row);
            } else {
                $(this).val(row.find('.linha-descricao-original').val()).blur();
            }
        }
    });
});

Linha = {
	add: function(){
		var newRow = $("#tmplLinha").tmpl({}).insertAfter(".linha-list .title");
		//$("input.mask-year").mask("9999");
		newRow.find('input.linha-ano').focus();
	},
	
	del: function(row){
		if(confirm("Deseja remover o destaque?")){
			var id        = row.find(".linha-id");
			
			if($.trim(id.val()).length > 0){
				var url = URL+"/admin/"+DIR+"/linhadotempo-del.json?id="+id.val();
				$.getJSON(url,function(data){
					if(data.error){
						alert(data.error);
					} else {
						Linha.remove(row);
					}
				});
			} else {
				Linha.remove(row);
			}
		}
	},
	
	remove: function(row){
		row.remove();
		MsgLinha.add("Destaque removido.").show(null,3000);
	},
	
	save: function(row){
		MsgLinha.add("Enviando...").show();
		
		var id          = row.find(".linha-id"),
			ano         = row.find(".linha-ano"),
			ano_o         = row.find(".linha-ano-original"),
			descricao   = row.find(".linha-descricao");
			descricao_o = row.find(".linha-descricao-original");
			
		var url = URL+"/admin/"+DIR+"/linhadotempo-save.json?id="+id.val()
				+"&ano="+ano.val()+"&descricao="+urlencode(descricao.val())+"&pagina_id="+$("#linha-pagina-id").val();
		
		ano.attr('disabled',true);
		descricao.attr('disabled',true);
		
		$.getJSON(url,function(data){
			if(data.error){
				alert(data.error);
			} else {
				ano_o.val(ano.val());
				descricao_o.val(descricao.val());
				MsgLinha.add("Destaque salvo com sucesso.").show(null,3000);
				ano.attr('disabled',false);
				descricao.attr('disabled',false);
			}
		});
	}
}