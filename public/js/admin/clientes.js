$(document).ready(function(){
    head.js(JS_PATH+'/meu-cadastro.js',function(){
        // Adiciona máscara ao campo de busca se for selecionado CPF
        if($('select#search-by').val() == 'cpf'){
            $('input#search-txt').mask('999.999.999-99');
        }
        
        $('select#search-by').change(function(){
            if($(this).val() == 'cpf'){
                $('input#search-txt').mask('999.999.999-99');
            } else {
                $('input#search-txt').unmask();
            }
        }); // máscara cpf
        
        $('.txt2').addClass('txt').removeClass('txt2'); // arruma classes
    });
}); // document.ready