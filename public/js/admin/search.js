$(document).ready(function(){
    $(".search-result table tbody tr").mouseenter(function(){
        $(this).addClass("over");
    }).mouseleave(function(){
        $(this).removeClass("over");
    });
    
    $(".search-result table tbody tr td a.del").click(function(e){
        var elm = $(this);
        Msg.reset();
        
        if(confirm("Deseja realmente excluir o registro?")){
            e.preventDefault();
            
            var url = $(this).attr("href").replace("del","del.json");
            $.getJSON(url,function(data){
                var err = data.erro || data.error;
                if(err){
                    Msg.add(err,"erro").show();
                } else {
                    Msg.add("Registro excluído com sucesso.","message").show();
                    elm.parents("tr").fadeOut("slow",function(){ $(this).remove(); });
                }
            });
        }
        return false;
    });
});