load_bxslider(function(){
	// fotos do produto
	$('#produto-slideshow1').bxSlider({
		minSlides: 3,
		maxSlides: 3,
		slideWidth: 94,
		slideMargin: 0,
		infiniteLoop: false
	});

	$('#produto-slideshow .produto a').live('click',function(e){
		e.preventDefault();

		var $this = $(this), $img = $('.produto-foto'), toi;

		$img.css('opacity','0.2');
		// $img.css('background-image','url('+$this.attr('href')+')').css('opacity','1');

		$.get($this.attr('href'),function(){
			// $img.css('background-image','url('+$this.attr('href')+')');
			$img.find('img').attr('src',$this.attr('href'));
			$img.css('opacity','1');
		});

		return false;
	});
});

// preload de imagens
$('#produto-slideshow .produto a').each(function(i,elm){
	var newImg = new Image();
	newImg.src = elm.href;
});

$('.produto:last-child .produto-link').addClass('last');

// adiciona produto ao orçamento
$('#solicitar-orcamento').click(function(){
	$(this).fadeOut('fast',function(){
		$('#fields-solicitar-orcamento').addClass('show');
		$('#qtde').focus();
	});
});

$('#frm-solicitar-orcamento').submit(function(e){
	e.preventDefault();
	return false;
});

head.js(JS_PATH+'/Shop.js',function(){ // load Shop module
	$('#submit-solicitar-orcamento').click(function(e){
		e.preventDefault();
		var $this = $(this), data = {}, abort = false;

		$this.parents('form').find('input[type=text],input[type=hidden],textarea,select').each(function(i,v){
			var $t = $(this), val = $.trim($t.val());

			if($t.data('validate') && (val == '' || val == $t.attr('placeholder'))){
				abort = true;
				alert($t.data('validate'));
				$t.focus();
				return false;
			} else if(val == $t.attr('placeholder')){
				val = '';
			}

			switch($t.attr('id')){
				case 'formato': 
					data['formato_id'] = val; 
					data['formato'] = $t.find('option:selected').text(); 
					break;
				case 'gramatura': 
					data['gramatura_id'] = val; 
					data['gramatura'] = $t.find('option:selected').text(); 
					break;
				default: 
					data[$t.attr('id')] = val;
			}
		});

		if(abort) return false;

		Shop.addToCart(data,function(){
			$('#fields-solicitar-orcamento').fadeOut('fast',function(){
				$('#status-solicitar-orcamento').fadeIn();
			});
		});

		return false;
	});

	$('#pedidos .del').live('click',function(e){
		Shop.removeFromCart($(this).data('id'),function(){
			if(Number(Shop.count()) == 0) {
				$('#frm-enviar-pedido .submit').addClass('disabled');
			}
		});
	});

	$('#pedido-popup .back').live('click',function(e){
		(CONTROLLER=='produto') ? $.fancybox.close() : location.href = URL;
	});

	$('#frm-enviar-pedido .submit').live('click',function(e){
		var $this = $(this);

		if(!$this.hasClass('disabled')){
			var form = $('#frm-enviar-pedido'),
				$this = $(this),
				inputs = form.find('input[type=text]'),
				status = form.find('.status').html('<i>Aguarde...</i>');

			form.submit();
			/*$.post(form.attr('action')+'.json',form.serialize(),function(json){
				if(json.error) {
					alert(json.error);
					return false;
				}

				status.html(json.msg);
				$this.addClass('disabled');
			},'json');*/
		}
	});
});