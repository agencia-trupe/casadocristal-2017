var hasPush = supports_history_api(), isPushed = false;

$('.portfolio').hover(function(){
	$(this).find('.desc').addClass('visible');
},function(){
	$(this).find('.desc').removeClass('visible');
});

// index
if(inIndex()){
	var filters = $('.filter > ul > li > a'),
		subfilters = $('.subfilter > ul > li > a'),
		isFiltered = false;

	filters.filter('.todos').addClass('active');

	filters.click(function(e){
		var $this = $(this),
			filter = $this.attr('href').split('#')[1];

		filters.removeClass('active');
		subfilters.removeClass('active');
		$this.addClass('active');

		$('.subfilter').hide();
		if(filter!='todos') $('.subfilter.'+filter).show();
	});

	subfilters.click(function(e){
		var $this = $(this),
			filter = $this.attr('href').split('#')[1];

		subfilters.removeClass('active');
		$this.addClass('active');
	});

	var $container = $('.portfolios');
	head.js(JS_PATH+'/jquery.isotope.min.js',function(){
		$container.isotope({
			// itemSelector : '.produto',
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			},
			masonry : {
				columnWidth : 172
			} 
		});

		subfilters.click(function(e){
			var $this = $(this),
				filter = $this.attr('href').split('#')[1];

			$container.isotope({filter:'.'+filter});
			isFiltered = filter;

			var filterType = $this.data('filter'), psData = {};
			psData[filterType] = filter;
			_push(psData,filterType,'/portfolio/'+filterType+'/'+filter);
		});

		$('#todos').live('click',function(){
			$container.isotope({filter:'.portfolio'});
			$('.portfolio').show();
			isFiltered = false;
			$('#busca').val('');
			_push({},'Todos','/portfolio');

			if(!isPushed) location.href = URL+'/portfolio';
		});

		// filtro inicial
		if(location.href != URL+'/'+CONTROLLER){
			var params = location.href.replace(URL+'/'+CONTROLLER+'/','').split('/'),
				filter = params[1],
				filterType = params[0];

			// filtrando (não filtra tags,busca)
			if($.inArray(filterType,['busca']) == -1){
				_log('filtrar '+filterType);
				$container.isotope({filter:'.'+filter});
				isFiltered = filter;

				var psData = {};
				psData[filterType] = filter;
				_push(psData,filterType,'/portfolio/'+filterType+'/'+filter);

				// ativando menus
				filters.filter('.todos').removeClass('active');
				filters.filter('.'+filterType).addClass('active');
				$('.subfilter.'+filterType).show();
				subfilters.filter('[href=#'+filter+']').addClass('active');
			}

			// filtrando busca
			if(filterType == 'busca'){
				var psData = {'busca':filter};
				_push(psData,'Busca','/portfolio/busca/'+psData.busca);

				_filter(pesquisa.selector,pesquisa.field,filter);
			}
		}
	});

	/*/ busca
	var b = $("#busca");
	$('.filter .search').submit(function(e){
	    e.preventDefault();
	    
	    if(b.val() == '' || b.val() == b.data('prevalue')){
	        alert('Escreva algo para buscar');
	    } else {
	        window.location = URL+'/portfolio/busca/'+b.val();
	    }
	    
	    return false;
	});*/

	function _filter(selector,field,query) {
		query = $.trim(query);
		//query = query.replace(/ /gi, '|'); //add OR for regex
		var hides = [], shows = [], idx = -1;

		$(selector).each(function() {
			idx = $.inArray('#'+$(this).attr('id'),hides);

			if(($(this).find(field).val() ? $(this).find(field).val().search(new RegExp(query, "i")) : $(this).find(field).text().search(new RegExp(query, "i"))) < 0){
				$(this).hide().removeClass('visible');//.addClass('invisible');
				if(idx!=-1) delete hides[idx];
			} else {
				$(this).show()/*.removeClass('invisible')*/.addClass('visible');
				hides.push('#'+$(this).attr('id'));
			}
		});

		if(hides.length > 0){
			var s = Boolean(isFiltered) ? 
					'.'+isFiltered+hides.join(',.'+isFiltered) :
					hides.join(',');
			$container.isotope({filter:s});
			$container.isotope('reLayout');
			//console.log(s);
		}
	}
	$('.filter form').submit(function(e){ e.preventDefault;return false; });
	var pesquisa = {
		selector:".portfolios .portfolio",
		field:".desc"
	};
	$('#busca').keyup(function(event) {
		if (event.keyCode == 27) $(this).val("").blur(); // Esc
		if(event.keyCode == 27 || $.trim(this.value) == ''){ //if press Esc or no text
			_push({},'Busca','/portfolio');

			$(pesquisa.selector).removeClass('visible').show().addClass('visible');
			var s = '.portfolio'+((Boolean(isFiltered)?'.'+isFiltered:''));
			$container.isotope({filter:s});
		} else { //if there is text, lets filter
			var psData = {'busca':$(this).val()};
			_push(psData,'Busca','/portfolio/busca/'+psData.busca);

			_filter(pesquisa.selector,pesquisa.field,$(this).val());
		}
		// $container.isotope('reLayout');
	});

} else { // detalhe

	if(document.getElementById('filmes')){
		if($('#slider li').length > 4){
			head.js(JS_PATH+'/jquery.bxSlider.min.js',function(){
				$('#slider').bxSlider({
					displaySlideQty:4,
					moveSlideQty:4,
					infiniteLoop:false
				});
			});
		}
	}
}

function supports_history_api() {
	return !!(window.history && history.pushState);
}

function _push(data,name,url){
	if(hasPush){
		history.pushState(data,name,url);
		isPushed = true;
	}
}

function inIndex(){
	var $main = $('.main');
	
	return $main.hasClass('main-portfolio-index') || 
		   $main.hasClass('main-portfolio-categoria') || 
		   $main.hasClass('main-portfolio-estilo') || 
		   $main.hasClass('main-portfolio-tag') || 
		   $main.hasClass('main-portfolio-busca');
}