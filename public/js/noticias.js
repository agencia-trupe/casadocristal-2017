if($('article .diario').hasClass('noticia')){
	$('.frm-comentario').submit(function(e){
		e.preventDefault();

		var form   = $(this),
			status = form.find('.status').removeClass('error').html('').show(),
			// url    = form.attr('action')+'.json';
			url    = [URL,CONTROLLER,'comentar.json'].join('/');
			// url    = [URL,'comentar',CONTROLLER+'.json'].join('/');

		form.find('.submit').attr('disabled',true);
		status.html('Enviando...');//alert(url);return;

		$.post(url,form.serialize(),function(json){
			form.find('.submit').attr('disabled',false);

			if(json.error){
				status.addClass('error')
					  .html(json.msg)
					  .click(function(){$(this).fadeOut();});
				return false;
			}

			status.html(json.msg).delay(3000).fadeOut();

			var date = new Date(),
				dt   = date.getHours()+'h'+date.getMinutes(),
				html = '<li>'+
					   '<span class="date">'+dt+'</span>'+
					   '<span class="title"><b>'+((USER)?USER.nome:'Anônimo')+'</b> disse:</span>'+
					   '<span class="comment">'+
					   form.find('#comentario').val()+
					   '</span>';

			$('li.no-comment').remove();
			$(html).hide().prependTo('.comentarios ul').slideDown('slow');
			form.find('#comentario').val('');
		},'json');

		return false;
	});

	if($('article .diario img').length > 0){
		$.getCss(CSS_PATH+'/fancybox/jquery.fancybox-1.3.4.css');
		head.js(JS_PATH+'/jquery.fancybox-1.3.4.pack.js',function(){
			$('article .diario img').each(function(){
				var $this = $(this);
				$this.wrap('<a href="'+$this.data('src')+'" class="fb" />');
			});

			$('article .diario a.fb').fancybox();
		});
	}
}