<?php
/**
 * Funções genéricas
 */
class Func
{
	public function img($img)
	{
		return "<img src=\"".ROOT_PATH.$img."\" />";
	}
	
	public function removeDecorators(&$elm)
    {
        $elm->removeDecorator('label')
			->removeDecorator('htmlTag')
            ->removeDecorator('description')
            ->removeDecorator('errors');
    }
	
	public function clean($val)
	{
		return str_replace(array('.',',',' ','-','_','(',')','[',']'),'',trim($val));
	}
	
	public function br($j)
	{
		$br = "";
		for($i=0;$i<=$j;$i++){
			$br.= "<br/>";
		}
		return $br;
	}
    
    public static function _toUtf8(&$item, $key) {
        $item = iconv("iso-8859-1","utf-8",$item);
    }
    
    public static function _toIso(&$item, $key) {
        $item = iconv("utf-8","iso-8859-1",$item);
    }
    
    public function arrayToObject($array)
    {
        $object = new stdClass();
        if (is_array($array)){
            foreach ($array as $name=>$value){
                $name = trim($name);
                if (!empty($name)){
                    $object->$name = $value;
                }
            }
        } else {
            //die("param is not an array.");
        }
        return $object;
    }
    
    public function objectToArray($object)
    {
        $array = array();
        if (is_object($object)){
            $array = get_object_vars($object);
        }
        return $array;
    }
    
    public static function _encodeUtf8(&$item,$key)
	{
		$item = utf8_encode($item);
	}
    
    public static function _decodeUtf8(&$item,$key)
	{
		$item = utf8_decode($item);
	}
    
    public static function _arrayToObject(&$item,$key)
	{
		$item = self::arrayToObject($item);
	}
    
    public function drop($name=null,$belong,$options,$value='',$class='f1')
	{
		array_walk($options,'Func::_encodeUtf8');
		$drop = new Zend_Form_Element_Select($belong,$name ? array('belongsTo' => $name) : array());
		$drop->setAttrib('class',$class)
			->addMultiOptions($options)
			->setValue($value);
		Func::removeDecorators($drop);
		return $drop;
	}
    
    public function chk_cb($val1,$val2)
    {
        return $val1 == $val2 ? 'checked="checked"' : '';
    }
    
    public function chk_drop($val1,$val2)
    {
        return $val1 == $val2 ? 'selected="selected"' : '';
    }
}

class Img
{
    public function bg($path,$preload=false)
    {
        $path = URL."/img/".str_replace("/","|",$path);
        $style = "style=\"background-image:url('$path');\"";
        
        if($preload){
            $img = 'img_'.rand(0,999999);
            $script = Js::script("var $img = new Image(); $img.src = '$path';");
            return $style.'>'.substr($script,0,strlen($script)-2);
        }
        
        return $style;
    }
    
    public function src($path,$attrs="")
    {
        return "<img src='".URL."/img/".str_replace("/","|",$path)."' ".$attrs." />";
    }

    public function base64($path)
    {
        $type = end(explode('.', $path));
        $data = file_get_contents($path);
        $base64 = 'data:image/'.$type.';base64,'.base64_encode($data);
        return $base64;
    }
}

class Image
{
    public function bg($path)
    {
        return "style=\"background-image:url('".CSS_PATH."/img/".$path."');\"";
    }
    
    public function src($path,$attrs="",$default_path=true)
    {
        $path = ($default_path ? IMG_PATH."/" : "").$path;
        return "<img src='".$path."' ".$attrs." />";
    }

    public function base64($path) { return Img::base64($path); }
}

class Menu extends ZendPlugin_Controller_Ajax
{
    public function load($name,$c=null)
    {
        $_menu_nav = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', $name);
        $menu_nav = $_menu_nav->toArray();
        $_name = 'menu_'.$name;
        if($c===null) $c = Zend_Controller_Front::getInstance();
        
        // adicionando compatibilidade com / e /SITE_NAME
        if(substr_count($_SERVER['SCRIPT_NAME'],'/') == 2 && count($menu_nav)){
            foreach($menu_nav as &$mnt) $mnt['uri'] = URL.$mnt['uri'];
        }
        
        $this->view->$_name = new stdClass();
        $this->view->$_name->top = new Zend_Navigation($menu_nav);
        
        $uri = APPLICATION_ENV == 'development' ?
                URL.$this->_request->getPathInfo() : // dev
                URL.$this->_request->getPathInfo();  // production
        
        foreach(get_object_vars($this->view->$_name) as $menu){
            $activeNav = $menu->findByUri($uri) or
            $activeNav = $menu->findByUri(str_replace('http://'.$_SERVER['HTTP_HOST'],'',$uri));
            
            if(null !== $activeNav){
                $activeNav->active = true;
                $activeNav->setClass("active"); 
            }
        }
    }
}

class Youtube
{
    public function checkStr($str)
    {
        $regex = "#youtu(be.com|.b)(/v/|/watch\\?v=|e/|/watch(.+)v=)(.{11})#";
        preg_match_all($regex , $str, $m);
        return $m;
    }
    
    public function hasVideo($str)
    {
        $s = self::checkStr($str);
        return count($s[0]) ? $s : false;
    }
    
    public function getText($str)
    {
        if(!$s = self::hasVideo($str)) return $str;
        $p = '/(.*)(http.*)/';
        preg_match($p,$str,$m);
        return $m[1];
    }
    
    public function getUrl($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[0][0];
    }
    
    public function getCode($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[4][0];
    }
    
    public function parseStr($str)
    {
        $o = new stdClass();
        $o->text = self::getText($str);
        $o->url  = self::getUrl($str);
        $o->code = self::getCode($str);
        return $o;
    }
}

class Vimeo
{
    public function checkStr($str)
    {
        // $regex = "#youtu(be.com|.b)(/v/|/watch\\?v=|e/|/watch(.+)v=)(.{11})#";
        $regex = "#vimeo.com/([0-9]*)#";
        preg_match_all($regex , $str, $m);
        return $m;
    }
    
    public function hasVideo($str)
    {
        $s = self::checkStr($str); //_d($s);
        return count($s[0]) ? $s : false;
    }
    
    public function getText($str)
    {
        if(!$s = self::hasVideo($str)) return $str;
        $p = '/(.*)(http.*)/';
        preg_match($p,$str,$m);
        return $m[1];
    }
    
    public function getUrl($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[0][0];
    }
    
    public function getCode($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[1][0];
    }
    
    public function parseStr($str)
    {
        $o = new stdClass();
        $o->text = self::getText($str);
        $o->url  = self::getUrl($str);
        $o->code = self::getCode($str);
        return $o;
    }
}

function _d($var,$exit=true){
    return Is_Var::dump($var,$exit);
}

function export_url($section,$ext)
{
    $url = explode('/'.$section,'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    return implode('/',array($url[0],$section,'export.'.$ext.$url[1]));
}

function cleanHtml($text){
    // $tags = '<b><strong><p><div><a><i><u><ul><ol><li><img><br><h1><h2><h3><h4><h5><h6><table><thead><tbody><th><tr><td><hr>';
    $tags = 'b,strong,p,div,a,i,u,ul,ol,li,img,br,h1,h2,h3,h4,h5,h6,'.
            'table,thead,tbody,th,tr,td,hr,strike';
    $tags = '<'.implode('><',explode(',',$tags)).'>';
    $text = stripslashes(($text));
    $text = (strip_tags($text,$tags));
    
    // replace contents
    $replaces = array(
        //'\<a(.*)\<\/a\>' => '<a target="_blank" rel="nofollow" \\1</a>',
        // '<([a-zA-Z0-9]*)(.*)(/?)(.*)>' => '<\\1 \\3>',
        // 'style\=\"(.*)\"' => ' ',
        '\<(\/)?div\>' => '<\\1p>',
        // '\<\/p\>(\<br(\s)?(\/)?\>)+\<p\>' => '</p><p>',
        '\<\/p\>(\<br(\s)?(\/)?\>)+\<p\>' => '<br>',
        '\<p\>([\s\n])?\<\/p\>' => '',
    );
    foreach($replaces as $p => $r) $text = ereg_replace($p,$r,$text);

    // import html
    Lib::import('phpQuery');
    $html = phpQuery::newDocumentHTML($text);

    // add attributes
    $adds = array(
        'a' => array('target'=>'_blank','rel'=>'nofollow')
    );
    foreach($adds as $p => $r) foreach($r as $k => $v) $html->find($p)->attr($k,$v);

    // clean styles
    $allow_styles = array(
        'text-align: center',
        'text-align: left',
        'text-align: right',
        'text-align: justify',
        'width: 100%'
    );
    
    foreach (explode('><',substr($tags,1,-1)) as $tag) {
        $items = $html->find($tag);
        
        foreach ($items as $item) {
            $style = array();

            foreach ($allow_styles as $as) {
                if(strstr(pq($item)->attr('style'), $as)) $style[] = $as;
            }

            (count($style)) ? 
                pq($item)->attr('style',implode(';',$style)) : 
                pq($item)->removeAttr('style');

            pq($item)->removeAttr('face');
            pq($item)->removeAttr('class');
        }
    }
    
    $html_new = $html->html();
    
    // se for texto plano (n houver <p> ou <div>) encapsula dentro de <p>
    if(!strstr($html_new, '<p') && !strstr($html_new, '<div'))
        $html_new = '<p>'.$html_new.'</p>';

    $html_new = preg_replace('/^(?!<)(.*)<.*/i', '<p>${1}</p>${2}', $html_new);

    return $html_new;
    // return $html->html();
    // return $text;
}

function parse_html($html, $remove_styles = array('p','ul','li'), $allow_tags='<b><a><i><u><br><ul><ol><li><img><p>')
{
    Lib::import('phpQuery');
    $body = strip_tags($html,$allow_tags);
    $html = phpQuery::newDocumentHTML($body);
    
    // retirando estilos extras de elementos
    foreach($remove_styles as $rs){
        $ps = $html->find($rs);
        foreach($ps as $p) pq($p)->attr('class','')->attr('align','')->attr('style','');
    }

    return $html->html();
}

function _currency($v,$view=null)
{
    return $view ? $view->currency($v) : number_format($v,2,',','.');
}

function addQueryString($qs)
{
    $url = FULL_URL;
    $url.= (strstr($url,'?') ? '&' : '?').$qs;
    return $url;
}

function _parseEndereco($dados)
{
    $endereco = $dados->endereco;
    $bairro = $dados->bairro;
    $cidade = $dados->cidade;
    $estado = $dados->estado;

    $endereco = str_replace(' cj ', ' ', $endereco);
    $endereco = reset(explode(' - ', $endereco));
    $bairro = reset(explode(' - ', $bairro));
    
    return implode(', ',array($endereco,$bairro,$cidade,$estado));
}