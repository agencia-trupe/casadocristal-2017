<?php

class Helper_Page
{
    public function title($title)
    {
        $titles = array(
			"imoveis" => "Imóveis",
		);
		
		return array_key_exists($title,$titles) ? $titles[$title] : ucwords($title);
    }
}
