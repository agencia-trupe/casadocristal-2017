<?php
class Project_Application_Controller_Ajax extends Zend_Controller_Action
{
    protected $_ajax_view = 'ajax.phtml';
    
    public function preDispatch()
    {
        if ($this->_request->isXmlHttpRequest() || isset($_GET['ajax'])){
            Zend_Controller_Action_HelperBroker::removeHelper('Layout');
            $this->getHelper('ViewRenderer')->setNoRender();
            return TRUE;
        }
    }
    
    public function postDispatch()
    {
        if ($this->_request->isXmlHttpRequest() || isset($_GET['ajax'])){
            $this->_getAjaxView() ? $this->renderScript($this->_getAjaxView()) : $this->_getAjaxData();
        }
    
        return TRUE;
    }
    
    public function ajaxJsonAction()
    {
        print $this->_toJson($this->_getAjaxData());
    }
    
    public function ajaxXmlAction()
    {
        print $this->_toXml($this->_getAjaxData());
    }
    
    public function _getAjaxData()
    {
        //
    }
    
    protected function _setAjaxView($view)
    {
        $this->_ajax_view = $view;
        return TRUE;
    }
    protected function _getAjaxView()
    {
        return $this->_ajax_view;
    }
    
    /**
     * Traduz resultado para o padrão UTF-8
     *
     * @param string $item :Valor a ser encodado
     */
    public static function _toUtf8(&$item, $key) {
        $item = iconv("iso-8859-1","utf-8",$item);
    }
    
    /**
     * Retorna dados em formato JSON
     *
     * @param array $arr :Array para ser transformada em JSON
     * @return json_array
     */
    public static function _toJson($arr){
        header('Content-type: application/json');
        return json_encode($arr);
    }
    
    /**
     * Retorna dados em formato XML
     *
     * @param array $data :Array para ser transformada em XML
     * @param string $rootNodeName :Nome do nó raiz do xml
     * @param string $xml :Nó para ser atualizado
     * @return xml
     */
    public static function _toXml($data,$rootNodeName='data',$xml=null){
		header ("Content-type: text/xml");
        if (ini_get('zend.ze1_compatibility_mode') == 1){
			ini_set ('zend.ze1_compatibility_mode', 0);
		}
		if ($xml == null){
			$xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");
		}
        
		foreach($data as $key => $value){
			if (is_numeric($key)){
				$key = "unknownNode_". strval($key);
			}
            
			$key = preg_replace('/[^a-z]/i', '', $key);
            
			if (is_array($value)){
				$node = $xml->addChild($key);
				self::_toXml($value, $rootNodeName, $node);
			} else {
				$value = htmlentities($value);
				$xml->addChild($key,$value);
			}
		}
		return $xml->asXML();
	}
}