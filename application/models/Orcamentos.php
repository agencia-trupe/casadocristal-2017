<?php

class Application_Model_Orcamentos
{
    public function __construct()
    {
        // models
        $this->cobranca = new Application_Model_Db_CobrancaPagSeguro(); // tabela de cobrança
        //$this->carrinho = new Application_Model_Db_Pedidos(); // tabela de carrinho
        $this->pedidos = new Application_Model_Db_Orcamentos(); // tabela de pedidos
        //$this->carrinho_itens = new Application_Model_Db_PedidosItems(); // tabela de itens do carrinho
        $this->pedidos_itens = new Application_Model_Db_OrcamentoItems(); // tabela de itens do pedido
        // $this->pedidos_taxas = new Application_Model_Db_OrcamentoTaxas(); // tabela de taxas do pedido
        $this->taxas = new Application_Model_Db_Taxas(); // tabela de taxas
        $this->produtos = new Application_Model_Db_Produtos(); // tabela de produtos
        $this->cores    = new Application_Model_Db_Cores(); // tabela de cores
        $this->clientes = new Application_Model_Db_Clientes(); // tabela de clientes
        $this->session  = new Zend_Session_Namespace(SITE_NAME."_cart"); // sessão que armazena os dados do carrinho
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
        $this->select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter()); // select
    }
    
    /**
     * Atualiza status do pedido
     */
    // public function updateStatus($id,$status,$rastreio=null,$rastreio_mail=false)
    public function updateStatus($id,$status,$dados=array())
    {
        $pedido = $this->pedidos->fetchRow('id='.$id);
        $pedido->orcamento_status_id = $status;
        
        // atualizando valores dos pedidos
        if(isset($dados['items_id']) && (bool)@$dados['items_id']){
            for($i=0; $i<sizeof($dados['items_id']); $i++){
                $produto_id = $dados['items_id'][$i];
                $valor = $dados['items_valor'][$i];

                $this->pedidos_itens->update(array(
                    'valor' => $valor
                ),'orcamento_id = '.$id.' and produto_id = '.$produto_id);
            }
        }

        // enviando email caso concluído ou cancelado
        if(in_array($status,array('3','4'))){
            $n = Application_Model_Carrinho::formatNumber($pedido->id);
            $cliente = $this->clientes->fetchRow('id='.$pedido->cliente_id);
            $url_pedido = URL.'/meu-cadastro';
            $url_contato = URL.'/contato';

            $sub = 'Orçamento '.($status=='3' ? 'concluído' : 'cancelado');
            $html = $status=='3' ?
                    '<p>Olá '.$cliente->nome.'!</p>'.
                    '<p>Seu orçamento de nº '.$n.' foi concluído.</p>'.
                    '<p>Acesse seu painel de controle para verificá-lo: <a href="'.$url_pedido.'">'.$url_pedido.'</a></p>'.
                    '<p><br />Atenciosamente, <br />'.SITE_TITLE.'</p>'
                    :
                    '<p>Olá '.$cliente->nome.'!</p>'.
                    '<p>Seu orçamento de nº '.$n.' foi cancelado.</p>'.
                    '<p>Para maiores informações acesse seu painel de controle: <a href="'.$url_pedido.'">'.$url_pedido.'</a> '.
                    'ou entre em contato conosco através de nosso site: <a href="'.$url_contato.'">'.$url_contato.'</a></p>'.
                    '<p><br />Atenciosamente, <br />'.SITE_TITLE.'</p>';

            try {
                Trupe_CinCin_Mail::send($cliente->email,$cliente->nome,$sub,$html);
            } catch(Exception $e){
                $err = 'Erro ao enviar e-mail ao cliente.';
                $err = APPLICATION_ENV!='production' ? $e->getMessage() : '';

                throw new Exception($err);
            }
        }
        
        return $pedido->save();
    }
    
    /**
     * Exclui um pedido
     */
    public function delete($id)
    {
        return $this->pedidos->delete('id='.$id);
    }
    
    /**
     * Retorna fetchAll com todos os Joins necessários
     *
     * @param string $where - cláusula where - permitido: id,cliente_nome,cliente_cpf,carrinho_status_id
     * @param string $order - ordenação
     * @param int    $limit
     * @param int    $offset
     * @param array  $joins - Joins / Propriedades que serão adicionados ao objeto de retorno
     * @example - opções disponíveis - array(
     *    'cliente', - dados do cliente
     *    'status', - dados de status do pedido
     *    'taxas', - taxas do pedido
     *    'items', - dados dos produtos
     *    'items_fotos', - fotos dos produtos
     * ) - Por padrão não é retornado items_fotos para minimizar o tempo de consulta
     *
     * @return object - Objeto contendo os dados do pedido e dos Joins requeridos
     */
    public function getAll($where=null,$order=null,$limit=null,$offset=null,$joins=array('cliente','status'))
    {
        $where_pedido = (bool)strstr($where,'pedido_id') ? str_replace('pedido_','',$where) : null;
        $where_pedido = $where_pedido ? $where_pedido : ((bool)strstr($where,'carrinho_status_id') ? str_replace('carrinho_status_id','carrinho_status_id',$where) : null);
        $where_cliente = (bool)strstr($where,'cliente_nome') ? str_replace('cliente_','',$where) : null;
        $where_cliente = $where_cliente ? $where_cliente : ((bool)strstr($where,'cliente_cpf') ? str_replace('cliente_','',$where) : null);
        
        if($where_cliente){
            $where = $where_cliente;
            
            if($rows = $this->clientes->fetchAll($where,$order,$limit,$offset)){
                $clientes = Is_Array::utf8DbResult($rows);
                $pedidos = array();
                
                for($i=0;$i<sizeof($rows);$i++){
                    $object = new stdClass();
                    $select = $this->pedidos->select()->order('id desc'); // select p/ ordenação na busca por referência
                    
                    if($_pedidos = $rows[$i]->findDependentRowset('Application_Model_Db_Orcamentos',null,$select)){
                        $object = Is_Array::utf8DbResult($_pedidos);
                        for($j=0;$j<sizeof($_pedidos);$j++){
                            $object[$j]->cliente = $clientes[$i];
                            
                            /*if($forma = $_pedidos[$j]->findDependentRowset('Application_Model_Db_PedidosFormas')){
                                $object[$j]->forma = Is_Array::utf8DbRow($forma->current());
                            }*/ 
                            
                            if(in_array('status',$joins)){
                                if($status = $_pedidos[$j]->findDependentRowset('Application_Model_Db_OrcamentoStatus')){
                                    $object[$j]->status = Is_Array::utf8DbRow($status->current());
                                }
                            }
                            
                            if(in_array('items',$joins)){
                                $object[$j]->count = 0;
                                $object[$j]->total = 0;
                                $object[$j]->items = array();
                                
                                if($items = $_pedidos[$j]->findDependentRowset('Application_Model_Db_OrcamentoItems')){
                                    foreach($items as $item){
                                        $p = new stdClass();
                                        $p->item = in_array('items_fotos',$joins) ?
                                            $this->produtos->getWithFotos($item->produto_id) :
                                            Is_Array::utf8DbRow($this->produtos->fetchRow('id = '.$item->produto_id));
                                        $p->count = $item->qtde;
                                        $p->cor_id = $item->produto_cor_id;
                                        $cor = $this->cores->fetchRow('id="'.$p->cor_id.'"');
                                        $p->cor = $cor ? Is_Array::utf8DbRow($cor) : null;

                                        if($p->item->categoria_id==VALEPRESENTEID) $p->item->valor = -$p->item->valor;
                                        
                                        $object[$j]->items[] = $p;
                                        $object[$j]->count+= $item->qtde;
                                    }
                                    
                                    foreach($object[$j]->items as $item){
                                        $object[$j]->total+= $item->item->valor * $item->count;
                                    }
                                }
                                
                                if(in_array('taxas',$joins)){
                                    $object[$j]->taxas = array();
                                    
                                    if($items = $_pedidos[$j]->findDependentRowset('Application_Model_Db_OrcamentoTaxas')){
                                        foreach($items as $item){
                                            $p = Is_Array::utf8DbRow($this->taxas->fetchRow('id = '.$item->taxa_id));
                                            
                                            $object[$j]->taxas[] = $p;
                                        }
                                        
                                        foreach($object[$j]->taxas as $item){
                                            $object[$j]->total+= $item->valor;
                                        }
                                    }
                                }
                            } else { 
                                $object[$j]->count = 0;
                                $object[$j]->total = 0;
                                $object[$j]->items = array();
                                
                                if($items = $_pedidos[$j]->findDependentRowset('Application_Model_Db_OrcamentoItems')){
                                    foreach($items as $item){
                                        $_p = $this->produtos->fetchRow('id='.$item->produto_id);

                                        $p = new stdClass();
                                        $p->item = new stdClass();
                                        $p->item->id = $item->produto_id;
                                        $p->item->descricao = utf8_encode($item->titulo);
                                        $p->item->valor = $item->valor;
                                        $p->item->peso = $_p->peso;
                                        $p->item->cod = $_p->cod;
                                        $p->count = $item->qtde;
                                        // $p->cor_id = $item->produto_cor_id;
                                        // $cor = $this->cores->fetchRow('id="'.$p->cor_id.'"');
                                        // $p->cor = $cor ? Is_Array::utf8DbRow($cor) : null;
                                        
                                        // if($_p->categoria_id==VALEPRESENTEID) $p->item->valor = -$p->item->valor;

                                        $object[$j]->items[] = $p;
                                        $object[$j]->count+= $item->qtde;
                                    }
                                    
                                    foreach($object[$j]->items as $item){
                                        $object[$j]->total+= $item->item->valor * $item->count;
                                    }
                                }
                                
                                $object[$j]->taxas = array();
                                
                                /*if($items = $_pedidos[$j]->findDependentRowset('Application_Model_Db_OrcamentoTaxas')){
                                    foreach($items as $item){
                                        $p = new stdClass();
                                        $p->id = $item->taxa_id;
                                        $p->descricao = utf8_encode($item->taxa_descricao);
                                        $p->valor = $item->taxa_valor;
                                        
                                        $object[$j]->taxas[] = $p;
                                    }
                                    
                                    foreach($object[$j]->taxas as $item){
                                        $object[$j]->total+= $item->valor;
                                    }
                                }*/
                            }
                            
                            array_push($pedidos,$object[$j]);
                        }
                    }
                }
                
                //array_push($pedidos,$object);
                //Is_Var::dump($pedidos);
                return $pedidos;
            }
        } else {
            $where = $where_pedido ? $where_pedido : $where;
            
            if($rows = $this->pedidos->fetchAll($where,$order,$limit,$offset)){
                $pedidos = Is_Array::utf8DbResult($rows);
                
                for($i=0;$i<sizeof($rows);$i++){
                    /*if($forma = $rows[$i]->findDependentRowset('Application_Model_Db_PedidosFormas')){
                        $pedidos[$i]->forma = Is_Array::utf8DbRow($forma->current());
                    }*/ 
                    
                    if(in_array('cliente',$joins)){
                        if($cliente = $rows[$i]->findDependentRowset('Application_Model_Db_Clientes')){
                            $pedidos[$i]->cliente = Is_Array::utf8DbRow($cliente->current());
                        }
                    }
                    if(in_array('status',$joins)){
                        if($status = $rows[$i]->findDependentRowset('Application_Model_Db_OrcamentoStatus')){
                            $pedidos[$i]->status = Is_Array::utf8DbRow($status->current());
                        }
                    }
                    if(in_array('items',$joins)){
                        $pedidos[$i]->count = 0;
                        $pedidos[$i]->total = 0;
                        $pedidos[$i]->items = array();
                        
                        if($items = $rows[$i]->findDependentRowset('Application_Model_Db_OrcamentoItems')){
                            foreach($items as $item){
                                $p = new stdClass();
                                $p->item = in_array('items_fotos',$joins) ?
                                    $this->produtos->getWithFotos($item->produto_id) :
                                    Is_Array::utf8DbRow($this->produtos->fetchRow('id = '.$item->produto_id));
                                $p->count = $item->qtde;
                                $p->cor_id = $item->produto_cor_id;
                                $cor = $this->cores->fetchRow('id="'.$p->cor_id.'"');
                                $p->cor = $cor ? Is_Array::utf8DbRow($cor) : null;

                                if($p->item->categoria_id==VALEPRESENTEID) $p->item->valor = -$p->item->valor;
                                
                                $pedidos[$i]->items[] = $p;
                                $pedidos[$i]->count+= $item->qtde;
                            }
                            
                            foreach($pedidos[$i]->items as $item){
                                $pedidos[$i]->total+= $item->item->valor * $item->count;
                            }
                        }
                        
                        if(in_array('taxas',$joins)){
                            $pedidos[$i]->taxas = array();
                            
                            if($items = $rows[$i]->findDependentRowset('Application_Model_Db_OrcamentoTaxas')){
                                foreach($items as $item){
                                    $p = Is_Array::utf8DbRow($this->taxas->fetchRow('id = '.$item->taxa_id));
                                    
                                    $pedidos[$i]->taxas[] = $p;
                                }
                                
                                foreach($pedidos[$i]->taxas as $item){
                                    $pedidos[$i]->total+= $item->valor;
                                }
                            }
                        }
                    } else {
                        $pedidos[$i]->count = 0;
                        $pedidos[$i]->total = 0;
                        $pedidos[$i]->items = array();
                        
                        if($items = $rows[$i]->findDependentRowset('Application_Model_Db_OrcamentoItems')){
                            foreach($items as $item){
                                $_p = $this->produtos->fetchRow('id="'.$item->produto_id.'"');
                                // if($_p->categoria_id==VALEPRESENTEID) $p->item->valor = -$p->item->valor;

                                $p = new stdClass();
                                $p->item = new stdClass();
                                $p->item->id = $item->produto_id;
                                $p->item->descricao = utf8_encode($item->titulo);
                                $p->item->valor = $item->valor;
                                $p->item->peso = $_p->peso;
                                $p->item->cod = $_p->cod;
                                $p->count = $item->qtde;
                                // $p->cor_id = $item->produto_cor_id;
                                // $cor = $this->cores->fetchRow('id="'.$p->cor_id.'"');
                                // $p->cor = $cor ? Is_Array::utf8DbRow($cor) : null;
                                $p->cor = null;
                                
                                $pedidos[$i]->items[] = $p;
                                $pedidos[$i]->count+= $item->qtde;
                            }
                            
                            foreach($pedidos[$i]->items as $item){
                                $pedidos[$i]->total+= $item->item->valor * $item->count;
                            }
                        }
                        
                        $pedidos[$i]->taxas = array();
                        
                        /*if($items = $rows[$i]->findDependentRowset('Application_Model_Db_OrcamentoTaxas')){
                            foreach($items as $item){
                                $p = new stdClass();
                                $p->id = $item->taxa_id;
                                $p->descricao = utf8_encode($item->taxa_descricao);
                                $p->valor = $item->taxa_valor;
                                
                                $pedidos[$i]->taxas[] = $p;
                            }
                            
                            foreach($pedidos[$i]->taxas as $item){
                                $pedidos[$i]->total+= $item->valor;
                            }
                        }*/
                    }
                }
                
                //_d($pedidos);
                return $pedidos;
            }
        }        
        
        return false;
    }
    
    /**
     * Retorna quantidade total
     * 
     * @param string $where - string de seleção where, padrão NULL
     *
     * @return int
     */
    public function count($where=null)
    {
        return $this->pedidos->count($where);
    }

    public function geraNumPedido($n)
    {
        return str_pad($n,6,'0',STR_PAD_LEFT);
    }
    
}

