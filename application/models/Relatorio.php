<?php

class Application_Model_Relatorio
{
    // $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
    // global $dbAdapter; $db = $dbAdapter;
    // $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    // $session = Zend_Session::getId();

    public $relatorio_tipos = array(
        '2' => 'Cadastros',
        '1' => 'Cruz. de variáveis',
        '3' => 'Visualizações',
        // '4' => 'Downloads', // implementar em seguida
        '5' => 'Mailling'
    );

    public $relatorio_padrao = '2';

    // lista de extensões que não devem entrar no relatório
    const url_deny = '.ico,.css,.less,.js,.json,.xml,.jpg,.jpeg,.png,.gif,.bmp,.swf,function(';

    public function __construct()
    {
        // $this->usuarios = new Application_Model_Db_Usuario();
        // $this->report = new Application_Model_Db_TrackAccess();
        // $this->report_downloads = new Application_Model_Db_TrackDownloads();
        $this->select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
    }

    public function setAccess($user,$url=null)
    {
        global $dbAdapter; $db = $dbAdapter;
        $url || $url = reset(explode('?','http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));

        $query = 'insert into track_access(url,user_id,session_id,data) values('.
                "'".$url."',".
                "'".$user."',".
                "'".Zend_Session::getId()."',".
                "'".date('Y-m-d H:i:s')."'"
            .') on duplicate key update counter = counter+1';

        try {
            if(self::allowUrl($url)) $db->query($query);
        } catch(Exception $e){
            if(APPLICATION_ENV=='development') _d($query.'<br>'.$e->getMessage());
        }
    }

    public function setDownload($data=array())
    {
        global $dbAdapter; $db = $dbAdapter;

        if(!isset($data['data'])) $data['data'] = date('Y-m-d H:i:s');
        if(!isset($data['session_id'])) $data['session_id'] = Zend_Session::getId();
        if(!isset($data['counter'])) $data['counter'] = 1;

        $into = array(); $values = array();
        foreach($data as $k => $v){
            $into[] = $k;
            $values[] = $db->quote($v);
        }

        $query = 'insert into track_downloads('.
                implode(',',$into)
            .') values('.
                implode(',',$values)
            .') on duplicate key update counter = counter+1';

        try {
            $db->query($query);
        } catch(Exception $e){
            if(APPLICATION_ENV=='development') _d($query.'<br>'.$e->getMessage());
        }
    }

    public function allowUrl($url)
    {
        $sufix = end(explode('.',$url)); // extensão da url
        $sufixes_deny = explode(',',self::url_deny);

        foreach($sufixes_deny as $d) if(strstr($url,$d)) return false;

        return true;
    }

    public function getTipo($tipo=null)
    {
        return $tipo ? $this->relatorio_tipos[$tipo] : $this->relatorio_tipos;
    }

    public function replaceUrl($url)
    {
        return str_replace(array(
            'http://'.URL,
            'http://territoriodobrincar',
            'http://www.territoriodobrincar.com.br',
            'http://territoriodobrincar.com.br'
        ), '', $url);
    }

    /* Reports */
    public function getReport($tipo=null,$params=array())
    {
        if($tipo===null) return array();

        global $dbAdapter; $db = $dbAdapter;
        $select = $this->select->reset();

        switch($tipo){
            case '1':
                if(!isset($params['filter']) &&
                   !isset($params['filter2']) &&
                   !isset($params['filter3']))
                    return array();

                $select->from('usuarios as u',array(
                        'date_format(u.data_cad,"%Y-%m-%d") as data',
                        'u.atividade_profissional_id',
                        'u.escolaridade_id',
                        'u.uf',
                        'u.cidade',
                        'u.sexo',
                        'u.filhos',
                        'u.interesse',
                        'count(u.id) as count_records'
                        )
                    )
                    ->where('u.role in (2)');
                
                if((bool)@$params['data_from'])
                    $select->where('u.data_cad >= '.$db->quote(Is_Date::br2am($params['data_from']).' 00:00:00'));
                
                if((bool)@$params['data_to'])
                    $select->where('u.data_cad <= '.$db->quote(Is_Date::br2am($params['data_to']).' 23:59:59'));

                $group = array();
                $order = array();

                if((bool)@$params['filter']){
                    switch($params['filter']){
                        case 'uf':
                            $group[] = 'u.uf';
                            $order[] = 'u.uf asc';
                            break;
                        case 'atividade_profissional_id':
                            $group[] = 'u.atividade_profissional_id';
                            $order[] = 'u.atividade_profissional_id asc';
                            break;
                    }
                }

                if((bool)@$params['filter2']){
                    switch($params['filter2']){
                        case 'atividade_profissional_id':
                            $group[] = 'u.atividade_profissional_id';
                            $order[] = 'u.atividade_profissional_id asc';
                            break;
                        case 'escolaridade_id':
                            $group[] = 'u.escolaridade_id';
                            $order[] = 'u.escolaridade_id asc';
                            break;
                        case 'sexo':
                            $group[] = 'u.sexo';
                            $order[] = 'u.sexo asc';
                            break;
                        case 'cidade':
                            $group[] = 'u.cidade';
                            $order[] = 'u.cidade asc';
                            break;
                        case 'interesse':
                            $group[] = 'u.interesse';
                            $order[] = 'u.interesse asc';
                            break;
                    }
                }

                if((bool)@$params['filter3']){
                    switch($params['filter3']){
                        case 'filhos':
                            $group[] = 'u.filhos';
                            $order[] = 'u.filhos desc';
                            break;
                    }
                }

                $select->group($group);
                $select->order($order);

                break;

            case '2':
                $select->from('usuarios as u',array(
                        'date_format(u.data_cad,"%Y-%m-%d") as data',
                        'u.atividade_profissional_id',
                        'u.escolaridade_id',
                        'count(u.id) as count_records'
                        )
                    )
                    ->where('u.role in (2)');
                
                if((bool)@$params['data_from'])
                    $select->where('u.data_cad >= '.$db->quote(Is_Date::br2am($params['data_from']).' 00:00:00'));
                
                if((bool)@$params['data_to'])
                    $select->where('u.data_cad <= '.$db->quote(Is_Date::br2am($params['data_to']).' 23:59:59'));

                
                if((bool)@$params['filter']){
                    if($params['filter']=='escolaridade_id'){
                        $select->group('u.escolaridade_id');
                        $select->order((bool)@$params['order']?$params['order']:array('u.escolaridade_id desc','u.escolaridade_completo desc'));
                    } else {
                        $select->group('atividade_profissional_id');
                        $select->order((bool)@$params['order']?$params['order']:'u.atividade_profissional_id desc');
                    }
                } else {
                    $select->group('data');
                    $select->order((bool)@$params['order']?$params['order']:'u.data_cad desc');
                }

                break;

            case '3':
                $select->from('track_access as ta',array('ta.url,ta.data,ta.session_id,ta.user_id,sum(ta.counter) as count_access'))
                    ->joinLeft('usuarios as u','u.id = ta.user_id',array(
                        'date_format(u.data_cad,"%Y-%m-%d") as data',
                        'u.nome',
                        'u.atividade_profissional_id',
                        'u.escolaridade_id',
                        'u.uf',
                        'u.cidade',
                        'u.sexo',
                        'u.filhos',
                        'u.interesse'
                    ))
                    ->where('u.role in (2)');
                
                if((bool)@$params['data_from'])
                    $select->where('u.data_cad >= '.$db->quote(Is_Date::br2am($params['data_from']).' 00:00:00'));
                
                if((bool)@$params['data_to'])
                    $select->where('u.data_cad <= '.$db->quote(Is_Date::br2am($params['data_to']).' 23:59:59'));

                $group = array();
                $order = array();

                if((bool)@$params['filter']){
                    switch($params['filter']){
                        case 'uf':
                            $group[] = 'u.uf';
                            $order[] = 'u.uf asc';
                            break;
                        case 'nome':
                            $group[] = 'u.id';
                            $order[] = 'u.nome asc';
                            break;
                        case 'atividade_profissional_id':
                            $group[] = 'u.atividade_profissional_id';
                            $order[] = 'u.atividade_profissional_id asc';
                            break;
                    }
                }

                if((bool)@$params['filter2']){
                    switch($params['filter2']){
                        case 'nome':
                            $group[] = 'u.id';
                            $order[] = 'u.nome asc';
                            break;
                    }
                }

                if((bool)$group) $select->group($group); //else $select->group('data');
                if((bool)$order) $select->order($order); else $select->order('count_access desc');

                break;

            case '4':
                $select->from('track_downloads as ta',array('ta.arquivo_id,ta.foto_id,date_format(ta.data,"%Y-%m-%d") as data,ta.session_id,ta.user_id,sum(ta.counter) as count_access'))
                    ->joinLeft('usuarios as u','u.id = ta.user_id',array(
                        // 'date_format(u.data_cad,"%Y-%m-%d") as data',
                        'u.nome',
                        'u.atividade_profissional_id',
                        'u.escolaridade_id',
                        'u.uf',
                        'u.cidade',
                        'u.sexo',
                        'u.filhos',
                        'u.interesse'
                    ))
                    ->where('u.role in (2)');
                
                if((bool)@$params['data_from'])
                    $select->where('u.data_cad >= '.$db->quote(Is_Date::br2am($params['data_from']).' 00:00:00'));
                
                if((bool)@$params['data_to'])
                    $select->where('u.data_cad <= '.$db->quote(Is_Date::br2am($params['data_to']).' 23:59:59'));

                $group = array();
                $order = array();

                if((bool)@$params['filter']){
                    switch($params['filter']){
                        case 'uf':
                            $group[] = 'u.uf';
                            $order[] = 'u.uf asc';
                            break;
                        case 'nome':
                            $group[] = 'u.id';
                            $order[] = 'u.nome asc';
                            break;
                        case 'atividade_profissional_id':
                            $group[] = 'u.atividade_profissional_id';
                            $order[] = 'u.atividade_profissional_id asc';
                            break;
                    }
                }

                if((bool)@$params['filter2']){
                    switch($params['filter2']){
                        case 'nome':
                            $group[] = 'u.id';
                            $order[] = 'u.nome asc';
                            break;
                    }
                }

                if((bool)$group) $select->group($group); else $select->group('ta.foto_id','ta.arquivo_id');
                if((bool)$order) $select->order($order); else $select->order('count_access desc');

                break;

            case '5':
                $select->from('usuarios as u',array(
                        'date_format(u.data_cad,"%Y-%m-%d") as data',
                        'u.atividade_profissional_id',
                        'u.escolaridade_id',
                        // 'count(u.id) as count_records',
                        'u.nome',
                        'u.email',
                        'u.uf'
                        )
                    )
                    ->where('u.role in (2)');
                
                if((bool)@$params['data_from'])
                    $select->where('u.data_cad >= '.$db->quote(Is_Date::br2am($params['data_from']).' 00:00:00'));
                
                if((bool)@$params['data_to'])
                    $select->where('u.data_cad <= '.$db->quote(Is_Date::br2am($params['data_to']).' 23:59:59'));

                $group = array();
                $order = array();

                if((bool)@$params['filter']){
                    switch($params['filter']){
                        case 'uf':
                            // $group[] = 'u.uf';
                            $order[] = 'u.uf asc';
                            $order[] = 'u.nome asc';
                            break;
                        case 'escolaridade_id':
                            // $group[] = 'u.atividade_profissional_id';
                            $order[] = 'u.escolaridade_id desc';
                            $order[] = 'u.escolaridade_completo desc';
                            $order[] = 'u.nome asc';
                            break;
                        case 'atividade_profissional_id':
                            // $group[] = 'u.atividade_profissional_id';
                            $order[] = 'u.atividade_profissional_id asc';
                            $order[] = 'u.nome asc';
                            break;
                        default:
                            $order[] = 'u.nome asc';
                    }
                }

                if((bool)$group) $select->group($group); //else $select->group('ta.foto_id','ta.arquivo_id');
                if((bool)$order) $select->order($order); else $select->order('nome asc');

                break;
        }
        // _d($select->query());
        $_rows = $select->query()->fetchAll();
        $rows = array();

        if(count($_rows)){
            for($i=0;$i<sizeof($_rows);$i++){
                $r = Is_Array::utf8All($_rows[$i]);
                $r = Is_Array::toObject($r);

                $rows[$i] = $r;
            }
        }

        return $rows;
    }

    public function getAccess($url=null,$unique=false)
    {
        if(!$url) return 0;
        global $dbAdapter; $db = $dbAdapter;

        $query = 'select sum(a.counter) as counts from track_access as a '.
                 'where a.url like '.$db->quote($url);
        if($unique) $query.= ' group by a.user_id';

        $result = $db->query($query)->fetchAll();
        return $unique ? count($result) : $result[0]['counts'];
    }
}
