<?php

class Application_Model_Carrinho
{
    public $payments = array(
        'PagSeguro',
        'Mup'
    );

    public $activePayment = 'Mup';

    public function __construct()
    {
        // models
        // $this->cobranca = new Application_Model_Db_CobrancaPagSeguro(); // tabela de cobrança
        $this->orcamentos = new Application_Model_Db_Orcamentos(); // tabela de orcamento
        $this->orcamento_itens = new Application_Model_Db_OrcamentoItems(); // tabela de itens do orcamento
        $this->pedidos = new Application_Model_Db_Pedidos(); // tabela de pedidos
        $this->pedidos_itens = new Application_Model_Db_PedidosItems(); // tabela de itens do pedido
        // $this->pedidos_taxas = new Application_Model_Db_PedidosTaxas(); // tabela de taxas do pedido
        $this->pedidos_formas = new Application_Model_Db_PedidosFormas(); // tabela de formas do pedido
        $this->pedidos_status = new Application_Model_Db_PedidosStatus(); // tabela de status do pedido
        $this->pedidos_vp = new Application_Model_Db_PedidosVp(); // tabela de pedidos vale-presente
        // $this->locais = new Application_Model_Db_LocalEntrega(); // tabela de status do pedido
        // $this->taxas = new Application_Model_Db_Taxas(); // tabela de taxas
        // $this->produtos = new Application_Model_Db_Produtos(); // tabela de produtos
        // $this->cores = new Application_Model_Db_Cores(); // tabela de cores
        $this->clientes = new Application_Model_Db_Clientes(); // tabela de clientes
        $this->usuarios = new Application_Model_Db_Usuario(); // tabela de usuarios
        $this->produtos = new Application_Model_Db_Produtos(); // tabela de produtos
        $this->session  = new Zend_Session_Namespace(SITE_NAME."_cart"); // sessão que armazena os dados do carrinho
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login'); // sessão de login
        $this->select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter()); // select
        $this->payment = null;
    }
    
    public function _whereTaxa($total){
        return 'status_id = 1'.
               ' and (isencao is not null'.
               ' and isencao <> \'0.00\''.
               ' and isencao > \''.$total.'\')';
    }
    
    /** ************************************************************************
     * Início das funções que trabalham com a sessão do carrinho
     ************************************************************************ */
    
    /**
     * Adiciona item a sessão do carrinho
     *
     * @param int $id - id do produto a ser adicionado
     *
     * @return void
     */
    public function add($id=null,$titulo='',$formato_id='',$formato='',$gramatura_id='',$gramatura='',$qtde='',$obs='')
    {
        if(!(bool)$id) return;
        
        $sid = $id;
        $field_id = 'produto_id';
        $table = 'produtos';
        
        //if(!isset($this->session->items[$sid])){
            $this->session->items[$sid] = new stdClass();
            $this->session->items[$sid]->$field_id = $id;
            $this->session->items[$sid]->id = $id;
            $this->session->items[$sid]->titulo = $titulo;
            $this->session->items[$sid]->formato_id = $formato_id;
            $this->session->items[$sid]->formato = $formato;
            $this->session->items[$sid]->gramatura_id = $gramatura_id;
            $this->session->items[$sid]->gramatura = $gramatura;
            $this->session->items[$sid]->qtde = $qtde;
            $this->session->items[$sid]->obs = $obs;
            $this->session->items[$sid]->amount = 1;
            $this->session->items[$sid]->item = Is_Array::utf8DbRow($this->$table->fetchRow('id="'.$id.'"'));
            $this->session->count = $this->countItens();
        //} else {
            //$this->session->items[$sid]->amount++;
            //$this->session->count = $this->countItens();
        //}
    }
    
    /**
     * Atualiza a sessão do carrinho
     *
     * @param array $values - array com os valores do carrinho (id,amount,produto_id)
     *
     * @return void
     */
    public function update($values)
    {
        $this->reset();
        
        for($i=0;$i<sizeof($values['item-id']);$i++){
            $sid = $values['item-id'][$i];//.'_'.@$values['item-cor-id'][$i];
            
            $this->session->items[$sid] = new stdClass();
            $this->session->items[$sid]->id = $values['item-id'][$i];
            $this->session->items[$sid]->amount = $values['item-amount'][$i];
            $this->session->items[$sid]->item = $this->produtos->checkDesconto(Is_Array::utf8DbRow($this->produtos->fetchRow('id="'.$values['item-id'][$i].'"')));
            // $this->session->items[$sid]->cor_id = @$values['item-cor-id'][$i];
            // $cor = $this->cores->fetchRow('id="'.@$values['item-cor-id'][$i].'"');
            // $this->session->items[$sid]->cor = $cor ? Is_Array::utf8DbRow($cor) : null;
            
            if(isset($values['vp_para_'.$values['item-id'][$i]])){
                $this->session->items[$sid]->vp_para = $values['vp_para_'.$values['item-id'][$i]];
            }
            
            if(isset($values['item-withvp_'.$values['item-id'][$i]])){
                $this->session->items[$sid]->withvp = true;
                $this->session->items[$sid]->item->valor = $this->session->items[$sid]->item->valor - ($this->session->items[$sid]->item->valor*2);
            }
        }
        
        if(MOD_FRETE){
            $this->session->frete = $values['item-valor-total-frete'];
        }
        
        $this->session->count = $this->countItens();
    }
    
    /**
     * Remove item da sessão do carrinho
     *
     * @param int $id - id do produto a ser removido
     *
     * @return void
     */
    public function remove($id)
    {
        if(isset($this->session->items[$id])){
            unset($this->session->items[$id]);
            $this->session->count = $this->countItens();
        }
    }
    
    /**
     * Conta itens do carrinho
     */
    public function countItens()
    {
        $count = 0;
        
        foreach($this->session->items as $item){
            $count+= $item->amount;
        }
        
        return $count;
    }
    
    /**
     * Retorna valor total do carrinho
     */
    public function getTotalPrice()
    {
        $total = 0;
        
        foreach($this->getItems() as $item){
            $total+= $item->item->valor * $item->amount;
        }
        
        return $total;
    }
    
    /**
     * Reseta seção do carrinho
     *
     * @return void
     */
    public function reset()
    {
        $this->session->items = array();
        $this->session->count = 0;
        Zend_Session::regenerateId();
    }
    
    /**
     * Retorna items do carrinho ou item se passado o id
     *
     * @param int|void $id - id do produto / item da sessão
     *
     * @return array - item ou itens da sessão
     */
    public function getItems($id=null)
    {
        return $id ? $this->session->items[$id] : $this->session->items;
    }
    
    /**
     * Transforma itens para serem usados pelo Form de pagamento
     */
    public function itemsToValues()
    {
        $values = array(); $total = 0;
        
        foreach($this->getItems() as $item){
            $value = array(
                'item_id' => $item->id,
                'item_descr' => $item->item->titulo,
                // 'item_cor' => $item->cor_id,
                'item_quant' => $item->amount,
                'item_valor' => str_replace('.','',$item->item->valor)
            );
            $values[] = $value;
            $total+= $item->item->valor * $item->amount;
        }
        /*
        // selecionamos as taxas ativas e com isenção
        if($taxas = $this->taxas->fetchAll($this->_whereTaxa($total))){
            foreach(Is_Array::utf8DbResult($taxas) as $item){
                $value = array(
                    'item_id' => $item->id,
                    'item_descr' => $item->descricao,
                    'item_quant' => 1,
                    'item_valor' => str_replace('.','',$item->valor)
                );
                $values[] = $value;
            }
        }*/
        
        return $values;
    }

    public function dadosBoletoMup($order_id=null)
    {
        // resgatando dados da compra
        if(!(bool)$order_id) die('Erro ao localizar compra');

        $order = new stdClass();
        $order->orderid = $order_id;
        $total = 0;

        $pedido = Is_Array::utf8DbRow($this->pedidos->fetchRow('id='.$order_id));

        if(!(bool)$order_id) die('Erro ao localizar compra');

        // pegando itens
        $items = Is_Array::utf8DbResult($this->pedidos_itens->fetchAll('pedido_id='.$order_id));

        if(count($items)){
            $_items = array();

            foreach($items as $item){
                $_items[$item->id] = new stdClass();
                $_items[$item->id]->descritivo = $item->descricao;
                $_items[$item->id]->quantidade = $item->qtde;
                $_items[$item->id]->unidade = 'un';
                $_items[$item->id]->valor = str_replace(array(',','.'),'',$item->valor);

                $total+= $item->valor;
            }

            $order->items = $_items;
        }

        // dados do cliente
        $cliente = Is_Array::utf8DbRow($this->usuarios->fetchRow('id='.$pedido->cliente_id));
        $order->cliente = new stdClass();
        $order->cliente->NOMESACADO = $cliente->nome;
        $order->cliente->ENDERECOSACADO = $cliente->endereco.', '.$cliente->numero.
                                          (((bool)trim($cliente->complemento)) ?
                                                ' - '.$cliente->complemento.' - ':' - ').
                                          $cliente->bairro;
        $order->cliente->CIDADESACADO = $cliente->cidade;
        $order->cliente->UFSACADO = $cliente->estado;
        $order->cliente->CEPSACADO = $cliente->cep;
        $order->cliente->CPFSACADO = $cliente->cpf;

        // outros dados da compra
        $order->VALORDOCUMENTOFORMATADO = 'R$'.number_format($total,2,',','.');

        // importando MUP e exibindo dados
        $cwd = getcwd();
        chdir('./library/');
        Lib::import('Payment.Mup','./'); // importa biblioteca do Mup
        // Lib::importAll('PagSeguroLibrary',array('loader/')); // importa biblioteca do Mup
        chdir($cwd);

        $mup = new Payment_Mup();
        return $mup->dadosBoleto($order);
    }

    public function dadosDebitoMup($order_id=null)
    {
        // resgatando dados da compra
        if(!(bool)$order_id) die('Erro ao localizar compra');

        $order = new stdClass();
        $order->orderid = $order_id;
        $total = 0;

        $pedido = Is_Array::utf8DbRow($this->pedidos->fetchRow('id='.$order_id));

        if(!(bool)$order_id) die('Erro ao localizar compra');

        // pegando itens
        $items = Is_Array::utf8DbResult($this->pedidos_itens->fetchAll('pedido_id='.$order_id));

        if(count($items)){
            $_items = array();

            foreach($items as $item){
                $_items[$item->id] = new stdClass();
                $_items[$item->id]->descritivo = $item->descricao;
                $_items[$item->id]->quantidade = $item->qtde;
                $_items[$item->id]->unidade = 'un';
                $_items[$item->id]->valor = str_replace(array(',','.'),'',$item->valor);

                $total+= $item->valor;
            }

            $order->items = $_items;
        }
        /*
        // dados do cliente
        $cliente = Is_Array::utf8DbRow($this->usuarios->fetchRow('id='.$pedido->cliente_id));
        $order->cliente = new stdClass();
        $order->cliente->NOMESACADO = $cliente->nome;
        $order->cliente->ENDERECOSACADO = $cliente->endereco.', '.$cliente->numero.
                                          (((bool)trim($cliente->complemento)) ?
                                                ' - '.$cliente->complemento.' - ':' - ').
                                          $cliente->bairro;
        $order->cliente->CIDADESACADO = $cliente->cidade;
        $order->cliente->UFSACADO = $cliente->estado;
        $order->cliente->CEPSACADO = $cliente->cep;
        $order->cliente->CPFSACADO = $cliente->cpf;

        // outros dados da compra
        $order->VALORDOCUMENTOFORMATADO = 'R$'.number_format($total,2,',','.');
        */

        // importando MUP e exibindo dados
        $cwd = getcwd();
        chdir('./library/');
        Lib::import('Payment.Mup','./'); // importa biblioteca do Mup
        // Lib::importAll('PagSeguroLibrary',array('loader/')); // importa biblioteca do Mup
        chdir($cwd);

        $mup = new Payment_Mup();
        return $mup->dadosDebito($order);
    }

    public function setPayment($var1=null)
    {
        $action = 'setPayment'.$this->activePayment;
        $this->$action($var1);
    }

    /**
     * Seta forma de pagamento e adiciona os itens da seção ao banco
     */
    public function setPaymentMup($tipo='boleto')
    {
        $cwd = getcwd();
        chdir('./library/');
        Lib::import('Payment.Mup','./'); // importa biblioteca do Mup
        // Lib::importAll('PagSeguroLibrary',array('loader/')); // importa biblioteca do Mup
        chdir($cwd);

        $this->tipo_pagamento = $tipo;
        $this->payment = new Payment_Mup($tipo); // cria um novo objeto de pagamento do PagSeguro
        $items = $this->getItems();
        $total = 0;
        $extra = 0;
        
        foreach($items as $item){
            $item_titulo = utf8_decode($item->titulo);
            $this->payment->addItem($item->id,$item_titulo,$item->amount,$item->valor);
            $total+= $item->item->valor * $item->amount;
        }
        
        // $bairro = $this->login->user->local_id === NULL ? $this->login->user->bairro : utf8_encode($this->locais->fetchRow('id='.$this->login->user->local_id)->descricao);
        
        /*$this->payment->setSender(
            $this->login->user->nome,
            $this->login->user->email,
            substr($this->login->user->telefone,0,2),
            substr($this->login->user->telefone,2,8)
        );
        
        $this->payment->setShippingAddress(
            $this->login->user->cep,
            $this->login->user->logradouro,
            $this->login->user->numero,
            $this->login->user->complemento,
            $bairro,
            $this->login->user->cidade,
            strtoupper($this->login->user->uf),
            'BRA'
        );
        
        // checa taxas
        if($taxas = $this->taxas->fetchAll($this->_whereTaxa($total))){
            foreach(Is_Array::utf8DbResult($taxas) as $item){
                $extra+= $item->valor;
            }
        }
        
        // checa frete
        if(MOD_FRETE && isset($this->session->frete)){
            $extra+= $this->session->frete;
        }
        
        if($extra > 0){
            $this->payment->setExtraAmount($extra);
        }*/
        
        // seta referência da transação
        $pedido_id = $this->save();
        $_SESSION[SITE_NAME.'_pedido_id'] = $pedido_id;
        $this->payment->setOrder($pedido_id);
        
        if($total < 0){
            header('Location: '.URL.'/');
            exit();
        }
    }
    
    /**
     * Seta forma de pagamento e adiciona os itens da seção ao banco
     */
    public function setPaymentPagSeguro()
    {
        $cwd = getcwd();
        chdir('./library/');
        Lib::import('PagSeguroLibrary.PagSeguroLibrary','./'); // importa biblioteca do PagSeguro
        Lib::importAll('PagSeguroLibrary',array('loader/')); // importa biblioteca do PagSeguro
        chdir($cwd);
        
        $this->payment = new PaymentRequest(); // cria um novo objeto de pagamento do PagSeguro
        $items = $this->getItems();
        $total = 0;
        $extra = 0;
        
        foreach($items as $item){
            if(isset($item->withvp) && @$item->withvp == true){
                $extra+= $item->item->valor;
                $total+= $item->item->valor;
            } else {
                $item_titulo = utf8_decode($item->item->titulo).
                               ((bool)$item->item->cod ? ' ['.$item->item->cod.']' : '');
                               /*($item->cor?' ('.$item->cor->descricao.')':'');*/
                $this->payment->addItem($item->id,$item_titulo,$item->amount,$item->item->valor);
                $total+= $item->item->valor * $item->amount;
            }
        }
        
        $bairro = $this->login->user->local_id === NULL ? $this->login->user->bairro : utf8_encode($this->locais->fetchRow('id='.$this->login->user->local_id)->descricao);
        
        $this->payment->setSender(
            $this->login->user->nome,
            $this->login->user->email,
            substr($this->login->user->telefone,0,2),
            substr($this->login->user->telefone,2,8)
        );
        
        $this->payment->setShippingAddress(
            $this->login->user->cep,
            $this->login->user->logradouro,
            $this->login->user->numero,
            $this->login->user->complemento,
            $bairro,
            $this->login->user->cidade,
            strtoupper($this->login->user->uf),
            'BRA'
        );
        /*
        // checa taxas
        if($taxas = $this->taxas->fetchAll($this->_whereTaxa($total))){
            foreach(Is_Array::utf8DbResult($taxas) as $item){
                $extra+= $item->valor;
            }
        }*/
        
        // checa frete
        if(MOD_FRETE && isset($this->session->frete)){
            $extra+= $this->session->frete;
        }
        
        if($extra > 0){
            $this->payment->setExtraAmount($extra);
        }
        
        // seta referência da transação
        $pedido_id = $this->save();
        $_SESSION[SITE_NAME.'_pedido_id'] = $pedido_id;
        $this->payment->setReference($pedido_id);
        
        if($total < 0){
            header('Location: '.URL.'/meu-carrinho/confirmacao/');
            exit();
        }
    }
    
    public function setTransaction($pedido_id=null,$transaction_id=null)
    {
        return ($pedido_id && $transaction_id) ? $this->pedidos->update(
            array('transaction_id'=>$transaction_id),
            'id='.$pedido_id
        ) : null;
    }

    public function complete()
    {
        $action = 'complete'.$this->activePayment;
        $this->$action();
    }
    
    /**
     * Finaliza a transação com base na forma de pagamento
     */
    public function completeMup()
    {
        if($this->payment === null){
            throw new Exception('Forma de pagamento inválida.');
            return false;
        }
        
        // _d($this->payment->url());
        header('Location: '.$this->payment->url());
        exit();
    }
    
    /**
     * Finaliza a transação com base na forma de pagamento
     */
    public function completePagSeguro()
    {
        if($this->payment === null){
            throw new Exception('Forma de pagamento inválida.');
            return false;
        }
        
        $cobranca = $this->getCobranca(); // seta dados de cobrança
        $this->payment->setCurrency($cobranca['moeda']);
        $this->payment->setShippingType($cobranca['tipo_frete']);
        $credentials = new AccountCredentials($cobranca['email_cobranca'],$cobranca['token']);
        
        try{
            $url = $this->payment->register($credentials);
        } catch(PagSeguroServiceException $e){
            $msg = '<h3>Erro '.$e->getHttpStatus()->getStatus().'</h3>';
            
            foreach($e->getErrors(null) as $key => $error){
                $msg.= '<br/>('.$error->getCode().') '.$error->getMessage();
            }
            
            throw new Exception($msg,$e->getHttpStatus()->getStatus());
            return false;
        } catch(Exception $e){
            throw new Exception('1'.$e->getMessage(),$e->getCode());
            return false;
        }
        
        if(strstr($url,'showError')) header('Location: '.URL.'/meu-carrinho/confirmacao/');
        header('Location: '.$url);
    }
    
    public function formatTransactionId($cod)
    {
        return substr($cod,0,8).'-'.
               substr($cod,8,4).'-'.
               substr($cod,12,4).'-'.
               substr($cod,16,4).'-'.
               substr($cod,20,strlen($cod));
    }
    
    public function retorno($post)
    {
        $cwd = getcwd();
        chdir('./library/');
        Lib::import('PagSeguroLibrary.PagSeguroLibrary','./'); // importa biblioteca do PagSeguro
        Lib::importAll('PagSeguroLibrary',array('loader/')); // importa biblioteca do PagSeguro
        chdir($cwd);
        
        $cobranca = $this->getCobranca(); // seta dados de cobrança
        $credentials = new AccountCredentials($cobranca['email_cobranca'],$cobranca['token']);
        
        /* Tipo de notificação recebida */  
        $type = isset($post['notificationType']) ? $post['notificationType'] : null;
        /* Código da notificação recebida */  
        $code = isset($post['notificationCode']) ? $post['notificationCode'] : null;  
        
        /* Verificando tipo de notificação recebida */
        if(isset($post['TransacaoID'])){
            $transaction_id = $this->formatTransactionId($post['TransacaoID']);
            
            $transaction = TransactionSearchService::searchByCode(  
                $credentials,  
                $transaction_id
            );
            //Is_Var::dump($transaction->getStatus()->getValue());
            $status = $transaction->getStatus()->getValue();
            $pedido_id = $post['Referencia']; // $transaction->getReference();
            
            $pedido = $this->pedidos->fetchRow('id='.$pedido_id);
            $pedido->pedido_status_id = $status;
            
            if($status == 3){ // se status for 'Pago' atualiza a data do pagamento
                $pedido->data_pago = new Zend_Db_Expr('NOW()');
            }
            
            $pedido->save();
            //$this->reset();
            
            try {
                $sub = 'Alteração de status do Pedido: '.$this->formatNumber($pedido_id);
                $html = '<p>Alteração de status do pedido: '.
                        '<a href="'.URL.'/admin/acompanhamento/pedidos/?id='.$pedido_id.
                        '">#'.$this->formatNumber($pedido_id).'</a><br /><br />'.
                        'Status da transação:<b>'.
                        $this->pedidos_status->fetchRow('id='.$status)->descricao.'</b></p>';
                Trupe_CinCin_Mail::sendFollowUp($sub,$html);
                
                if($status == 4){ // se for confirmação de pagamento
                    // envia vale-presente
                    $select = $this->select->reset();
                    $select->from('pedidos_vp as pvp',array('pvp.cod','pvp.valor','pvp.para','pvp.usado'))
                           ->joinLeft('pedidos_items as pi','pi.id = pvp.pedido_item_id',array())
                           ->where('pi.pedido_id = ?',$pedido_id);
                    
                    $rows = $select->query()->fetchAll();
                    
                    if(count($rows)){
                        $row = Is_Array::toObject($rows[0]);
                        $cliente = $this->clientes->fetchRow('id="'.$pedido->cliente_id.'"');
                        
                        $sub2 = 'Você recebeu um vale presente!';
                        $html2 = '<p>Olá! <br /><br /> Você recebeu um vale presente de nossa loja '.
                                 'no valor de <b>R$ '.str_replace('.',',',$row->valor).'</b>'.
                                 'em nome de '.$cliente->nome.'.</p>'.
                                 '<p>Para utilizar seu vale presente, utilize o código abaixo ao realizar uma compra em nosso site, e terá um '.
                                 'desconto no valor do vale.</p>'.
                                 '<p>Código do vale presente: <b>'.$row->cod.'</b></p>'.
                                 '<p>Acesse nossa loja: <a href="'.URL.'">'.URL.'</a></p>'.
                                 '<b>*Ps.:</b> Caso o valor da compra seja maior do que o do vale será cobrado o valor excedente, caso seja menor não haverá retorno(troco) para a compra.</p>'.
                                 '<p><b>*Ps2.:</b> O código só poderá ser utilizado uma vez (não cumulativo). <br />'.
                                 '<p>Boas compras!! <br />Atenciosamente, <br />'.SITE_TITLE.'</p>';
                        Trupe_CinCin_Mail::send($row->para,$row->para,$sub2,$html2);
                    }
                }
            } catch(Exception $e){
                $log = 'public/logs/error.json';
                $err = array(
                    'date'  => date('Y-m-d H:i:s'),
                    'error' => $e->getMessage()
                );
                
                $f = fopen($log,'r');
                $content = json_decode(fread($f,filesize($log)));
                fclose($f);
                //Is_Var::dump($content);
                $content->errors[] = $err;
                
                $f = fopen($log,'a+');
                ftruncate($f,0);
                fwrite($f,json_encode($content));
                fclose($f);
                
                Trupe_CinCin_Mail::sendError(SITE_NAME.' error: carrinho-retorno',$e->getMessage(),$e->getMessage());
                die($e->getMessage());
            }
        } else if ($type == 'transaction') {
            exit();
            /* Obtendo o objeto Transaction a partir do código de notificação */  
            $transaction_status = NotificationService::checkTransaction(  
                $credentials,  
                $code // código de notificação  
            );
            
            $transaction = TransactionSearchService::searchByCode(  
                $credentials,  
                $post['transaction_id']
            );
            
            $status = $transaction_status->getStatus()->getValue();
            $pedido_id = $transaction->getReference();
            
            $pedido = $this->pedidos->fetchRow('id='.$pedido_id);
            $pedido->pedido_status_id = $status;
            
            if($status == 3){ // se status for 'Pago' atualiza a data do pagamento
                $pedido->data_pago = new Zend_Db_Expr('NOW()');
            }
            
            $pedido->save();
            //$this->reset();
            
            try {
                $sub = 'Alteração de status do Pedido: '.$this->formatNumber($pedido_id);
                $html = '<p>Alteração de status do pedido: '.
                        '<a href="'.URL.'/admin/acompanhamento/pedidos/?id='.$pedido_id.
                        '">#'.$this->formatNumber($pedido_id).'</a><br /><br />'.
                        'Status da transação:<b>'.
                        $this->pedidos_status->fetchRow('id='.$status)->descricao.'</b></p>';
                Trupe_CinCin_Mail::sendFollowUp($sub,$html);
                
                if($status == 4){ // se for confirmação de pagamento
                    // envia vale-presente
                    $select = $this->select->reset();
                    $select->from('pedidos_vp as pvp',array('pvp.cod','pvp.valor','pvp.para','pvp.usado'))
                           ->joinLeft('pedidos_items as pi','pi.id = pvp.pedido_item_id',array())
                           ->where('pi.pedido_id = '.$pedido_id);
                    
                    $rows = $select->query()->fetchAll();
                    
                    if(count($rows)){
                        $row = Is_Array::toObject($rows[0]);
                        $cliente = $this->clientes->fetchRow('id="'.$pedido->cliente_id.'"');
                        
                        $sub2 = 'Você recebeu um vale presente!';
                        $html2 = '<p>Olá! <br /><br /> Você recebeu um vale presente de nossa loja '.
                                 'no valor de <b>R$ '.str_replace('.',',',$row->valor).'</b>'.
                                 'em nome de '.$cliente->nome.'.</p>'.
                                 '<p>Para utilizar seu vale presente, utilize o código abaixo ao realizar uma compra em nosso site, e terá um '.
                                 'desconto no valor do vale presente.</p>'.
                                 '<p>Código do vale presente: <b>'.$row->cod.'</b></p>'.
                                 '<p>Acesse nossa loja: <a href="'.URL.'">'.URL.'</a></p>'.
                                 '<p>Atenciosamente, <br />'.SITE_TITLE.'</p>';
                        Trupe_CinCin_Mail::send($row->para,$row->para,$sub2,$html2);
                    }
                }
            } catch(Exception $e){
                $log = 'public/logs/error.json';
                $err = array(
                    'date'  => date('Y-m-d H:i:s'),
                    'error' => $e->getMessage()
                );
                
                $f = fopen($log,'r');
                $content = json_decode(fread($f,filesize($log)));
                fclose($f);
                //Is_Var::dump($content);
                $content->errors[] = $err;
                
                $f = fopen($log,'a+');
                ftruncate($f,0);
                fwrite($f,json_encode($content));
                fclose($f);
                
                Trupe_CinCin_Mail::sendError(SITE_NAME.' error: carrinho-retorno',$e->getMessage(),$e->getMessage());
                die($e->getMessage());
            }
        } else {
            //Is_Var::dump($post);
        }
    }
    
    /** ************************************************************************
     * Início das funções que trabalham com o banco de dados
     ************************************************************************ */
    
    /**
     * Adiciona carrinho da sessão no banco
     */
    public function save()
    {
        if(count($this->session->items)){
            $html = '<ul>'; $total = 0;
            
            if($pedido_id = $this->pedidos->exists(array(
                    'cliente_id' => $this->login->user->id,
                    'sessid'     => Zend_Session::getId(),
                    'ip'         => ip2long(Is_Server::getIp())
                ))){
                $this->pedidos->update(array(
                    // 'pedido_forma_id' => null,//$this->session->forma,
                    'forma'     => substr($this->tipo_pagamento,0,1),
                    'obs'       => (isset($_POST) && isset($_POST['cliente_obs'])) ?
                                    utf8_decode($_POST['cliente_obs']) :
                                    null,
                    'data_edit' => date('Y-m-d H:i:s'),
                    'user_edit' => $this->login->user->id
                ),'id='.$pedido_id);
                
                $this->pedidos_itens->delete('pedido_id='.$pedido_id);
                // $this->pedidos_taxas->delete('pedido_id='.$pedido_id);
            } else {                
                $pedido_id = $this->pedidos->insert(array(
                    'cliente_id'         => $this->login->user->id,
                    'pedido_status_id'   => 1, // Pedido efetuado
                    // 'pedido_forma_id'    => null,//$this->session->forma,
                    'forma'              => substr($this->tipo_pagamento,0,1),
                    'sessid'             => Zend_Session::getId(),
                    'ip'                 => ip2long(Is_Server::getIp()),
                    'obs'                => (isset($_POST) && isset($_POST['cliente_obs'])) ?
                                             utf8_decode($_POST['cliente_obs']) :
                                             null,
                    'data_cad'           => date('Y-m-d H:i:s'),
                    'user_cad'           => $this->login->user->id
                ));
            }
            
            $html.= '<li><b>Data:</b> '.date('d/m/Y H:i:s').'</li>';
            //$html.= '<li><b>Forma de entrega:</b> '.$this->pedidos_formas->fetchRow('id='.$this->session->forma)->descricao.'</li>';
            $html.= '<li><b>Itens:</b></li><ul>';
            
            foreach($this->session->items as $key => $item){
                // $p = $this->produtos->fetchRow('id='.$item->id);
                
                $piid = $this->pedidos_itens->insert(array(
                    'pedido_id'         => $pedido_id,
                    $item->tipo.'_id'   => $item->id,
                    // 'produto_cor_id'    => $item->cor_id,
                    // 'produto_cor'       => $item->cor ? $item->cor->descricao : null,
                    'descricao'         => $item->titulo,
                    'valor'             => $item->valor,
                    // 'produto_peso'      => $p->peso,
                    // 'produto_cod'       => $p->cod,
                    'qtde'              => $item->amount,
                    'data_agendamento'  => $item->tipo=='consulta' ? $item->item->data_ini : null,
                    'agendado'          => $item->tipo=='consulta' ? 1 : 0
                ));

                if($item->tipo=='consulta') {
                    $this->consultas->update(array('status'=>1),'id='.$item->id);
                }
                
                $html.= '<li>'.utf8_encode($item->titulo).'</li>';//' ('.$item->amount.')</li>';
                $total+= $item->item->valor * $item->amount;
            }
            
            /*$html.= '</ul><li><b>Taxas:</b></li><ul>';
            
            // taxas
            foreach($this->taxas->fetchAll($this->_whereTaxa($total)) as $taxa){
                $this->pedidos_taxas->insert(array(
                    'pedido_id'      => $pedido_id,
                    'taxa_id'        => $taxa->id,
                    'taxa_descricao' => $taxa->descricao,
                    'taxa_valor'     => $taxa->valor,
                ));
                
                $html.= '<li>'.utf8_encode($taxa->descricao).'</li>';
                $total+= $taxa->valor;
            }*/
            
            /*// frete
            if(MOD_FRETE && isset($this->session->frete)){
                $this->pedidos_taxas->insert(array(
                    'pedido_id'      => $pedido_id,
                    'taxa_id'        => null,
                    'taxa_descricao' => 'Frete Correios',
                    'taxa_valor'     => $this->session->frete,
                ));
                
                $html.= '<li>Frete Correios ('.$this->session->frete.')</li>';
                $total+= $this->session->frete;
            }*/
            
            $html.= '</ul><li><b>Total do pedido:</b> R$ '.$total.'</li></ul>';
            $html.= '<p><b>Dados do cliente:</b></p><ul>';
            $html.= '<li><b>Nome:</b> '.utf8_encode($this->login->user->nome).'</li>';
            $html.= '<li><b>E-mail:</b> <a href="mailto:'.$this->login->user->email.'">'.
                    $this->login->user->email.'</a></li>';
            $html.= '<li><b>Telefone:</b> '.Is_F::formatTel($this->login->user->tel).'</li>';
            $html.= '<li><b>Mensagem:</b> '.((isset($_POST) && isset($_POST['cliente_obs'])) ? $_POST['cliente_obs'] : '').'</li>';
            $html.= '</ul>';
            
            try {
                $sub = 'Novo Pedido: '.$this->formatNumber($pedido_id);
                $html = '<p>Novo pedido realizado através do site: '.
                        '<a href="'.URL.'/admin/acompanhamento/pedidos/?id='.$pedido_id.
                        '">#'.$this->formatNumber($pedido_id).'</a><br /><br />'.
                        '<b>Dados do pedido:</b></p>'.$html;
                Trupe_CinCin_Mail::sendFollowUp($sub,$html,true);
            } catch(Exception $e){
                die($e->getMessage());
            }
            
            return $pedido_id;
        }
        
        return null;
    }

    /**
     * Adiciona carrinho de orçamento no banco
     */
    public function saveOrcamento($dados_cliente=array(),$user)
    {
        $dados_orcamento = array();
        $dados_orcamento_items = array();

        if(count($this->session->items)){
            $dados_orcamento['cliente_id']         = $user->id;
            $dados_orcamento['cliente_nome']       = $user->nome;
            $dados_orcamento['cliente_email']      = $user->email;
            $dados_orcamento['cliente_telefone']   = Is_Cpf::clean($user->telefone);
            $dados_orcamento['obs']                = $dados_cliente['obs'];
            $dados_orcamento['orcamento_status_id']= 1; // Pedido efetuado;
            $dados_orcamento['sessid']             = Zend_Session::getId();
            $dados_orcamento['ip']                 = ip2long(Is_Server::getIp());
            $dados_orcamento['data_cad']           = date('Y-m-d H:i:s');
            
            $html = '<p><b>Dados do cliente:</b></p><ul>';
            $html.= '<li><b>Nome:</b> '.($user->nome).'</li>';
            $html.= '<li><b>E-mail:</b> <a href="mailto:'.$user->email.'">'.$user->email.'</a></li>';
            $html.= '<li><b>Telefone:</b> '.$user->telefone.'</li>';
            $html.= '<li><b>Observações:</b> '.@$dados_cliente['obs'].'</li>';
            $html.= '</ul>';

            $html.= '<p><b>Dados do pedido:</b></p><ul>'; 
            // $total = 0;
            
            $html.= '<li><b>Data:</b> '.date('d/m/Y H:i:s').'</li>';
            $html.= '<li><b>Itens:</b></li><ul>';
            
            for($i=0;$i<sizeof($dados_cliente['id']);$i++){
                // _d($item);
                $dados_orcamento_items[] = array(
                    'produto_id'   => $dados_cliente['produto_id'][$i],
                    'titulo'       => utf8_decode($dados_cliente['titulo'][$i]),
                    'qtde'         => $dados_cliente['qtde'][$i],
                    'data_cad'     => date('Y-m-d H:i:s')
                );
                
                $dados_item = array(
                    '<a href="'.URL.'/produto/'.$dados_cliente['alias'][$i].'">'.($dados_cliente['titulo'][$i]).'</a>',
                    $dados_cliente['qtde'][$i]
                );
                $html.= '<li>'.implode(' &bull; ', $dados_item);
                // $total+= $item->item->valor * $item->amount;
            }
            /*foreach($this->session->items as $key => $item){
                // _d($item);
                $dados_orcamento_items[] = array(
                    'produto_id'   => $item->item->id,
                    'titulo'       => $item->item->titulo,
                    'qtde'         => $item->qtde,
                    'data_cad'     => date('Y-m-d H:i:s')
                );
                
                $dados_item = array(
                    '<a href="'.URL.'/produto/'.$item->item->alias.'">'.($item->item->titulo).'</a>',
                    $item->qtde
                );
                $html.= '<li>'.implode(' &bull; ', $dados_item);
                // $total+= $item->item->valor * $item->amount;
            }*/
            // _d(array($dados_orcamento_items,$html));
            
            // $html.= '</ul><li><b>Total do pedido:</b> R$ '.$total.'</li></ul>';
            
            try {
                $orcamento_id = $this->orcamentos->insert($dados_orcamento);

                foreach($dados_orcamento_items as $doi){
                    $doi['orcamento_id'] = $orcamento_id;
                    $this->orcamento_itens->insert($doi);
                }


                $sub = 'Orçamento';//: '.$this->formatNumber($orcamento_id);
                $html = '<p>Novo pedido de orçamento realizado através do site: '.
                        // '<a href="'.URL.'/admin/acompanhamento/pedidos/?id='.$orcamento_id.
                        // '">#'.$this->formatNumber($orcamento_id).'</a>'.
                        '<br /><br /></p>'.$html;

                Trupe_CinCin_Mail::sendFollowUp($sub,$html,true);

                $this->reset();
            } catch(Exception $e){
                $err = 'Erro ao salvar pedido de orçamento.';
                
                if(APPLICATION_ENV!='production') {
                    _d($err.'\n'.$e->getMessage(),0);
                    _d($dados_orcamento,0);
                    _d($dados_orcamento_items);
                }

                return false;
            }
            
            return $orcamento_id;
        }
        
        return null;
    }
    
    /**
     * Formata o id do pedido para impressão
     */
    public function formatNumber($id)
    {
        return str_pad($id,6,'0',STR_PAD_LEFT);
    }
    
    /**
     * Pega dados de cobrança
     */
    public function getCobranca()
    {
        $obj = (array)Is_Array::utf8DbRow($this->cobranca->get());
        unset($obj['id']);
        unset($obj['data_cad']);
        unset($obj['data_edit']);
        unset($obj['user_cad']);
        unset($obj['user_edit']);
        return $obj;
    }
}
