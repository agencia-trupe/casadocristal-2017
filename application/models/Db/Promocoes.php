<?php

class Application_Model_Db_Promocoes extends ZendPlugin_Db_Table 
{
    protected $_name = "promocoes";
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }
    
    /**
     * Retorna as fotos da promoção
     *
     * @param int  $id    - id da promoção
     * @param bool $check - checar se promoção existe?
     *
     * @return array - rowset com fotos da promoção
     */
    public function getFotos1($id,$check=true)
    {
        $promocao = $this->fetchRow('id="'.$id.'"');

        if($check && !$promocao) return false;
        
        $fotos = array();
        
        if($promocao_fotos = $promocao->findDependentRowset('Application_Model_Db_PromocoesFotos')){
            foreach($promocao_fotos as $promocao_foto){
                $fotos[] = Is_Array::utf8DbRow($promocao_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }

    /**
     * Retorna fotos dos promocoes
     * 
     * @param array $promocoes - rowset de promocoes para associar fotos
     * @param bool  $withObjects - se irá retornar as fotos ou somente id
     * 
     * @return array of objects - promocoes com fotos
     */
    public function getFotos($promocoes,$withObjects=false)
    {
        $pids = array(); // ids de promocoes

        // identificando promocoes
        foreach($promocoes as $produto) $pids[] = $produto->id;

        if($withObjects){ // se o retorno for com objetos
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select->from('promocoes_fotos as fc',array('fc.promocao_id'))
                   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
                   ->where('fc.promocao_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
            
            $fotos = $select->query()->fetchAll();
            $fotos = array_map('Is_Array::toObject',$fotos);
        } else {
            $_promocoes_fotos = new Application_Model_Db_PromocoesFotos();
            $fotos = $_promocoes_fotos->fetchAll('promocao_id in ('.implode(',',$pids).')');
        }

        // associando fotos
        foreach($promocoes as &$produto){
            $produto->fotos = $this->getFotosSearch($produto->id,$fotos);
        }

        return $promocoes;
    }

    /**
     * Monta rowset de fotos com base no produto_id ($pid)
     */
    public function getFotosSearch($pid=null,$cats=array())
    {
        $fotos = array();

        foreach($cats as $cat) if($cat->promocao_id == $pid) $fotos[] = $cat;
        
        return $fotos;
    }
    
}