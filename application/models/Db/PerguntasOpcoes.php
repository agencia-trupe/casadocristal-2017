<?php

class Application_Model_Db_PerguntasOpcoes extends ZendPlugin_Db_Table 
{
    protected $_name = "perguntas_opcoes";
    
    protected $_dependentTables = array(
        'Application_Model_Db_Perguntas'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Perguntas' => array(
            'columns' => 'pergunta_id',
            'refTableClass' => 'Application_Model_Db_Perguntas',
            'refColumns'    => 'id'
        )
    );
    
    public function getOpcaoLiteral($value,$opcoes=null)
    {
        if(!(bool)$value) return null;
        
        if($opcoes){
            foreach($opcoes as $o) if($o->id==$value) return $o;
        } else {
            return $this->fetchRow('id='.$value);
        }
    }
}