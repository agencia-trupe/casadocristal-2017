<?php

class Application_Model_Db_ProdutosInfos extends Zend_Db_Table
{
    protected $_name = "produtos_infos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Produtos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Produtos' => array(
            'columns' => 'produto_id',
            'refTableClass' => 'Application_Model_Db_Produtos',
            'refColumns'    => 'id'
        )
    );
}
