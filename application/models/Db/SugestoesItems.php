<?php

class Application_Model_Db_SugestoesItems extends ZendPlugin_Db_Table
{
    protected $_name = "sugestoes_items";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Sugestoes');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Sugestoes' => array(
            'columns' => 'sugestao_id',
            'refTableClass' => 'Application_Model_Db_Sugestoes',
            'refColumns'    => 'id'
        )
    );
}