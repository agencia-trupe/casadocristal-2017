<?php

class Application_Model_Db_SugestoesCategorias extends ZendPlugin_Db_Table
{
    protected $_name = "sugestoes_categorias";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Sugestoes');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Sugestoes' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Sugestoes',
            'refColumns'    => 'categoria_id'
        )
    );
}