<?php

class Application_Model_Db_Portfolio extends Zend_Db_Table 
{
    protected $_name = "portfolio";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_PortfolioFotos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_PortfolioFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_PortfolioFotos',
            'refColumns'    => 'portfolio_id'
        ),
        'Application_Model_Db_PortfolioSugestoes' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_PortfolioSugestoes',
            'refColumns'    => 'portfolio_id'
        ),
    );
    
    /**
     * Retorna produto com suas imagens com base no alias se @alias for string ou id se @alias for numérico
     *
     * @param string|int $alias - valor do alias ou id do produto
     *
     * @return object|bool - objeto contendo o produto com suas imagens e categoria ou false se não for encontrado
     */
    public function getWithFotos($alias)
    {
        $column = is_numeric($alias) ? 'id' : 'alias';
        if(!$produto = $this->fetchRow($column.'="'.$alias.'"')){
            return false;
        }
        $fotos = array();
        
        if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_PortfolioFotos')){
            foreach($produto_fotos as $produto_foto){
                $fotos[] = $produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current();
            }
        }
        
        $object = Is_Array::utf8DbRow($produto);
        $object->fotos = $fotos;
        //_d($object);
        return $object;
    }
    
    /**
     * Retorna as fotos do produto
     *
     * @param int $id - id do produto
     *
     * @return array - rowset com fotos do produto
     */
    public function getFotos($id)
    {
        if(!$produto = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $fotos = array();
        
        if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_PortfolioFotos')){
            foreach($produto_fotos as $produto_foto){
                $fotos[] = ($produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }
    
    /**
     * Retorna quantidade total
     * 
     * @param string $where - string de seleção where, padrão NULL
     *
     * @return int
     */
    public function count($where=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from($this->_name,array('count(*) as cnt'));
        $count = $where ? $select->where($where)->query()->fetchAll() : $select->query()->fetchAll();
        return $count[0]['cnt'];
    }

    /**
     * Retorna as sugestoes do produto
     *
     * @param int  $id        - id do produto
     * @param bool $get_fotos - selecionar também fotos?
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do produto
     */
    public function getSugestoes($id,$get_fotos=false,$limit=null,$rand=false)
    {
        $_sugestoes = new Application_Model_Db_PortfolioSugestoes();
        $sugestoes = array();

        if(!(bool)$prods = $_sugestoes->fetchAll('portfolio_id="'.$id.'"')){
            return false;
        }

        foreach($prods as $ps){
            $s = Is_Array::utf8DbRow($this->fetchRow('id='.$ps->sugestao_id));
            if($s->status_id==1) $sugestoes[] = $s;
        }
        
        /*
        // saímos se o produto não existir
        if(!$produto = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $sugestoes = array();
        
        // montando a array de sugestões
        $select = $rand ? // randomizamos os resultados se for solicitado
                  $this->select()->order(new Zend_Db_Expr('RAND()'))->limit($limit) :
                  null;
        if($produto_sugestoes = $produto->findDependentRowset('Application_Model_Db_PortfolioSugestoes',null,$select)){
            foreach($produto_sugestoes as $produto_sugestao){
                $s = Is_Array::utf8DbRow($produto_sugestao->findDependentRowset('Application_Model_Db_Portfolio')->current());
                if($s->status_id==1) $sugestoes[] = $s;
            }
        }*/
        
        // pegamos as fotos se for solicitado
        if($get_fotos){
            $produtos_fotos = new Application_Model_Db_PortfolioFotos();
            
            foreach($sugestoes as &$sugestao){
                $sugestao->fotos = array();
                
                if($produto_fotos = $produtos_fotos->fetchAll('portfolio_id='.$sugestao->id)){    
                    foreach($produto_fotos as $produto_foto){
                        $sugestao->fotos[] = $produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current();
                    }
                }
            }
        }
        
        // randomizamos os resultados
        // - retirado para melhor performance caso haja muitas sugestões, solução adicionada acima
        //if($rand){ shuffle($sugestoes); }
        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;
    }

    /**
     * Retorna as tags do produto
     *
     * @param int  $id        - id do produto
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do produto
     */
    public function getTags($id,$limit=null,$rand=false)
    {
        $_sugestoes = new Application_Model_Db_PortfolioTags();
        $_tags = new Application_Model_Db_Tags();
        $sugestoes = array();

        if(!(bool)$prods = $_sugestoes->fetchAll('portfolio_id="'.$id.'"')){
            return false;
        }

        foreach($prods as $ps){
            $s = Is_Array::utf8DbRow($_tags->fetchRow('id='.$ps->tag_id));
            $sugestoes[] = $s;
        }
        
        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;
    }

    /**
     * Retorna as tags do produto pr alias
     *
     * @param int  $alias     - alias da tag
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do produto
     */
    public function getTagsByAlias($alias,$limit=null,$rand=false)
    {
        $_sugestoes = new Application_Model_Db_PortfolioTags();
        $_tags = new Application_Model_Db_Tags();
        $sugestoes = array();
        $alias = is_array($alias) ? implode('","',$alias) : $alias;

        if(!(bool)$tag = $_tags->fetchAll('alias in ("'.$alias.'")')){
            return false;
        }

        $tag_ids = array();
        foreach($tag as $t) $tag_ids[] = $t->id;

        if(!(bool)$prods = $_sugestoes->fetchAll('tag_id in ("'.(implode('","',$tag_ids)).'")')){
            return false;
        }

        foreach($prods as $ps){
            array_push($sugestoes,$ps->portfolio_id);
        }
        
        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;
    }
}