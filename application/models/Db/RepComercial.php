<?php

class Application_Model_Db_RepComercial extends ZendPlugin_Db_Table
{
    protected $_name = "rep_comercial";
    protected $default_order = 'ordem'; // ordem padrão para ordenação dos registros
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Fotos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Fotos' => array(
            'columns' => 'foto_id',
            'refTableClass' => 'Application_Model_Db_Fotos',
            'refColumns'    => 'id'
        )
    );
    
    /**
     * Retorna chave => valor ('id'=>valor)
     *
     * @param string $text  - campo a ser retornado como valor - padrão('descrição')
     * @param bool   $combo - se for true, adiciona um valor 'selecione...' para ser usado como combobox
     *
     * @return array
     */
    public function getKeyValues($text='descricao',$combo=false)
    {
        $values = $combo ? array("__none__"=>"Selecione...") : array();
        $rows = $this->fetchAll(null,'ordem');
        
        foreach($rows as $row){
            $values[$row->id] = utf8_encode($row->$text);
        }
        
        return $values;
    }
    
    /**
     * Retorna as fotos da categoira
     *
     * @param int $id - id da categoria
     *
     * @return array - rowset com fotos da categoria
     */
    public function getFotos($id)
    {
        if(!$promocao = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $fotos = array();
        
        if($promocao_fotos = $promocao->findDependentRowset('Application_Model_Db_Fotos')){
            foreach($promocao_fotos as $promocao_foto){
                $fotos[] = Is_Array::utf8DbRow($promocao_foto->current());
            }
        }
        
        return $fotos;
    }
}