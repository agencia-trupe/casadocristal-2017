<?php

class Application_Model_Db_Fotos extends Zend_Db_Table {
    protected $_name = "fotos";
    
    protected $_dependentTables = array(
        'Application_Model_Db_ProdutosFotos',
        'Application_Model_Db_ClippingFotos',
        'Application_Model_Db_PromocoesFotos',
        'Application_Model_Db_DestaquesFotos',
        'Application_Model_Db_Categorias',
        'Application_Model_Db_UsuariosFotos',
        'Application_Model_Db_CasosClinicosFotos',
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_ProdutosFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_ProdutosFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_NoticiasFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_NoticiasFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_ClippingFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_ClippingFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_PromocoesFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_PromocoesFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_DestaquesFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_DestaquesFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_Categorias' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Categorias',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_RepComercial' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_RepComercial',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_PaginaHomeFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_PaginaHomeFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_UsuariosFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_UsuariosFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_CasosClinicosFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_CasosClinicosFotos',
            'refColumns'    => 'foto_id'
        )
    );
}
