<?php

class Application_Model_Db_ExamesFotos extends Zend_Db_Table
{
    protected $_name = "exames_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array(
        'Application_Model_Db_Fotos',
        'Application_Model_Db_Exames'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Fotos' => array(
            'columns' => 'foto_id',
            'refTableClass' => 'Application_Model_Db_Fotos',
            'refColumns'    => 'id'
        ),
        'Application_Model_Db_Exames' => array(
            'columns' => 'exame_id',
            'refTableClass' => 'Application_Model_Db_Exames',
            'refColumns'    => 'id'
        )
    );
}
