<?php

class Application_Model_Db_Orcamentos extends ZendPlugin_Db_Table 
{
    protected $_name = "orcamentos";
    public static $formas = array(
        'b' => 'Boleto',
        'd' => 'Débito',
        'c' => 'Crédito'
    );
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Clientes');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Clientes' => array(
            'columns' => 'cliente_id',
            'refTableClass' => 'Application_Model_Db_Clientes',
            'refColumns'    => 'id'
        )
    );

    /**
     * Retorna pedidos com joins
     * 
     * @param array $where - Array de wheres p/ cada tabela do join
     *                       Ex.: array(
     *                           'usuarios' => 'status_id = 1',
     *                           'pedidos' => 'status_id = 1'
     *                       )
     * 
     * @param array $order - Array de orders p/ cada tabela do join
     *                       Ex.: array(
     *                           'usuarios' => 'nome asc',
     *                           'pedidos' => 'id desc'
     *                       )
     * 
     * @param int $limit
     * @param int $offset
     */
    public function getAll($where=null,$order=null,$limit=null,$offset=null)
    {
        if((!is_array($where) && $where !== null) ||
           (!is_array($order) && $order !== null))
            throw new Exception("Parâmetro 'where' e 'order' precisam ser do tipo Array", 1);

        $usuarios = new Application_Model_Db_Usuario();
        $pedidos = new Application_Model_Db_Pedidos();
        $pedidos_status = new Application_Model_Db_PedidosStatus();
        $pedidos_items = new Application_Model_Db_PedidosItems();

        $_clientes = array();
        $_pedidos = array();
        $_pedidos_status = array();
        $_exames = array();

        $rows = Is_Array::utf8DbResult(
            $pedidos_items->fetchAll(
                $this->_has('pedidos_items',$where),
                $this->_has('pedidos_items',$order),
                $limit,
                $offset
            )
        );
        
        if(count($rows)){
            foreach ($rows as &$row){
                // pegando pedido
                if(!$this->_has($row->pedido_id,$_pedidos)){
                    $w = 'id='.$row->pedido_id;
                    if($this->_has('pedidos',$where)) $w.= ' and '.$where['pedidos'];
                    $o = $this->_has('pedidos',$order);

                    $_pedidos[$row->pedido_id] = $this->fetchRow($w,$o);
                }

                // pegando cliente
                $cliente_id = $_pedidos[$row->pedido_id]->cliente_id;
                if(!$this->_has($cliente_id,$_clientes)){
                    $w = 'id='.$cliente_id;
                    if($this->_has('usuarios',$where)) $w.= ' and '.$where['usuarios'];
                    $o = $this->_has('usuarios',$order);

                    $_clientes[$cliente_id] = $usuarios->fetchRow($w,$o);
                }

                // pegando status
                $pedido_status_id = $_pedidos[$row->pedido_id]->pedido_status_id;
                if(!$this->_has($pedido_status_id,$_pedidos_status)){
                    $w = 'id='.$pedido_status_id;
                    if($this->_has('pedidos_status',$where)) $w.= ' and '.$where['pedidos_status'];
                    $o = $this->_has('pedidos_status',$order);

                    $_pedidos_status[$pedido_status_id] = $pedidos_status->fetchRow($w,$o);
                }

                $row->pedido = Is_Array::utf8DbRow($_pedidos[$row->pedido_id]);
                $row->cliente = Is_Array::utf8DbRow($_clientes[$cliente_id]);
                $row->pedido_status = Is_Array::utf8DbRow($_pedidos_status[$pedido_status_id]);
            }
        }
        _d($rows);
        return $rows;

        /* *************************** v2 ***************************
        $usuarios = new Application_Model_Db_Usuario();
        $pedidos = new Application_Model_Db_Pedidos();
        $pedidos_status = new Application_Model_Db_PedidosStatus();
        $pedidos_items = new Application_Model_Db_PedidosItems();

        $rows = Is_Array::utf8DbResult(
            $this->fetchAll($where,$order,$limit,$offset)
        );

        if(count($rows)){
            foreach ($rows as &$row){
                $row->cliente = Is_Array::utf8DbRow(
                    $usuarios->fetchRow('id='.$row->cliente_id)
                );
                $row->pedido_status = Is_Array::utf8DbRow(
                    $pedidos_status->fetchRow('id='.$row->pedido_status_id)
                );
                $row->exames = Is_Array::utf8DbResult(
                    $pedidos_items->fetchAll('pedido_id='.$row->id)
                );
            }
        }

        return $rows;
        ****************************** v2 *************************** */

        /* *************************** v1 ***************************
        global $dbAdapter; $db = $dbAdapter;

        $query = 'select * from pedidos as p';
        if($where) $query.= ' where '.$where;

        $query.= ' left join usuarios as u on u.id = p.cliente_id';
        $query.= ' left join pedidos_status as ps on ps.id = p.pedido_status_id';

        if($order) $query.= ' order by '.$order;
        if($limit) $query.= ' limit '.$limit;
        if($offset&&$limit) $query.= ','.$offset;

        _d($db->query($query)->fetchAll());
        ****************************** v1 *************************** */
    }

    public function _has($need,$stack=null)
    {
        if($stack===null && !is_array($stack)) return null;

        return array_key_exists($need,$stack) ? $stack[$need] : null;
    }

    public function getForma($forma)
    {
        return self::$formas[$forma];
    }
}
