<?php
/**
 * Modelo da tabela de usuarios
 */
class Application_Model_Db_Usuario extends ZendPlugin_Db_Table 
{
    protected $_name = "usuarios";
    protected $_login_column = "email";
    public $_role = '0,1';
    public $regioes = array(
        'ZC' => 'Zona Central',
        'ZN' => 'Zona Norte',
        'ZS' => 'Zona Sul',
        'ZL' => 'Zona Leste',
        'ZO' => 'Zona Oeste',
    );
    
    protected $_dependentTables = array(
        'Application_Model_Db_UsuariosFotos',
        'Application_Model_Db_CasosClinicos',
        'Application_Model_Db_Avaliacoes'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_UsuariosFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_UsuariosFotos',
            'refColumns'    => 'usuario_id'
        ),
        'Application_Model_Db_CasosClinicos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_CasosClinicos',
            'refColumns'    => 'usuario_id'
        ),
        'Application_Model_Db_Avaliacoes' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Avaliacoes',
            'refColumns'    => 'avaliador_id'
        )
    );
    
    public function setRole($role)
    {
        $this->_role = $role;
        return $this;
    }
    
    // Finds
    public function findByLoginAtivo($email,$password,$md5=true)
    {
        $rows = $this->fetchAll($this->select()
            ->where('email = ? or login = ?',$email)
            // ->where('email = "'.$email.'" or login = "'.$email.'" or crm = "'.$email.'"')
            ->where('senha = ?',$md5?md5($password):$password)
            ->where("status_id = ?",1)
            ->where("role in (".$this->_role.")")
            ->limit(1,0));
        return count($rows) ? $rows->current() : false;
    }
    
    public function findByLogin($email,$password,$md5=true)
    {
        $rows = $this->fetchAll($this->select()
            ->where($this->_login_column.' = ?',$email)
            ->where('senha = ?',$md5 ? md5($password) : $password)
            ->limit(1,0));
        return count($rows) ? $rows->current() : false;
    }
    
    public function findByEmail($email)
    {
        //$rows = $this->fetchAll($this->select()->where('email = ?',$email)->limit(1));
        //return count($rows) ? $rows->current() : false;
        return $this->fetchRow('email = "'.$email.'"');
    }
    
    /**
     * Retorna chave => valor ('id'=>valor)
     *
     * @param string $text  - campo a ser retornado como valor - padrão('descrição')
     * @param bool   $combo - se for true, adiciona um valor 'selecione...' para ser usado como combobox
     *
     * @return array
     */
    /*public function getKeyValues($text='nome',$combo=true,$where=null)
    {
        $values = $combo ? array("__none__"=>"Selecione...") : array();
        $rows = $this->fetchAll($where,'nome');
        
        foreach($rows as $row){
            $values[$row->id] = utf8_encode($row->$text);
        }
        
        return $values;
    }*/
    
    
    public function getRoles()
    {
        return array(
			// 0 => 'Super Admin',
			1 => 'Administrador',
			2 => 'Usuário',
			3 => 'Atendimento'
		);
    }
    
	public function getRole($role)
	{
		$roles = self::getRoles();		
		return $roles[$role];
	}
    
    /**
     * Retorna a foto do usuario
     *
     * @param int $id - id do usuario
     *
     * @return object|null - foto do usuario
     */
    public function getFoto($id)
    {
        $table = new Application_Model_Db_UsuariosFotos();
        $foto = null;
        
        if($usuario_foto = $table->fetchRow('usuario_id='.$id)){
            $foto = Is_Array::utf8DbRow($usuario_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
        }
        
        return $foto;
    }
}