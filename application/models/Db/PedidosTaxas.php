<?php

class Application_Model_Db_PedidosTaxas extends Zend_Db_Table 
{
    protected $_name = "pedidos_taxas";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Pedidos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Pedidos' => array(
            'columns' => 'pedido_id',
            'refTableClass' => 'Application_Model_Db_Pedidos',
            'refColumns'    => 'id'
        )
    );
    
}