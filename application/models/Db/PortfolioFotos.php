<?php

class Application_Model_Db_PortfolioFotos extends Zend_Db_Table
{
    protected $_name = "portfolio_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array(
        'Application_Model_Db_Portfolio',
        'Application_Model_Db_Fotos'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Portfolio' => array(
            'columns' => 'portfolio_id',
            'refTableClass' => 'Application_Model_Db_Portfolio',
            'refColumns'    => 'id'
        ),
        'Application_Model_Db_Fotos' => array(
            'columns' => 'foto_id',
            'refTableClass' => 'Application_Model_Db_Fotos',
            'refColumns'    => 'id'
        )
    );
}
