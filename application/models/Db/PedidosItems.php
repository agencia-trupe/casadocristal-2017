<?php

class Application_Model_Db_PedidosItems extends Zend_Db_Table 
{
    protected $_name = "pedidos_items";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Pedidos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Pedidos' => array(
            'columns' => 'pedido_id',
            'refTableClass' => 'Application_Model_Db_Pedidos',
            'refColumns'    => 'id'
        )
    );
    
}