<?php

class Application_Model_Db_FaqPerguntas extends Zend_Db_Table 
{
    protected $_name = "faq_perguntas";
    
    protected $_dependentTables = array(
        'Application_Model_Db_Faq'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Faq' => array(
            'columns' => 'faq_id',
            'refTableClass' => 'Application_Model_Db_Faq',
            'refColumns'    => 'id'
        )
    );
}