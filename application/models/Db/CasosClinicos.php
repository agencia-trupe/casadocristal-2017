<?php

class Application_Model_Db_CasosClinicos extends Zend_Db_Table 
{
    protected $_name = "casos_clinicos";
    
    protected $_dependentTables = array(
        'Application_Model_Db_Campanha',
        'Application_Model_Db_Usuario',
        'Application_Model_Db_CasosClinicosFotos',
        'Application_Model_Db_CasosClinicosMensagem'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Campanha' => array(
            'columns' => 'campanha_id',
            'refTableClass' => 'Application_Model_Db_Campanha',
            'refColumns'    => 'id'
        ),
        'Application_Model_Db_Usuario' => array(
            'columns' => 'usuario_id',
            'refTableClass' => 'Application_Model_Db_Usuario',
            'refColumns'    => 'id'
        ),
        'Application_Model_Db_CasosClinicosFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_CasosClinicosFotos',
            'refColumns'    => 'casos_clinicos_id'
        ),
        'Application_Model_Db_CasosClinicosMensagem' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_CasosClinicosMensagem',
            'refColumns'    => 'casos_clinicos_id'
        )
    );
    
    public function getStatus($id=null)
    {
        $status = array(
            '1' => 'novo',
            '2' => 'aguardando autor',
            '3' => 'em avaliação',
            '4' => 'avaliado',
            '5' => 'aguardando ADM'
        );
        
        if($id===null) return $status;
        return array_key_exists($id,$status) ? $status[$id] : null;
    }
}