<?php
/**
 * Modelo da página home
 */
class Application_Model_Db_PaginaHome extends Zend_Db_Table 
{
    protected $_name = "pagina_home";
    
    protected $_dependentTables = array('Application_Model_Db_PaginaHomeFotos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_PaginaHomeFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_PaginaHomeFotos',
            'refColumns'    => 'pagina_id'
        )
    );
    
    /**
     * Retorna as fotos do produto
     *
     * @param int $id - id do produto
     *
     * @return array - rowset com fotos do produto
     */
    public function getFotos($id)
    {
        if(!$produto = $this->fetchRow('id="'.$id.'"')) return false;
        $fotos = array();
        
        if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_PaginaHomeFotos')){
            foreach($produto_fotos as $produto_foto){
                $fotos[] = Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }
}