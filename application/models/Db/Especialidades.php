<?php

class Application_Model_Db_Especialidades extends Zend_Db_Table
{
    protected $_name = "especialidades";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Usuario');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Usuario' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Usuario',
            'refColumns'    => 'especialidade_id'
        )
    );
    
    /**
     * Retorna chave => valor ('id'=>valor)
     *
     * @param string $text  - campo a ser retornado como valor - padrão('descrição')
     * @param bool   $combo - se for true, adiciona um valor 'selecione...' para ser usado como combobox
     *
     * @return array
     */
    public function getKeyValues($text='descricao',$combo=false)
    {
        $values = $combo ? array("__none__"=>"Selecione...") : array();
        $rows = $this->fetchAll(null,'descricao');
        
        foreach($rows as $row){
            $values[$row->id] = utf8_encode($row->$text);
        }
        
        return $values;
    }
}