<?php
/**
 * Modelo da tabela de usuarios
 */
class Application_Model_Db_UsuarioCod extends ZendPlugin_Db_Table 
{
    protected $_name = "usuario_cod";
    protected $_primary = "cod";
    public $padLen = 5;
    
    protected $_dependentTables = array('Application_Model_Db_Usuario');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Usuario' => array(
            'columns' => 'usuario_id',
            'refTableClass' => 'Application_Model_Db_Usuario',
            'refColumns'    => 'id'
        )
    );
    
    public function isLpadded($n){
        $n = trim($n);
        return strlen($n)==$this->padLen && $n[0]=='0';
    }
}