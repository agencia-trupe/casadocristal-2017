<?php

class Application_Model_Db_Perguntas extends Zend_Db_Table 
{
    protected $_name = "perguntas";
    
    protected $_dependentTables = array(
        'Application_Model_Db_Campanha',
        'Application_Model_Db_PerguntasOpcoes',
        'Application_Model_Db_Respostas',
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Campanha' => array(
            'columns' => 'campanha_id',
            'refTableClass' => 'Application_Model_Db_Campanha',
            'refColumns'    => 'id'
        ),
        'Application_Model_Db_PerguntasOpcoes' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_PerguntasOpcoes',
            'refColumns'    => 'pergunta_id'
        ),
        'Application_Model_Db_Respostas' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Respostas',
            'refColumns'    => 'pergunta_id'
        )
    );
}