<?php

class Application_Model_Db_OrcamentoStatus extends Zend_Db_Table 
{
    protected $_name = "orcamento_status";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Orcamentos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Orcamentos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Orcamentos',
            'refColumns'    => 'orcamento_status_id'
        )
    );
    
    /**
     * Retorna chave => valor ('id'=>valor)
     *
     * @param string $text  - campo a ser retornado como valor - padrão('descrição')
     * @param bool   $combo - se for true, adiciona um valor 'selecione...' para ser usado como combobox
     *
     * @return array
     */
    public function getKeyValues($text='descricao',$combo=false)
    {
        $values = $combo ? array("__none__"=>"Selecione...") : array();
        $rows = Is_Array::utf8DbResult($this->fetchAll(null,'id'));
        
        foreach($rows as $row){
            $values[$row->id] = $row->$text;
        }
        
        return $values;
    }
}