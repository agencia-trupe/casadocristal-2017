<?php

class Application_Model_Db_UsuariosFotos extends Zend_Db_Table
{
    protected $_name = "usuarios_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array(
        'Application_Model_Db_Fotos',
        'Application_Model_Db_Usuario'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Fotos' => array(
            'columns' => 'foto_id',
            'refTableClass' => 'Application_Model_Db_Fotos',
            'refColumns'    => 'id'
        ),
        'Application_Model_Db_Usuario' => array(
            'columns' => 'usuario_id',
            'refTableClass' => 'Application_Model_Db_Usuario',
            'refColumns'    => 'id'
        )
    );
}
