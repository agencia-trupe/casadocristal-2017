<?php

class Application_Model_Db_Consultas extends ZendPlugin_Db_Table 
{
    protected $_name = "consultas";
    public static $status = array(
        '0'=>'Aberta',
        '1'=>'Agendada',
        '2'=>'Confirmada',
        '3'=>'Cancelada',
        '4'=>'Reagendada'
    );
    
    protected $_dependentTables = array(
        'Application_Model_Db_ExamesFotos',
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_ExamesFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_ExamesFotos',
            'refColumns'    => 'exame_id'
        )
    );

    public function getStatus($k=null)
    {
        return $k===null ? self::$status : self::$status[$k];
    }
}