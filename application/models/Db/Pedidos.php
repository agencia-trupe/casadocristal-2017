<?php

class Application_Model_Db_Pedidos extends ZendPlugin_Db_Table 
{
    protected $_name = "pedidos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Clientes');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Clientes' => array(
            'columns' => 'cliente_id',
            'refTableClass' => 'Application_Model_Db_Clientes',
            'refColumns'    => 'id'
        )
    );
}