<?php

class Application_Model_Db_PaginasArquivos extends Zend_Db_Table {
    protected $_name = "paginas_arquivos";
    
    protected $_dependentTables = array('Application_Model_Db_Paginas');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Paginas' => array(
            'columns' => 'pagina_id',
            'refTableClass' => 'Application_Model_Db_Paginas',
            'refColumns'    => 'id'
        )
    );
}