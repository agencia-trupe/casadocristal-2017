<?php

class Application_Model_Db_PedidosVp extends Zend_Db_Table 
{
    protected $_name = "pedidos_vp";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_PedidosItems');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Pedidos' => array(
            'columns' => 'pedido_item_id',
            'refTableClass' => 'Application_Model_Db_PedidosItems',
            'refColumns'    => 'id'
        )
    );
    
    public function genCode($max=10)
    {
        $dup = true;
        
        while($dup){
            $cod = self::_genPass($max);
            if(!count($this->fetchAll('cod="'.$cod.'"'))) $dup = false;
        }
        
        return $cod;
    }
    
    public function _genPass($maxchars=10)
    {
        $alfa = "abcdefghijklmnopqrstuvwxyz";
        $num  = "0123456789";
        $hash = array();
        
        for($i=0;$i<$maxchars;$i++){
            $j = $i % 2 == 0 ? rand(0,(strlen($alfa)-1)) : rand(0,(strlen($num)-1));
            $hash[] = $i % 2 == 0 ? $alfa[$j] : $num[$j];
        }
        
        array_rand($hash);
        
        return implode("",$hash);
    }
    
}