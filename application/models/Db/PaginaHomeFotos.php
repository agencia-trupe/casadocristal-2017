<?php

class Application_Model_Db_PaginaHomeFotos extends Zend_Db_Table {
    protected $_name = "pagina_home_fotos";
    
    protected $_dependentTables = array('Application_Model_Db_PaginaHome');
    
    protected $_referenceMap = array(
        'Application_Model_Db_PaginaHome' => array(
            'columns' => 'pagina_id',
            'refTableClass' => 'Application_Model_Db_PaginaHome',
            'refColumns'    => 'id'
        )
    );
    
    /**
     * Retorna as fotos do produto
     *
     * @param int $id - id do produto
     *
     * @return array - rowset com fotos do produto
     */
    public function getFotos($id,$tipo=null)
    {
        $where = 'pagina_id="'.$id.'"'.($tipo?' and tipo="'.$tipo.'"':'');
        $order = 'id desc';//null;
        if(!$homeFotos = $this->fetchAll($where,$order)) return false;
        $fotos = array();
        
        if(count($homeFotos)){
            foreach($homeFotos as $produto_foto){
                $fotos[] = Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }
}