<?php

class Application_Model_Db_Sugestoes extends ZendPlugin_Db_Table
{
    protected $_name = "sugestoes";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_SugestoesItems','Application_Model_Db_SugestoesCategorias');
    
    protected $_referenceMap = array(
        'Application_Model_Db_SugestoesItems' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_SugestoesItems',
            'refColumns'    => 'sugestao_id'
        ),
        'Application_Model_Db_SugestoesCategorias' => array(
            'columns' => 'categoria_id',
            'refTableClass' => 'Application_Model_Db_SugestoesCategorias',
            'refColumns'    => 'id'
        )
    );
    
    /**
     * Retorna sugestões com todos os subitens
     */
    public function getFull($where=null,$order=null,$limit=null,$offset=null){
        if($rows = $this->fetchAll($where,$order,$limit,$offset)){
            $sugestoes = Is_Array::utf8DbResult($rows);
            
            for($i=0;$i<sizeof($rows);$i++){
                if($items = $rows[$i]->findDependentRowset('Application_Model_Db_SugestoesItems')){
                    $sugestoes[$i]->items = Is_Array::utf8DbResult($items);
                    
                    for($j=0;$j<sizeof($items);$j++){
                        $produto = $items[$j]->findDependentRowset('Application_Model_Db_Produtos');
                        $foto = count($produto) ? $produto->current()->findDependentRowset('Application_Model_Db_ProdutosFotos') : null;
                        $foto = count($foto) ? $foto->current()->findDependentRowset('Application_Model_Db_Fotos') : null;
                        
                        $sugestoes[$i]->items[$j]->produto = count($produto) ? Is_Array::utf8DbRow($produto->current()) : null;
                        $sugestoes[$i]->items[$j]->produto->foto = count($foto) ? Is_Array::utf8DbRow($foto->current()) : null;
                    }
                } else {
                    $sugestoes[$i]->items = null;
                }
            }
            
            return $sugestoes;
        }
        
        return null;
    }
}