<?php
/**
 * Modelo da tabela de usuarios
 */
class Application_Model_Db_TrackDownloads extends Zend_Db_Table 
{
    protected $_name = "track_downloads";
    
    protected $_dependentTables = array('Aluno_Model_Pessoa');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Pessoa' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Pessoa',
            'refColumns'    => 'usuario_id'
        )
    );
    
}