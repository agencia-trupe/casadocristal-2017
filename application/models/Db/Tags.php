<?php

class Application_Model_Db_Tags extends Zend_Db_Table
{
    protected $_name = "tags";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_PortfolioTags');
    
    protected $_referenceMap = array(
        'Application_Model_Db_PortfolioTags' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_PortfolioTags',
            'refColumns'    => 'tag_id'
        )
    );
}
