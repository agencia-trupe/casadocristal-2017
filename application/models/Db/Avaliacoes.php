<?php

class Application_Model_Db_Avaliacoes extends ZendPlugin_Db_Table
{
    protected $_name = "avaliacoes";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Usuario');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Usuario' => array(
            'columns' => 'avaliador_id',
            'refTableClass' => 'Application_Model_Db_Usuario',
            'refColumns'    => 'id'
        )
    );
}