<?php

class Application_Model_Db_PortfolioTags extends Zend_Db_Table
{
    protected $_name = "portfolio_tags";
    
    /**
     * Referências
     */
    protected $_dependentTables = array(
        'Application_Model_Db_Portfolio',
        'Application_Model_Db_Tags'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Portfolio' => array(
            'columns' => 'portfolio_id',
            'refTableClass' => 'Application_Model_Db_Portfolio',
            'refColumns'    => 'id'
        ),
        'Application_Model_Db_Tags' => array(
            'columns' => 'tag_id',
            'refTableClass' => 'Application_Model_Db_Tags',
            'refColumns'    => 'id'
        )
    );
}
