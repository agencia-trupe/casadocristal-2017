<?php

class Application_Model_Db_CobrancaPagSeguro extends Zend_Db_Table 
{
    protected $_name = "cobranca_pagseguro";
    
    public function get()
    {
        return $this->fetchAll(null,'id desc',1)->current();
    }
}