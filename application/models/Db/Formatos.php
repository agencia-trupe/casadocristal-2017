<?php

class Application_Model_Db_Formatos extends Zend_Db_Table
{
    protected $_name = "formatos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_ProdutoFormato');
    
    protected $_referenceMap = array(
        'Application_Model_Db_ProdutoFormato' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_ProdutoFormato',
            'refColumns'    => 'formato_id'
        )
    );
}
