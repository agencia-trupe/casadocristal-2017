<?php

class Application_Model_Db_Gramaturas extends Zend_Db_Table
{
    protected $_name = "gramaturas";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_ProdutoGramatura');
    
    protected $_referenceMap = array(
        'Application_Model_Db_ProdutoGramatura' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_ProdutoGramatura',
            'refColumns'    => 'formato_id'
        )
    );
}
