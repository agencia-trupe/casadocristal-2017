<?php

class Application_Model_Db_CasosClinicosFotos extends Zend_Db_Table 
{
    protected $_name = "casos_clinicos_fotos";
    
    protected $_dependentTables = array(
        'Application_Model_Db_CasosClinicos'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_CasosClinicos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_CasosClinicos',
            'refColumns'    => 'casos_clinicos_id'
        )
    );
}