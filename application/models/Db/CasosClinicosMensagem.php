<?php

class Application_Model_Db_CasosClinicosMensagem extends Zend_Db_Table 
{
    protected $_name = "casos_clinicos_mensagem";
    
    protected $_dependentTables = array(
        'Application_Model_Db_CasosClinicos'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_CasosClinicos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_CasosClinicos',
            'refColumns'    => 'caso_clinico_id'
        )
    );
}