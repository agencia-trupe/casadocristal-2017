<?php

class Application_Model_Db_Arquivos extends Zend_Db_Table {
    protected $_name = "arquivos";
    
    protected $_dependentTables = array(
        'Application_Model_Db_CnbAudiosAudio',
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_CnbAudiosAudio' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_CnbAudiosAudio',
            'refColumns'    => 'arquivo_id'
        )
    );
}
