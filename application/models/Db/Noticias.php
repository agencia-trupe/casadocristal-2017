<?php

class Application_Model_Db_Noticias extends ZendPlugin_Db_Table 
{
    protected $_name = "noticias";
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }

    /**
     * Retorna as fotos do da noticia
     *
     * @param int $id - id da noticia
     *
     * @return array - rowset com fotos da noticia
     */
    public function getFotosById($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('noticias_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('noticia_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    /**
     * Retorna fotos dos noticias
     * 
     * @param array $noticias - rowset de noticias para associar fotos
     * @param bool  $withObjects - se irá retornar as fotos ou somente id
     * 
     * @return array of objects - noticias com fotos
     */
    public function getFotos($noticias,$withObjects=false)
    {
        $pids = array(); // ids de noticias

        // identificando noticias
        foreach($noticias as $noticia) $pids[] = $noticia->id;

        if($withObjects){ // se o retorno for com objetos
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select->from('noticias_fotos as fc',array('fc.noticia_id'))
                   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
                   ->where('fc.noticia_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
            
            $fotos = $select->query()->fetchAll();
            $fotos = array_map('Is_Array::toObject',$fotos);
        } else {
            $_noticias_fotos = new Application_Model_Db_NoticiasFotos();
            $fotos = $_noticias_fotos->fetchAll('noticia_id in ('.implode(',',$pids).')');
        }

        // associando fotos
        foreach($noticias as &$noticia){
            $noticia->fotos = $this->getFotosSearch($noticia->id,$fotos);
        }

        return $noticias;
    }

    /**
     * Monta rowset de fotos com base no noticia_id ($pid)
     */
    public function getFotosSearch($pid=null,$cats=array())
    {
        $fotos = array();

        foreach($cats as $cat) if($cat->noticia_id == $pid) $fotos[] = $cat;
        
        return $fotos;
    }
    
}