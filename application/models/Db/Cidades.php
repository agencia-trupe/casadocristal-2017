<?php

class Application_Model_Db_Cidades extends Zend_Db_Table
{
    protected $_name = "cidades";
    protected $_primary = "cod_cidades";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Produtos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Produtos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Produtos',
            'refColumns'    => 'categoria_id'
        )
    );
    
    /**
     * Retorna chave => valor ('id'=>valor)
     *
     * @param string $text  - campo a ser retornado como valor - padrão('descrição')
     * @param bool   $combo - se for true, adiciona um valor 'selecione...' para ser usado como combobox
     *
     * @return array
     */
    public function getKeyValues($key='id',$text='nome',$combo=false)
    {
        $values = $combo ? array("__none__"=>"Selecione...") : array();
        $rows = $this->fetchAll(null,$text);
        
        foreach($rows as $row){
            $values[$row->$key] = utf8_encode($row->$text);
        }
        
        return $values;
    }
}