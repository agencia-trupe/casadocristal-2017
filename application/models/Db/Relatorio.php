<?php

class Application_Model_Relatorio
{
    // $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
    // global $dbAdapter; $db = $dbAdapter;
    // $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    // $session = Zend_Session::getId();

    public $relatorio_tipos = array(
        '3' => 'Buscas no site',
        '1' => 'Faturamento'
    );

    public $relatorio_padrao = '2';

    // lista de extensões que não devem entrar no relatório
    const url_deny = '.ico,.css,.less,.js,.json,.xml,.jpg,.jpeg,.png,.gif,.bmp,.swf,function(';

    public function __construct()
    {
        // $this->usuarios = new Application_Model_Db_Usuario();
        // $this->report = new Application_Model_Db_TrackAccess();
        // $this->report_downloads = new Application_Model_Db_TrackDownloads();
        $this->select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
    }

    public function setAccess($user,$url=null)
    {
        global $dbAdapter; $db = $dbAdapter;
        $url || $url = reset(explode('?','http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));

        $query = 'insert into track_access(url,user_id,session_id,data) values('.
                "'".$url."',".
                "'".$user."',".
                "'".Zend_Session::getId()."',".
                "'".date('Y-m-d H:i:s')."'"
            .') on duplicate key update counter = counter+1';

        try {
            if(self::allowUrl($url)) $db->query($query);
        } catch(Exception $e){
            if(APPLICATION_ENV=='development') _d($query.'<br>'.$e->getMessage());
        }
    }

    public function setDownload($data=array())
    {
        global $dbAdapter; $db = $dbAdapter;

        if(!isset($data['data'])) $data['data'] = date('Y-m-d H:i:s');
        if(!isset($data['session_id'])) $data['session_id'] = Zend_Session::getId();
        if(!isset($data['counter'])) $data['counter'] = 1;

        $into = array(); $values = array();
        foreach($data as $k => $v){
            $into[] = $k;
            $values[] = $db->quote($v);
        }

        $query = 'insert into track_downloads('.
                implode(',',$into)
            .') values('.
                implode(',',$values)
            .') on duplicate key update counter = counter+1';

        try {
            $db->query($query);
        } catch(Exception $e){
            if(APPLICATION_ENV=='development') _d($query.'<br>'.$e->getMessage());
        }
    }

    public function allowUrl($url)
    {
        $sufix = end(explode('.',$url)); // extensão da url
        $sufixes_deny = explode(',',self::url_deny);

        foreach($sufixes_deny as $d) if(strstr($url,$d)) return false;

        return true;
    }

    public function getTipo($tipo=null)
    {
        return $tipo ? $this->relatorio_tipos[$tipo] : $this->relatorio_tipos;
    }

    public function replaceUrl($url)
    {
        return str_replace(array(
            'http://'.URL,
            'http://'.SITE_NAME,
            'http://www.'.SITE_NAME.'.com.br',
            'http://'.SITE_NAME.'.com.br'
        ), '', $url);
    }

    /* Reports */
    public function getReport($tipo=null,$params=array())
    {
        if($tipo===null) return array();

        global $dbAdapter; $db = $dbAdapter;
        $select = $this->select->reset();

        switch($tipo){
            case '1':
                $select->from('pedidos as p',array('p.id','p.data_cad'))
                    ->joinRight('pedidos_items as pi','pi.pedido_id = p.id',array('sum(pi.produto_valor * pi.qtde) as valor'))
                    ->joinRight('pedidos_taxas as pt','pt.pedido_id = p.id',array('sum(pt.taxa_valor) as taxas'));
                
                if((bool)@$params['data_from'])
                    $select->where('p.data_cad >= '.$db->quote(Is_Date::br2am($params['data_from']).' 00:00:00'));
                
                if((bool)@$params['data_to'])
                    $select->where('p.data_cad <= '.$db->quote(Is_Date::br2am($params['data_to']).' 23:59:59'));

                $select->group('p.id');
                // $select->order('p.id desc');

                break;

            case '2':
                $select->from('usuarios as u',array(
                        'date_format(u.data_cad,"%Y-%m-%d") as data',
                        'u.atividade_profissional_id',
                        'u.escolaridade_id',
                        'count(u.id) as count_records'
                        )
                    )
                    ->where('u.role in (2)');
                
                if((bool)@$params['data_from'])
                    $select->where('u.data_cad >= '.$db->quote(Is_Date::br2am($params['data_from']).' 00:00:00'));
                
                if((bool)@$params['data_to'])
                    $select->where('u.data_cad <= '.$db->quote(Is_Date::br2am($params['data_to']).' 23:59:59'));

                
                if((bool)@$params['filter']){
                    if($params['filter']=='escolaridade_id'){
                        $select->group('u.escolaridade_id');
                        $select->order((bool)@$params['order']?$params['order']:array('u.escolaridade_id desc','u.escolaridade_completo desc'));
                    } else {
                        $select->group('atividade_profissional_id');
                        $select->order((bool)@$params['order']?$params['order']:'u.atividade_profissional_id desc');
                    }
                } else {
                    $select->group('data');
                    $select->order((bool)@$params['order']?$params['order']:'u.data_cad desc');
                }

                break;

            case '3':
                $select->from('buscas as b',array('*'))
                    ->order('counter desc');
                
                if((bool)@$params['data_from'])
                    $select->where('b.data_edit >= '.$db->quote(Is_Date::br2am($params['data_from']).' 00:00:00'));
                
                if((bool)@$params['data_to'])
                    $select->where('b.data_edit <= '.$db->quote(Is_Date::br2am($params['data_to']).' 23:59:59'));

                break;
        }
        // _d($select->query());
        $_rows = $select->query()->fetchAll();
        $rows = array();

        if(count($_rows)){
            for($i=0;$i<sizeof($_rows);$i++){
                $r = Is_Array::utf8All($_rows[$i]);
                $r = Is_Array::toObject($r);

                $rows[$i] = $r;
            }
        }

        return $rows;
    }

    public function getAccess($url=null,$unique=false)
    {
        if(!$url) return 0;
        global $dbAdapter; $db = $dbAdapter;

        $query = 'select sum(a.counter) as counts from track_access as a '.
                 'where a.url like '.$db->quote($url);
        if($unique) $query.= ' group by a.user_id';

        $result = $db->query($query)->fetchAll();
        return $unique ? count($result) : $result[0]['counts'];
    }
}
