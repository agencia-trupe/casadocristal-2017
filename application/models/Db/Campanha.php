<?php

class Application_Model_Db_Campanha extends Zend_Db_Table 
{
    protected $_name = "campanha";
    
    protected $_dependentTables = array(
        'Application_Model_Db_CasosClinicos',
        'Application_Model_Db_Perguntas'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_CasosClinicos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_CasosClinicos',
            'refColumns'    => 'campanha_id'
        ),
        'Application_Model_Db_Perguntas' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Perguntas',
            'refColumns'    => 'campanha_id'
        )
    );
    
    public function getCampanhaAtiva()
    {
        $where = 'status_id=1 and '.
                 'data_ini <= date(now()) and '.
                 'data_fim >= date(now())';
        
        $order = 'id desc';
        
        return $this->fetchRow($where,$order);
    }
}