<?php

class Application_Model_Db_Faq extends Zend_Db_Table 
{
    protected $_name = "faq";
    
    protected $_dependentTables = array(
        'Application_Model_Db_FaqPerguntas'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_FaqPerguntas' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_FaqPerguntas',
            'refColumns'    => 'faq_id'
        )
    );
}