<?php

class Application_Model_Db_Clientes extends Zend_Db_Table
{
    protected $_name = "clientes";
    protected $_login_column = "email";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Pedidos','Application_Model_Db_Orcamentos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Pedidos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Pedidos',
            'refColumns'    => 'cliente_id'
        ),
        'Application_Model_Db_Orcamentos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Orcamentos',
            'refColumns'    => 'cliente_id'
        )
    );
    
    // gets
    public function findByLoginAtivo($email,$password)
    {
        return $this->fetchRow($this->_login_column.' = "'.$email.'" and senha = "'.md5($password).'" and status_id = 1');
    }
    
    public function findByEmail($email)
    {
        return $this->fetchRow('email = "'.$email.'"');
    }
    
    /**
     * Retorna quantidade total
     * 
     * @param string $where - string de seleção where, padrão NULL
     *
     * @return int
     */
    public function count($where=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from($this->_name,array('count(*) as cnt'));
        $count = $where ? $select->where($where)->query()->fetchAll() : $select->query()->fetchAll();
        return $count[0]['cnt'];
    }
}