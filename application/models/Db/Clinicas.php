<?php

class Application_Model_Db_Clinicas extends ZendPlugin_Db_Table 
{
    protected $_name = "clinicas";
    
    protected $_dependentTables = array(
        'Application_Model_Db_ExamesFotos',
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_ExamesFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_ExamesFotos',
            'refColumns'    => 'exame_id'
        )
    );
}