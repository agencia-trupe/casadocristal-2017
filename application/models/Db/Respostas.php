<?php

class Application_Model_Db_Respostas extends ZendPlugin_Db_Table 
{
    protected $_name = "respostas";
    
    protected $_dependentTables = array(
        'Application_Model_Db_Perguntas',
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Perguntas' => array(
            'columns' => 'pergunta_id',
            'refTableClass' => 'Application_Model_Db_Perguntas',
            'refColumns'    => 'id'
        )
    );
}