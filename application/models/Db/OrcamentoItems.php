<?php

class Application_Model_Db_OrcamentoItems extends Zend_Db_Table 
{
    protected $_name = "orcamento_items";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Orcamentos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Orcamentos' => array(
            'columns' => 'orcamento_id',
            'refTableClass' => 'Application_Model_Db_Orcamentos',
            'refColumns'    => 'id'
        )
    );
    
}