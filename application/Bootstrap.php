<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{    
    protected function _initRoute()
    {
        if($this->isDirectAccess()) return null;

        $this->router = new Zend_Controller_Router_Rewrite();
		$this->request =  new Zend_Controller_Request_Http();
		$this->router->route($this->request); // pegar todos os parametros
        
        $this->bootstrap('view');
        $this->view = $this->getResource('view');
        $this->bootstrap('layout');
        $this->layout = $this->getResource('layout');
        
        if($this->request->getModuleName()=="default"){
            $auth = new Zend_Session_Namespace(SITE_NAME.'_login_cliente');
            
            $this->view->isLogged = $this->view->logged = (bool)$auth;
            $this->view->login = $auth;

            $this->view->user_vip = false;
            if((bool)$this->view->login->user) 
                if($this->view->login->user->vip=='1') 
                    $this->view->user_vip = true;

            $this->view->where_role = 'role in (0'.
                ((bool)@$this->view->login->user ? ',1' : '').
                ($this->view->user_vip ? ',2' : '').
                ') ';
            
            // dados da empresa
            $dados_empresa = new Application_Model_Db_DadosEmpresa();
            $this->view->dados_empresa = Is_Array::utf8DbRow(
                $dados_empresa->fetchRow('id = 1')
            );
        }
        
        if($this->request->getControllerName()=="admin"){
            $this->layout->setLayout("admin");
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session(SITE_NAME));
            
            $this->view->isLogged = $this->view->logged = $auth->hasIdentity();
            $this->view->login = $auth->hasIdentity() ? $auth->getIdentity() : null;
        }
		
        if($this->request->getControllerName()=="rsvp"){
            $this->layout->setLayout("rsvp");
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session(SITE_NAME));
            
            $this->view->isLogged = $this->view->logged = $auth->hasIdentity();
            $this->view->login = $auth->hasIdentity() ? $auth->getIdentity() : null;
        }
        
        if(array_key_exists('busca',$this->request->getParams())){
            $this->view->busca = $this->request->getParam('busca');
        }
    }

    public function isDirectAccess()
    {
        return strstr(FULL_URL, '/img/') ||
               strstr(FULL_URL, '/public/') ||
               strstr(FULL_URL, '.json') ||
               strstr(FULL_URL, '.ajax') ||
               strstr(FULL_URL, '.csv') ||
               strstr(FULL_URL, '.xml');
        // return in_array($this->request->getControllerName(),array('img','public')) ||
        //        strstr($this->request->getActionName(), '.json') ||
        //        strstr($this->request->getActionName(), '.xml');
    }
    
    protected function _initLayoutConfigs(){
        if($this->isDirectAccess()) return null;

        $this->view->doctype('XHTML1_STRICT');
        $this->view->modulo = $this->request->getControllerName();
        $this->view->action = $this->request->getActionName();
        if(strstr($this->view->action,'?')) $this->view->action = reset(explode('?', $this->view->action));
        $this->view->controller = $this->request->getControllerName();
        $this->view->module = $this->request->getModuleName();

        if(($this->request->getControllerName() == 'admin') ||
           ($this->request->getControllerName() == 'portal')){
            $actionName = $this->request->getActionName();
            $actions = explode('/',$this->request->getRequestUri());
            // _d($actions);
            $curAction = end($actions);
            if($this->request->getControllerName() == 'portal') $curAction = (bool)@$actions[3] ? $actions[3] : 'index'; //end($actions);
            $s = array_search($actionName,$actions);
            $action = $curAction!=$actions[$s]?$curAction:'index';
            // _d($action);
            $this->view->action = $action;
            if(strstr($this->view->action,'?')) $this->view->action = reset(explode('?', $this->view->action));
            if(!(bool)trim($this->view->action)||strstr($this->view->action,'?')) $this->view->action = $actions[sizeof($actions)-1];
            if(!(bool)trim($this->view->action)||strstr($this->view->action,'?')) $this->view->action = $actions[sizeof($actions)-2];
            if(!(bool)trim($this->view->action)||strstr($this->view->action,'?')) $this->view->action = $actions[sizeof($actions)-3];
            $this->view->controller = $actionName;
            $this->view->module = $this->request->getControllerName();
            $this->view->action2 = $action;
            $this->view->controller2 = $actionName;
            $this->view->module2 = $this->request->getControllerName();
            $this->view->modulo2 = $this->request->getControllerName();
        }
        
        // adiciona a view a sessão do carrinho
        $this->view->cart = new Zend_Session_Namespace(SITE_NAME."_cart");
        if(!isset($this->view->cart->items)){
            $this->view->cart->items = array();
            $this->view->cart->forma = 1;
        }
		
		// switch($this->request->getControllerName()){
		// 	case 'rsvp':
		// 		$actionName = $this->request->getActionName();
		// 		$actions = explode('/',$this->request->getRequestUri());
		// 		$curAction = end($actions);
		// 		$s = array_search($actionName,$actions);
		// 		$action = $curAction!=$actions[$s]?$curAction:'index';
		// 		//_d($action);
		// 		$this->view->action = $action;
		// 		$this->view->controller = $actionName;
		// 		$this->view->module = $this->request->getControllerName();
		// 		break;
		// 	default:
		// 		$this->view->action = $this->request->getActionName();
		// 		$this->view->controller = $this->request->getControllerName();
		// 		$this->view->module = $this->request->getModuleName();

  //               // adiciona a view a sessão do carrinho
  //               $this->view->cart = new Zend_Session_Namespace(SITE_NAME."_cart");
  //               if(!isset($this->view->cart->items)){
  //                   $this->view->cart->items = array();
  //                   // $this->view->cart->forma = 1;
  //               }
		// }
    }
    
    /**
	 * used for handling top-level navigation
	 * @return Zend_Navigation
	 */
	protected function _initNavigation()
	{
        if($this->isDirectAccess()) return null;
        
        if($this->request->getControllerName()=="admin"){   
            $login = new Zend_Session_Namespace(SITE_NAME.'_login');         
            $config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation-admin.xml', 'nav');
            $config = $config->toArray();
            //_d(new Zend_Navigation($config['admin-top']));
            $this->view->menu = new stdClass();

            // lista paginas
            $pag = new Application_Model_Db_Paginas();
            $rows_pags = Is_Array::utf8DbResult($pag->fetchAll('status_id = 1',array('id')));
            
            if(count($rows_pags)){
                $config['admin-top']['paginas']['pages'] = array();

                foreach($rows_pags as $row_pag){
                    $config['admin-top']['paginas']['pages']['pagina-'.$row_pag->alias] = array(
                        'label' => $row_pag->titulo,
                        'uri'   => URL.'/admin/paginas/edit/'.$row_pag->alias
                    );
                }
            }

            self::configUrlPrefix($config['admin-top']);
            $this->view->menu->admin_top  = new Zend_Navigation($config['admin-top']);
        } else {
            $categs = new Application_Model_Db_Categorias();
            $config_categ = array();
            $config_categv = array();

            $where_categ = 'status_id = 1 ';
            $where_categ.= 'and '.$this->view->where_role;
            
            $categs_rows = (
                $categs->fetchAllWithPhoto($where_categ,array('ordem'))
            );
            // _d($categs_rows);
            $i = 0;
            foreach($categs_rows as $categ){
                if(!(bool)$categ->categoria_id){
                    $cat = array(
                        'label'     => ($categ->descricao),
                        'title'     => ($categ->descricao),
                        'descricao' => ($categ->body),
                        'foto'      => ($categ->foto_path),
                        'alias'     => $categ->alias,
                        'uri'       => URL.'/produtos/'.$categ->alias,
                        // 'uri'       => '',
                        'id'        => 'categoria-'.$categ->id,
                        'class'     => 'cat-main '.$categ->alias.' '.($categ->role=='2' ? 'categoria-vip' : '')
                    );

                    // adiciona subcategorias
                    $cat['pages'] = array();
                    
                    foreach($categs_rows as $child){
                        if($child->categoria_id == $categ->id){
                            $cat['pages'][] = array(
                                'label'     => ($child->descricao),
                                'title'     => ($categ->descricao),
                                'descricao' => ($categ->body),
                                'uri'       => URL.'/produtos/'.$child->alias,
                                'class'     => $categ->alias
                            );
                        }
                    }

                    if($categ->role=='2'){
                        $config_categv[$categ->alias] = $cat;
                    } else {
                        $config_categ[$categ->alias] = $cat;
                    }
                }
                
                $i++;
            }
            //_d($config_categv);

            $_menu_nav = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'nav');
            $menu_nav = $_menu_nav->toArray();
            $menu_nav_top = $menu_nav['top'];
            $menu_nav['footer2']['produtos']['pages'] = $config_categ;
            
			// prefixos
            self::configUrlPrefix($menu_nav['top']);
            self::configUrlPrefix($menu_nav['footer1']);
            self::configUrlPrefix($menu_nav['footer2']);
			
            $this->view->menu = new stdClass();
            $this->view->menu->top        = new Zend_Navigation($menu_nav['top']);
            $this->view->menu->footer1    = new Zend_Navigation($menu_nav['footer1']);
            $this->view->menu->footer2    = new Zend_Navigation($menu_nav['footer2']);
            $this->view->menu->categorias = new Zend_Navigation($config_categ);
            $this->view->menu->categoriasv= new Zend_Navigation($config_categv);
			
            $uri = APPLICATION_ENV == 'development' ?
                    URL.$this->request->getPathInfo() : // dev
                    URL.$this->request->getPathInfo();  // production
            
            foreach(get_object_vars($this->view->menu) as $menu){
                $activeNav = $menu->findByUri($uri) or
                $activeNav = $menu->findByUri(str_replace('http://'.$_SERVER['HTTP_HOST'],'',$uri));
                
                if(null !== $activeNav){
                    $activeNav->active = true;
                    $activeNav->setClass("active");	
                }
            }
        }
	}

    function configUrlPrefix(&$config)
    {
        foreach($config as &$c){
            // adiciona url ao link
            if(isset($c['uri'])) if($c['uri'] != '#' && !strstr($c['uri'],'http')) $c['uri'] = URL.$c['uri'];
            // adiciona recursão à função
            if(isset($c['pages'])) $c['pages'] = self::configUrlPrefix($c['pages']);
        }
        
        return $config;
    }
}