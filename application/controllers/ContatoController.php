<?php

class ContatoController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        
    }
    
    public function enviarAction()
    {
        $r = $this->getRequest();
        
        if(!$r->isPost()){ // interrompe se não for Post
            return array('error'=>'Requisição inválida');
        }
        
        $form = new Application_Form_Contato();
        $post = $r->getPost();
        
        if($form->isValid($post)){ // valida post
            $html = '<h2 style="font:19px Verdana, Arial, sans-serif">Contato</h2>'. // monta html
                    '<p style="font:12px Verdana, Arial, sans-serif;">'.nl2br($r->getParam('mensagem')).'</p>'.
                    '<p style="font:12px Verdana, Arial, sans-serif;">'.
                        "<b>Nome:</b> ".$r->getParam('nome')."<br/>".
                        "<b>E-mail:</b> <a href='mailto:".$r->getParam('email')."'>".$r->getParam('email')."</a><br/>".
                        ((bool)$post['telefone'] ? "<b>Telefone:</b> ".$r->getParam('ddd')." ".$r->getParam('telefone')."" : '').
                    '</p><br/><br/>';
            
            try { // tenta enviar o e-mail
                // if(APP_ENV_PRO) Trupe_CinCin_Mail::sendWithReply(
                Trupe_CinCin_Mail::sendWithReply(
                    $post['email'],
                    $post['nome'],
                    'Contato',
                    $html
                );
                return array('msg'=>'Mensagem enviada!<br/>'.
                                    'Em breve retornaremos o contato.');
            } catch(Exception $e){
                return array('error'=>$e->getMessage());
            }
        }
        
        return array('error'=>'Preencha todos os campos corretamente');
    }

    public function testAction()
    {
        // _d(Trupe_CinCin_Mail::getConfigs());

        try{
            // Trupe_CinCin_Mail::send('patrick@trupe.net','Icko','Confirmação','Confirmar leitura do texto');
            Trupe_CinCin_Mail::send('patrick.trupe@gmail.com','Trupe','Confirmação','Confirmar leitura do texto');
            echo "ok";
        } catch(Exception $e){
            echo $e->getMessage();
        }
        exit();
    }


}