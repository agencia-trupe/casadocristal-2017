<?php

class OrcamentoController extends Zend_Controller_Action
{

    public function init()
    {
        $this->carrinho = new Application_Model_Carrinho();
        $this->produtos = new Application_Model_Db_Produtos();
    }

    public function indexAction()
    {
        if(count($this->view->cart->items)){
            foreach($this->view->cart->items as &$item){
                $item->item = unserialize(serialize($item->item)); // bugfix for error: AbstractClass no definition
                $item->item->fotos = $this->produtos->getFotosById($item->id);//,$item->cor_id);
            }
        }
        // _d($this->view->cart->items[1]->item->fotos);

        Application_Model_Login::setRedirectUrl($this,URL.'/orcamento');
    }


}

