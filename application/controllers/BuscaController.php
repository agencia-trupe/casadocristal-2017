<?php

class BuscaController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        // models
        $this->produtos = new Application_Model_Db_Produtos();
    }

    public function indexAction()
    {
        $busca = addslashes(trim($this->_getParam('busca')));

        // produtos
        $order = 'data_cad desc';
        if($this->_hasParam('ordem')) if((bool)$this->_getParam('ordem')) $order = Application_Model_Db_Produtos::getOrdemTable($this->_getParam('ordem'));
        $this->view->order = $this->_hasParam('ordem') ? $this->_getParam('ordem') : null;
        
        $where = '(titulo like "%'.str_replace(' ','%',$busca).'%" ';
        $where.= 'or descricao like "%'.str_replace(' ','%',$busca).'%" ';
        $where.= 'or descricao_completa like "%'.str_replace(' ','%',$busca).'%" ';
        $where.= 'or info like "%'.str_replace(' ','%',$busca).'%") ';
        $where.= 'and status_id = 1 ';
        $where.= 'and '.$this->view->where_role;

        /* paginação */
        $records_per_page   = 9;
        $selectable_pages   = 9;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        $produtos = $this->produtos->fetchAll($where,$order,$limit,$offset);

        if(count($produtos)){
            $produtos = Is_Array::utf8DbResult($produtos);
            $produtos = $this->produtos->checkDesconto($produtos);
            $produtos = $this->produtos->getCategorias($produtos);
            $produtos = $this->produtos->getFotos($produtos,true);

            //_d($produtos);
        }
        
        $total = $this->view->total = $this->produtos->count($where);
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        $this->view->produtos = $produtos;
        $this->view->busca = $busca;

        if($total == 0){
            $destaques = Is_Array::utf8DbResult(
                $this->produtos->fetchAll(null,'data_edit desc',12)
            );

            $destaques = $this->produtos->getFotos($destaques,true);
            $this->view->destaques = $destaques;
        }
    }

}