<?php

class MeuCadastroController extends Zend_Controller_Action
{
    public function init()
    {
        Application_Model_LoginCliente::checkAuth($this);
        
        // models
        $this->messenger = new Helper_Messenger();
        $this->clientes = new Application_Model_Db_Clientes();
        $this->pedidos = new Application_Model_Db_Orcamentos();
        $this->pedidos_items = new Application_Model_Db_OrcamentoItems();
        $this->status = new Application_Model_Db_OrcamentoStatus();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente');
    }

    public function indexAction()
    {
        $form_meus_dados = new Application_Form_MeusDados();
        $form_meus_dados->addId($this->login->user->id); // adiciona id dinamico ao form
        
        // popula form com Post ou dados do banco
        if($this->_hasParam('data')){
            $form_meus_dados->populate($this->_getParam('data'));
        } else {
            $cliente = Is_Array::utf8All($this->clientes->fetchRow('id = '.$this->login->user->id)->toArray());
            
            $cliente['dddtel']   = substr($cliente['telefone'],0,2);
            $cliente['telefone'] = substr($cliente['telefone'],2,strlen($cliente['telefone']));
            $cliente['dddcel']   = substr($cliente['celular'],0,2);
            $cliente['celular']  = substr($cliente['celular'],2,strlen($cliente['celular']));
            // _d($cliente);
            
            $form_meus_dados->populate($cliente);
            $this->view->cliente = Is_Array::toObject($cliente);
        }
        
        $this->view->form_meus_dados = $form_meus_dados;
        
        // paginação
        $records_per_page   = 6;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);

        $where = 'cliente_id="'.$this->login->user->id.'"';
        
        // $this->view->pedidos = $this->pedidos->getAll('cliente_id="'.$this->login->user->id.'"','data_cad desc',$limit,$offset);
        $this->view->pedidos = Is_Array::utf8DbResult($this->pedidos->fetchAll($where,'data_cad desc',$limit,$offset));
        $this->view->status = $this->status->getKeyValues();
        
        $total = $this->view->total = $this->pedidos->count($where);
        
        // seta parâmetros da paginação
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        $this->view->paginacao = $pagination;
    }
    
    public function saveAction()
    {
        // nega acesso se não for Post
        if(!$this->_request->isPost()){
            $this->_forward('denied','error',null,array('url'=>URL.'/meu-cadastro/'));
            return;
        }
        
        $post = $this->getRequest()->getPost();
        $form = new Application_Form_MeusDados();
        $form->addId($this->login->user->id); // adiciona id dinâmico
        
        // caso altere a senha, adiciona para validação
        if(isset($post['senha']) && !empty($post['senha'])){
            $form->requireSenha();
        }
        
        if($form->isValid($post)){ // valida form
            $post = Is_Array::deUtf8All($form->getValues());
            
            // adiciona filtro se alterar senha
            if(isset($post['senha']) && !empty($post['senha'])){
                $post['senha'] = md5($post['senha']);
            } else {
                unset($post['senha']);
            }
            unset($post['senhac']); // remove campo de confirmação de senha
            
            // limpando dados
            $post['data_edit'] = date('Y-m-d H:i:s');
            $post['data_nascimento'] = Is_Date::br2am($post['data_nascimento']);
            $post['telefone'] = Is_Str::removeCaracteres($post['dddtel'].$post['telefone']);
            $post['celular'] = Is_Str::removeCaracteres($post['dddcel'].$post['celular']);
            $post['cep'] = Is_Str::removeCaracteres($post['cep']);
            
            if($post['local_id'] == '__none__'){
                $post['local_id'] = new Zend_Db_Expr('NULL');
            }

            $unsets = array('senhac','dddtel','dddcel');
            foreach($unsets as $u) if(isset($post[$u])) unset($post[$u]);
            
            $this->clientes->update($post,'id='.$post['id']);
            
            $this->messenger->addMessage('Cadastro alterado com sucesso!');
            // $this->_forward('index');
            $this->_redirect($this->view->controller);
        } else {
            $this->messenger->addMessage('Preencha o formulário corretamente.','error');
            Js::alert('Preencha o formulário corretamente.');
            // $this->_forward('index',null,null,array('data'=>$post));
            return $this->_redirect($this->view->controller);
        }
    }

    public function editarAction()
    {
        $form_meus_dados = new Application_Form_MeusDados();
        $form_meus_dados->addId($this->login->user->id); // adiciona id dinamico ao form
        
        // popula form com Post ou dados do banco
        if($this->_hasParam('data')){
            $form_meus_dados->populate($this->_getParam('data'));
        } else {
            $cliente = Is_Array::utf8All($this->clientes->fetchRow('id = '.$this->login->user->id)->toArray());
            
            $cliente['dddtel']   = substr($cliente['telefone'],0,2);
            $cliente['telefone'] = substr($cliente['telefone'],2,strlen($cliente['telefone']));
            $cliente['dddcel']   = substr($cliente['celular'],0,2);
            $cliente['celular']  = substr($cliente['celular'],2,strlen($cliente['celular']));
            $cliente['data_nascimento'] = Is_Date::am2br($cliente['data_nascimento']);
            // _d($cliente);
            
            $form_meus_dados->populate($cliente);
            $this->view->cliente = Is_Array::toObject($cliente);
        }
        
        $this->view->form_meus_dados = $form_meus_dados;
    }

    public function pedidoAction()
    {
        if(!$this->_hasParam('id')){
            return $this->_forward('not-found','error','default',array('url'=>URL.'/'));
        }

        $id = addslashes($this->_getParam('id'));
        $pedido = $this->pedidos->fetchRow('id = "'.$id.'"');

        if(!$pedido || @$pedido->cliente_id != $this->view->login->user->id){
            return $this->_forward('not-found','error','default',array('url'=>URL.'/'));
        }

        $pedido_items = Is_Array::utf8DbResult(
            $this->pedidos_items->fetchAll('orcamento_id = "'.$pedido->id.'"')
        );

        $this->view->pedido = $pedido;
        $this->view->pedido_items = $pedido_items;
        $this->view->status = $this->status->getKeyValues();
    }
    
    public function postDispatch()
    {
        // $this->view->flash_messages = $this->messenger->getCurrentMessages();
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }
}