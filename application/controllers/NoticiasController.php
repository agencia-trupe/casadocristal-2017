<?php

class NoticiasController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
		// if(APPLICATION_ENV!='development') $this->_redirect('/');

        // models
        $this->noticias = new Application_Model_Db_Noticias();
        $this->view->table = new Application_Model_Db_Noticias();
        
        $this->view->meta_description = 'Últimas notícias';
        $this->view->bitly = new Php_Bitly(BITLY_USER,BITLY_KEY);
    }

    public function indexAction()
    {
        if(!$this->_hasParam('noticia')){
            return $this->_redirect('/');
            $this->view->titulo = 'Notícias';
            
            /* paginação */
            $records_per_page   = 3;
            $selectable_pages   = 9;
            $pagination = new Php_Zebra_Pagination();
            $limit  = $records_per_page;
            $offset = (($pagination->get_page() - 1) * $records_per_page);
            
            $rows = $this->noticias->fetchAll('status_id = 1','data desc',$limit,$offset);
            $total = $this->view->total = $this->noticias->count('status_id = 1');
            
            /* seta parâmetros da paginação */
            $pagination->records($total)
                       ->records_per_page($records_per_page)
                       ->selectable_pages($selectable_pages)
                       ->padding(false);
            
            $this->view->paginacao = $pagination;
            
            $noticias = Is_Array::utf8DbResult($rows);

            if(count($noticias)){
                foreach ($noticias as &$noticia){
                    $noticia->fotos = $this->noticias->getFotos($noticia->id);
                }
            }

            $this->view->noticias = $noticias;
            return;
        }
        
        $noticia = $this->noticias->getByAlias($this->_getParam('noticia'));
        if(!$noticia || @$noticia->status_id == 0){
            $this->_forward('not-found','error','default',array('url'=>URL.'/'));
            return false;
        }
        
        $noticia = Is_Array::utf8DbRow($noticia);
        $noticia->fotos = $this->noticias->getFotosById($noticia->id);
        $this->view->noticia = $noticia;
        
        // meta tags
        $this->view->titulo = $this->view->noticia->titulo;
        $this->view->meta_description = str_replace("\n",'',Is_Str::crop(Php_Html::toText($this->view->noticia->body),200));
        $this->view->meta_canonical = URL.'/noticias/'.$this->view->noticia->alias;

        // meta og
        $this->view->meta_og_title = $this->view->noticia->titulo;
        $this->view->meta_og_description = str_replace("\n",'',Is_Str::crop(Php_Html::toText($this->view->noticia->body),200));
        $this->view->meta_og_url = URL.'/noticias/'.$this->view->noticia->alias;
        
        $noticias = $this->noticias->fetchAll('status_id=1','data desc',4);
        $this->view->novidades = count($noticias) ? Is_Array::utf8DbResult($noticias) : false;
    }


}

