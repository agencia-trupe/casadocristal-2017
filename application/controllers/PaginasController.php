<?php

class PaginasController extends Zend_Controller_Action
{

    public function init()
    {
        $this->paginas = new Application_Model_Db_Paginas();
    }

    public function indexAction()
    {
        if(!$this->_hasParam('alias')) return $this->_redirect('/');
        
        $alias = addslashes($this->_getParam('alias'));

        $pagina = Is_Array::utf8DbRow(
            $this->paginas->fetchRow('alias = "'.$alias.'"')
        );

        $pagina->fotos = $this->paginas->getFotos($pagina->id);

        $this->view->pagina = $pagina;
        $this->view->titulo = $pagina->titulo;
    }


}

