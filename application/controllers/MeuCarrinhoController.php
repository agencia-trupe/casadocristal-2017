<?php

class MeuCarrinhoController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        // models
        $this->carrinho = new Application_Model_Carrinho();
        $this->produtos = new Application_Model_Db_Produtos();
        // $this->taxas = new Application_Model_Db_Taxas();
        $this->formas = new Application_Model_Db_PedidosFormas();
        $this->messenger = new Helper_Messenger();
        
        //$where_taxa = $this->carrinho->_whereTaxa($this->carrinho->getTotalPrice());
        //$rates = $this->taxas->fetchAll($where_taxa);
        // $rates = $this->taxas->fetchAll('status_id = 1');
        // $this->view->rates = $rates ? Is_Array::utf8DbResult($rates) : false;
    }

    public function indexAction()
    {
        if(count($this->view->cart->items)){
            foreach($this->view->cart->items as &$item){
                $item->item->fotos = $this->produtos->getFotosById($item->id);//,$item->cor_id);
            }
        }
        
        $this->view->formas = $this->formas->getKeyValues();
    }

    public function enviarOrcamentoAction()
    {
        $request = $this->getRequest();
        Application_Model_Login::setRedirectUrl($this,URL.'/orcamento');
        
        if($request->isPost()){
            $itens = $this->carrinho->getItems();
            $post = $request->getPost();

            if(isset($post['login'])){
                if(!Application_Model_LoginCliente::process($post)){
                    $this->messenger->addMessage("Login incorreto.",'error');
                    //$f->populate($post);
                    $this->_redirect("login?1");
                    return;
                } else {
                    Application_Model_Login::unsetRedirectUrl();
                }
            }

            if(!Application_Model_LoginCliente::isLogged()){
                $this->_redirect("login?2");
                return;
            }

            $orcamento_id = $this->carrinho->saveOrcamento($post,$this->view->login->user);

            if($orcamento_id){
                $msg = 'Pedido de orçamento enviado! <br> Em breve entraremos em contato.';
                
                if($this->isAjax()){
                    return array('orcamento_id' => $orcamento_id,'msg' => $msg);
                }

                $this->messenger->addMessage($msg);
                $this->_redirect('meu-cadastro');
                return true;
            } else {
                $err = 'Erro ao enviar pedido de orçamento.';

                if($this->isAjax()){
                    return array('error' => $err);
                }

                $this->messenger->addMessage($err,'error');
                $this->_redirect('orcamento');
            }
        }

        if($this->isAjax()) return array('error'=>'Acesso negado');

        $this->messenger->addMessage('Acesso negado','error');
        $this->_redirect('orcamento');
    }
    
    public function identificacaoAction()
    {
        $request = $this->getRequest();
        Application_Model_Login::setRedirectUrl($this);
		
        if($request->isPost()){
            $post = $request->getPost();
            
            if(Application_Model_LoginCliente::isLogged()){
                $this->carrinho->update($post);
                $this->_redirect("meu-carrinho/enviar-orcamento");
                return;
            }
            
            if($this->_hasParam('frm-cart-action') && @$request->getParam('frm-cart-action') == 'login'){
                if(!Application_Model_LoginCliente::process($post)){
                    $this->messenger->addMessage("Login incorreto.",'error');
                    //$f->populate($post);
                } else {
                    Application_Model_Login::unsetRedirectUrl();
                    $this->_redirect("meu-carrinho/pagamento/");
                }
                
                return;
            }
            
            $this->carrinho->update($post);
        }
    }
    
    public function pagamentoAction()
    {
        $tipo = null;
        if($this->_hasParam('boleto')) $tipo = 'boleto';
        if($this->_hasParam('debito')) $tipo = 'debito';

        if($tipo===null){
            echo Js::alert('Tipo de pagamento inválido');
            $this->messenger->addMessage('Tipo de pagamento inválido','error');
            $this->_redirect('/');
            return false;
        }

        try{
            $this->carrinho->setPayment($tipo);
            $this->carrinho->complete();
        } catch(Exception $e){
            echo Js::alert($e->getMessage());
            $this->messenger->addMessage($e->getMessage(),'error');
            $this->_redirect('/');
            return false;
        }
    }

    public function dadosBoletoAction()
    {
        $order_id = $this->_hasParam('numOrder') ? $this->_getParam('numOrder') : null;

        // exibindo dados
        header("Content-Type: text/plain");
        // if($this->_hasParam('transId')) exit('<PUT_AUTH_OK>');
        echo $this->carrinho->dadosBoletoMup($order_id);
        exit();
    }

    public function dadosDebitoAction()
    {
        $order_id = $this->_hasParam('numOrder') ? $this->_getParam('numOrder') : null;

        // exibindo dados
        header("Content-Type: text/plain");
        // echo $this->carrinho->dadosBoletoMup();
        echo $this->carrinho->dadosDebitoMup($order_id);
        exit();
    }
    
    public function confirmacaoAction()
    {
        exit('<h1 align="center">Compra confirmada</h1>');

        Application_Model_LoginCliente::checkAuth($this,array(
            'controller'=>'meu-carrinho',
            'action'=>'identificacao',
            'module'=>'default'
        ));
        
        if($this->_hasParam('transaction_id') && isset($_SESSION[SITE_NAME.'_pedido_id'])){
            $this->carrinho->setTransaction(
                $_SESSION[SITE_NAME.'_pedido_id'],
                $this->_getParam('transaction_id')
            );
            unset($_SESSION[SITE_NAME.'_pedido_id']);
        }
        
        $this->carrinho->reset();
    }

    public function falhaPagamentoAction()
    {
        $err = '<h2>Falha no pagamento</h2><p>';
        if($this->_hasParam('cod')) $err.= '<b>Erro '.$this->_getParam('cod').':</b> ';
        if($this->_hasParam('ErrorDesc')) $err.= utf8_encode($this->_getParam('ErrorDesc'));
        $err.= '</p>';

        exit($err);
    }
    
    /** ************************************************************************
     * Ajax Actions
     ************************************************************************ */
    
    /**
     * Adiciona um produto ao carrinho
     */
    public function addAction()
    {
        if(!$this->_hasParam('id')){ //|| !$this->_hasParam('cor-id')){
            return array('error'=>'Item não encontrado');
        }
        
        $this->carrinho->add($this->_getParam('id'),
                             $this->_getParam('titulo'),
                             $this->_getParam('formato_id'),
                             $this->_getParam('formato'),
                             $this->_getParam('gramatura_id'),
                             $this->_getParam('gramatura'),
                             $this->_getParam('qtde'),
                             $this->_getParam('obs'));
        return array('msg'=>'Item adicionado');
    }
    
    public function listAction()
    {
        return $this->carrinho->getItems();
    }
    
    /**
     * Remove um produto do carrinho
     */
    public function removeAction()
    {
        if(!$this->_hasParam('id')){
            return array('error'=>'Item não encontrado');
        }
        
        $this->carrinho->remove($this->_getParam('id'));
        return array('msg'=>'Item removido do carrinho');
    }
    
    /**
     * Retorna número de itens no carrinho
     */
    public function countItemsAction()
    {
        return array('count'=>count($this->view->cart->items)?$this->view->cart->count:0);
    }
    
    /**
     * Checa vale presente
     */
    public function vpAction()
    {
        if(!$this->_hasParam('cod')) return array('error'=>'Vale presente não encontrado');
        
        $cod = addslashes($this->_getParam('cod'));
        
        $select = $this->carrinho->select->reset();
        $select->from('pedidos_vp as pvp',array('pvp.id as vpid','pvp.cod','pvp.valor','pvp.para','pvp.usado'))
               ->joinLeft('pedidos_items as pi','pi.id = pvp.pedido_item_id',array('pi.produto_id'))
               ->joinLeft('pedidos as pd','pd.id = pi.pedido_id',array('pd.pedido_status_id as status_id'))
               ->joinLeft('produtos as p','p.id = pi.produto_id',array('p.titulo'))
               ->where('pvp.cod = ?',$cod);
        
        $rows = $select->query()->fetchAll();
        
        if(count($rows)){
            $row = Is_Array::toObject($rows[0]);
            
            switch(true){
                case !in_array($row->status_id,array(3,4,5,8,9,10)):
                    return array('error'=>'Vale presente não ativo'); break;
                case $row->usado == 1:
                    return array('error'=>'Este vale presente já foi utilizado'); break;
                default:
                    return array(
                        'msg'   => 'Vale presente ativo',
                        'valor' => $row->valor,
                        'id'    => $row->produto_id,
                        'vpcod' => $row->vpid,
                        'vpid'  => $row->cod
                    );
                    break;
            }
        } else {
            return array('error'=>'Vale presente não encontrado');
        }
    }
	
	/**
	 * Checa frete
	 */
    
	public function freteAction()
	{
		if(!MOD_FRETE) return array('error'=>'Consulta frete não disponível.');
		
		$frete = Php_Correios::frete(MOD_FRETE_CEP_ORIGEM,
									 addslashes($this->_getParam('cep')),
									 addslashes($this->_getParam('peso')));
		
		return $frete ? $frete : array('error'=>'Erro na consulta, tente novamente.');
	}
	
    /**
     * Mensagens
     */
    public function postDispatch()
    {
        $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}