<?php

class EsqueciMinhaSenhaController extends Zend_Controller_Action
{

    public function init()
    {
        // models
        $this->messenger = new Helper_Messenger();
        $this->clientes = new Application_Model_Db_Clientes();
    }

    /**
     * Primeiro passo
     * >> Se método for POST, envia o e-mail com a senha temporária
     *
     * @return void
     * @see Admin_EsqueciMinhaSenhaController::_processIndex()
     *
     */
    public function indexAction()
    {
        $request = $this->getRequest();
        $f = new Application_Form_EsqueciMinhaSenha();
		
        if($request->isPost()){
            $post = $request->getPost();
            
            if(!$f->isValid($post) || !$this->_processIndex($post['email'])) {
                $this->messenger->addMessage("E-mail inválido.",'error');
				$f->populate($post);
            } else {
                $this->messenger->addMessage("Senha enviada! Acesse seu e-mail para receber a nova senha.");
            }
        }
        
		$this->view->form = $f->render();
    }
    
    /**
     * >> Verifica se o e-mail informado existe no banco de dados, caso exista
     *    cria uma senha temporária, cadastra no banco e envia para o e-mail.
     *
     * @access protected
     * @param String $_email
     * @return bool
     *
     */
    protected function _processIndex($_email)
    {
        if($user = $this->clientes->findByEmail($_email)){
            $temppass = $this->_genPass(6);
            $link = URL."/login/";
            
            $html = "<h1>Olá ".$user->nome."</h1>".
                    "<p>".
                    "Você solicitou a recuperação de sua senha, estamos enviando uma senha temporária para acesso.<br/>".
                    "Lembre-se de alterá-la em seu primeiro acesso.<br/><br/>".
                    "Senha temporária: <b>".$temppass."</b><br/><br/>".
                    "<a href='".$link."'>Acessar página de login</a>".
                    "</p>".
                    "<p>Atenciosamente.</p>";
            $subject = 'Recuperação de senha';
            
            try {
                Trupe_CinCin_Mail::send($user->email,$user->nome,$subject,$html);
            } catch(Exception $e){
                $this->messenger->addMessage("Erro ao enviar e-mail. Tente novamente."/*$e->getMessage()*/,'error');
            }
            
            //$user->ativo = 4;
            $user->senha = md5($temppass);
            $user->save();
            
            return true;
        }
        return false;
    }
    
    /**
     * Gera senha temporária
     */
    public function _genPass($maxchars=10)
    {
        $alfa = "abcdefghijklmnopqrstuvwxyz";
        $num  = "0123456789";
        $hash = array();
        
        for($i=0;$i<$maxchars;$i++){
            $j = $i % 2 == 0 ? rand(0,(strlen($alfa)-1)) : rand(0,(strlen($num)-1));
            $hash[] = $i % 2 == 0 ? $alfa[$j] : $num[$j];
        }
        
        array_rand($hash);
        
        return implode("",$hash);
    }

    public function erroAction(){}
    
    public function okAction(){}
    
    public function postDispatch()
    {
        $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}

