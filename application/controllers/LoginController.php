<?php

class LoginController extends Zend_Controller_Action
{
    
    public function init()
    {
        // models
        $this->messenger = new Helper_Messenger();
        $this->clientes = new Application_Model_Db_Clientes();
    }
    
    /**
     * Gera formulário e faz validação do login caso seja Post
     *
     * @see Aluno_LoginController::_process()
     */
    public function indexAction()
    {
        $request = $this->getRequest();
		
        if($request->isPost()){
            $post = $request->getPost();
			
            if($this->_hasParam('top')){
                $post = array(
                    'login' => $post['login_email'],
                    'senha' => $post['login_senha'],
                );
            }

            if(!Application_Model_LoginCliente::process($post)){
                $this->messenger->addMessage("Login incorreto.",'error');
				//$f->populate($post);
            } else {
                $this->messenger->addMessage("Seja bem-vindo!");
                $redirect_url = Application_Model_Login::getRedirectUrl();
                $this->_redirect((bool)$redirect_url ? $redirect_url : "meu-cadastro");
            }
        }

        $redirect_url = Application_Model_Login::getRedirectUrl();
        // Application_Model_Login::unsetRedirectUrl();
        $redirect_url = $redirect_url ? $redirect_url : URL.'/meu-cadastro';
        if(@$this->view->login->user) return $this->_redirect($redirect_url);
    }
	
    /**
     * Ação de logout
     * >> Limpa o objeto Zend_Auth
     * >> Destrói a Session
     * >> Redireciona para pag. de login
     */
	public function logoutAction()
    {
		Application_Model_LoginCliente::logout($this,true);
	}
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }
}

