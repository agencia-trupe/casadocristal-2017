<?php

class ProdutoController extends Zend_Controller_Action
{

    public function init()
    {
        // models
        $this->paginas = new Application_Model_Db_Paginas();
        $this->produtos = new Application_Model_Db_Produtos();
        $this->cores = new Application_Model_Db_Cores();

        $pagina = $this->paginas->getPagina('produtos');
        $this->view->pagina = $pagina;
    }

    public function indexAction()
    {
        if(!$this->_hasParam('produto') || !$this->view->produto = $this->produtos->getWithFotos($this->_getParam('produto'))){
            return $this->_forward('not-found','error','default',array('url'=>URL.'/'));
        }
        if(($this->view->produto->role == '1' && !(bool)@$this->view->login->user)
            || ($this->view->produto->role == '2' && !$this->view->user_vip)){
            return $this->_forward('not-found','error','default',array('url'=>URL.'/'));
        }

        if($this->view->produto->categoria)
            $this->view->categoria = $this->view->produto->categoria;

        $this->view->titulo = $this->view->produto->titulo.((bool)$this->view->produto->cod ?
                                                            ' - '.$this->view->produto->cod :
                                                            '');
        
        /*// pega cores
        $cores = array();
        $meta_cores = array();
        
        if(count($this->view->produto->fotos)){
            foreach($this->view->produto->fotos as $f) if($f->cor) $cores[] = $f->cor->id;
            
            $where = count($cores) ? 'id in ('.implode(',',$cores).')' : 'id = null';
            $cores = $this->cores->getKeyValues('descricao',array('0'=>'Selecione uma cor...'),'ordem',$where);
            
            foreach($cores as $key => $value){
                if($key != 0) $meta_cores[] = $value;
            }
        }
        
        $this->view->cores = $cores;*/
        
        // sugestões
        $this->view->sugestoes = null;//$this->produtos->getSugestoes($this->view->produto->id,true,4,true);
        
        // metas e outras view configs
        $this->view->meta_description = Is_Str::crop(
            $this->view->produto->titulo.' '.
            // $this->view->currency($this->view->produto->valor).': '.
            Php_Html::toText($this->view->produto->descricao)
        ,300);
        $this->view->meta_keywords = Is_Str::text2keywords(
            Php_Html::toText($this->view->meta_description),', ',10
        );
        $this->view->meta_canonical = URL.'/produto/'.$this->view->produto->alias.'/';

        // meta og
        $this->view->meta_og_title = $this->view->produto->titulo;
        $this->view->meta_og_description = Is_Str::crop(Php_Html::toText($this->view->produto->descricao),200);
        $this->view->meta_og_url = URL.'/produto/'.$this->view->produto->alias.'/';
        if(count($this->view->produto->fotos)){
            $this->view->meta_og_image = URL.'/img/produtos|'.$this->view->produto->fotos[0]->path.'?w=80&h=80';
        }

        if($this->_hasParam('getcart')) _d($this->view->cart->items);
    }
}