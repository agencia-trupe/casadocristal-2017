<?php

class CadastroController extends Zend_Controller_Action
{

    public function init()
    {
        // models
        $this->messenger = new Helper_Messenger();
        $this->clientes = new Application_Model_Db_Clientes();
        $this->sess_cad = new Zend_Session_Namespace(SITE_NAME.'_formcad');
    }

    public function indexAction()
    {
        $form_meus_dados = new Application_Form_MeusDados();
        $form_meus_dados->setAction(URL.'/cadastro/save');
        
        if($this->_hasParam('data')){
            $form_meus_dados->populate($this->_getParam('data'));
        } else if((bool)@$this->sess_cad && $this->sess_cad->post){
            $form_meus_dados->populate($this->sess_cad->post);
        } else if($this->_hasParam('email_novo')){
            $form_meus_dados->populate(array('email'=>$this->_getParam('email_novo')));
        }
        
        $this->view->form_meus_dados = $form_meus_dados;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/cadastro/'));
            return;
        }
        
        $post = $this->getRequest()->getPost();
        $form = new Application_Form_MeusDados();
        $form->requireSenha();
        
        $redirect_url = Application_Model_Login::getRedirectUrl();
        // Application_Model_Login::unsetRedirectUrl();
        $redirect_url = $redirect_url ? $redirect_url : URL.'/login';
        // _d($redirect_url);
        
        if($form->isValid($post)){
            try{
                $post = Is_Array::deUtf8All($form->getValues());
                unset($post['senhac']);
                $senhao = $post['senha'];
                $post['data_cad'] = date('Y-m-d H:i:s');
                $post['data_nascimento'] = Is_Date::br2am($post['data_nascimento']);
                $post['senha'] = md5($post['senha']);
                $post['status_id'] = 1;
                $post['telefone'] = $post['dddtel'].Is_Str::removeCaracteres($post['telefone']);
                $post['celular'] = $post['dddcel'].Is_Str::removeCaracteres($post['celular']);
                $post['cep'] = Is_Str::removeCaracteres($post['cep']);
                $post['cpf'] = ((bool)trim($post['cpf'])) ?
                                Is_Cpf::clean($post['cpf']) :
                                new Zend_Db_Expr('NULL');
                
                if(@$post['local_id'] == '__none__'){
                    $post['local_id'] = new Zend_Db_Expr('NULL');
                }

                $unsets = 'dddcel,dddtel';
                foreach(explode(',',$unsets) as $u) if(isset($post[$u])) unset($post[$u]);
                
                $this->clientes->insert($post);
                
                $this->messenger->addMessage('<div class="'.$this->view->controller.'">'.
                                             '<h3>Parabéns</h3>'.
                                             '<p>O seu cadastro foi realizado com sucesso</p>'.
                                             '</div>');
                
                $this->sess_cad->post = null;
                $login = Application_Model_LoginCliente::process(array(
                    'login' => $post['email'],
                    'senha' => $senhao,
                ));
                $this->_redirect($redirect_url);
                
                // $this->_forward('index');
                // echo Js::script('window.setTimeout(function(){ window.location = "'.$redirect_url.'" },3000)');
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),'uplicate') ? 'CPF ou E-mail já cadastrados. Redirecionando para página de login.' : $e->getMessage();
                $this->messenger->addMessage($msg,'error');
                // print Js::alert($msg);
                // print Js::location($redirect_url);
                $this->sess_cad->post = $post;
                return $this->_redirect($redirect_url);
                exit();
                //$this->_redirect('login/');
            }
        } else {
            $err = 'Preencha todos os campos corretamente';
            $err_fields = $form->checkErrorField();
            if(count($err_fields)) $err.= ':<br/>'.implode('<br/>',$err_fields);
            $this->messenger->addMessage($err,'error');
            $this->sess_cad->post = $post;
            // $this->_forward('index',null,null,array('data'=>$post));
            return $this->_redirect($this->view->controller);
        }
    }

    public function trAction()
    {
        $this->messenger->addMessage('mensagem teste',2);
        $this->_redirect('/login/');
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        // $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}