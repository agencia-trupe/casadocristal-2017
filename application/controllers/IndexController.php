<?php

class IndexController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        $this->promocoes = new Application_Model_Db_Promocoes();
        $this->noticias = new Application_Model_Db_Noticias();
        $this->produtos = new Application_Model_Db_Produtos();
        $this->messenger = new Helper_Messenger();
    }
    
    public function indexAction()
    {
        //$this->view->fotos = $this->pagina_home_fotos->getFotos(1,1);
        
        $promocoes = Is_Array::utf8DbResult(
            $this->promocoes->fetchAll('status_id = 1','ordem',7)
        );
        
        // $produtos = Is_Array::utf8DbResult(
        //     $this->produtos->fetchAll('status_id = 1',array('destaque desc','data_edit desc'),12)
        // );

        $noticias = Is_Array::utf8DbResult(
            $this->noticias->fetchAll('status_id = 1','ordem',6)
        );

        $promocoes = $this->promocoes->getFotos($promocoes,true);
        // $produtos = $this->produtos->getFotos($produtos,true);
        $noticias = $this->noticias->getFotos($noticias,true);
        // foreach($produtos as &$row) $row->fotos = $this->produtos->getFotos($row->id);
        // foreach($noticias as &$row) $row->fotos = $this->noticias->getFotos($row->id);
        
        $this->view->fotos = $promocoes;
        // $this->view->produtos = $produtos;
        $this->view->noticias = $noticias;
    }
    
    public function mailAction()
    {
        try{
            // Trupe_CinCin_Mail::send('patrick@trupe.net','Icko','Confirmação','Confirmar leitura do texto');
            Trupe_CinCin_Mail::send('patrickmarcelodeoliveira@gmail.com','Icko','Confirmação','Confirmar leitura do texto');
            echo "ok";
        } catch(Exception $e){
            echo $e->getMessage();
        }
        exit();
    }

    public function phpinfoAction()
    {
        echo phpinfo();
        exit();
    }

    public function enviarPorEmailAction()
    {
        $r = $this->getRequest();
        
        if(!$r->isPost()){ // interrompe se não for Post
            return array('error'=>'Requisição inválida');
        }

        $form = new Application_Form_EnviarPorEmail();
        $post = $r->getPost();
        
        if($form->isValid($post)){ // valida post
            $html = '<h2 style="font:19px Verdana, Arial, sans-serif">'.$post['from_name'].' compartilhou um link com você</h2>'. // monta html
                    (((bool)@$post['body']) ?
                        '<p style="font:12px Verdana, Arial, sans-serif;color:#666;"><i>'.nl2br($post['body']).'</i></p><br/>' :
                        '');

            if($this->_hasParam('news')){
                $news_id = addslashes($this->_getParam('news'));
                $noticia = Is_Array::utf8DbRow($this->news->fetchRow('id="'.$news_id.'"'));

                if((bool)$noticia){
                    $html.= '<h3 style="font:17px Verdana, Arial, sans-serif;color:#666;">';
                    $html.= $noticia->titulo.'</h3>';
                    $html.= '<p style="font:12px Verdana, Arial, sans-serif;">'.
                            Is_Str::crop(Php_Html::toText($noticia->body,false,true),500).
                            '</p><br/>';
                }
            }

            if($this->_hasParam('portfolio')){
                $portfolio_id = addslashes($this->_getParam('portfolio'));
                $portfolio = Is_Array::utf8DbRow($this->portfolio->fetchRow('id="'.$portfolio_id.'"'));

                if((bool)$portfolio){
                    $html.= '<h3 style="font:17px Verdana, Arial, sans-serif;color:#666;">';
                    $html.= $portfolio->titulo.'</h3>';
                    $html.= '<p style="font:12px Verdana, Arial, sans-serif;">'.
                            stripslashes(Is_Str::crop(Php_Html::toText($portfolio->body,false,true),500)).
                            '</p><br/>';
                }
            }

            $html.= '<p style="font:15px Verdana, Arial, sans-serif;text-align:left;">'.
                    '<a href="'.urldecode($r->getParam('url')).'">Ler mais &raquo;</a>'.
                    '</p><br/><br/>';
            
            try { // tenta enviar o e-mail
                Trupe_CinCin_Mail::sendShare(
                    $post['from_mail'],
                    $post['from_name'],
                    $post['to'],
                    $post['from_name'].' compartilhou um link com você',
                    $html
                );
                return array('msg'=>'Mensagem enviada!');
            } catch(Exception $e){
                return array('error'=>$e->getMessage());
            }
        }
        
        return array('error'=>'Preencha todos os campos corretamente');
    }
    
}