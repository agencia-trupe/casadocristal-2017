<?php

class CategoriaController extends Zend_Controller_Action
{

    public function init()
    {
        /* models */
        $this->paginas = new Application_Model_Db_Paginas();
        $this->categorias = new Application_Model_Db_Categorias();
        $this->produtos = new Application_Model_Db_Produtos();
        $this->produtos_categorias = new Application_Model_Db_ProdutosCategorias();
        // $this->destaques = new Application_Model_Db_Destaques();
        
        $this->view->meta_description = 'Produtos '.SITE_TITLE;
        
        $pagina = $this->paginas->getPagina('produtos');
        $this->view->pagina = $pagina;
    }

    public function indexAction()
    {
        // produtos
        $order = 'data_cad desc';
        if($this->_hasParam('ordem')) if((bool)$this->_getParam('ordem')) $order = Application_Model_Db_Produtos::getOrdemTable($this->_getParam('ordem'));
        $this->view->order = $this->_hasParam('ordem') ? $this->_getParam('ordem') : null;
        $where = null;

        // paginação
        $records_per_page   = 12;
        $selectable_pages   = 10;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);

        if(!$this->_hasParam('categoria')){ // se não tiver categoria lista todos os produtos
            $this->view->titulo = 'Produtos';
            $this->view->meta_canonical = URL.'/produtos/';

            $where = 'status_id = 1 ';
            $where.= 'and '.$this->view->where_role;
            
            $produtos = $this->produtos->fetchAll($where,$order,$limit,$offset);

            if(count($produtos)){
                $produtos = Is_Array::utf8DbResult($produtos);
                $produtos = $this->produtos->checkDesconto($produtos);
                $produtos = $this->produtos->getCategorias($produtos);
                $produtos = $this->produtos->getFotos($produtos,true);

                // _d($produtos);
            }
            
            $total = $this->view->total = $this->produtos->count($where);
            
            // seta parâmetros da paginação
            $pagination->records($total)
                       ->records_per_page($records_per_page)
                       ->selectable_pages($selectable_pages)
                       ->padding(false);
            
            $this->view->paginacao = $pagination;
            $this->view->produtos = $produtos;
            return;
        }

        $categoria = addslashes($this->_getParam('categoria'));

        $this->view->categoria = $this->categorias->getWithProdutos(
            $categoria,
            'status_id = 1 and '.$this->view->where_role,
            'status_id = 1 and '.$this->view->where_role,
            $order,
            $limit,$offset
        );
        
        // se não houver categoria redireciona p/ página de erro
        if(!$this->view->categoria){
            return $this->_forward('not-found','error','default',array('url'=>URL.'/'));
        }

        $total = $this->view->total = $this->view->categoria->count;
        
        // seta parâmetros da paginação
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $categoria_parent = (bool)$this->view->categoria->categoria_id ? 
                            $this->categorias->getParents($this->view->categoria) : 
                            null;
        $titulo = ((bool)$this->view->categoria->categoria_id) ?
                    ucfirst($categoria_parent->descricao).' &raquo; ' :
                    '';

        $this->view->categoria_parent = $categoria_parent;

        $this->view->titulo = utf8_encode($titulo).ucfirst($this->view->categoria->descricao);
        
        $this->view->meta_description = SITE_TITLE.' - '.$this->view->categoria->descricao;
        $this->view->meta_canonical = URL.'/categoria/'.$categoria.'/';
    }
}