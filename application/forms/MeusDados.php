<?php

class Application_Form_MeusDados extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/meu-cadastro/save')->setAttrib('id','frm-meus-dados')->setAttrib('name','frm-meus-dados');
		
        $locais = new Application_Model_Db_LocalEntrega();
        
        // elementos
		$this->addElement('text','nome',array('label'=>'Nome Completo: (*)','class'=>'txt2'));
        $this->addElement('text','cpf',array('label'=>'CPF:','class'=>'txt2 mask-cpf'));
        $this->addElement('text','rg',array('label'=>'RG:','class'=>'txt2'));
        $this->addElement('select','sexo',array('label'=>'Sexo:','class'=>'txt2','multiOptions'=>array('1'=>'Masc.','0'=>'Fem.')));
        $this->addElement('text','data_nascimento',array('label'=>'Data de Nascimento:','class'=>'txt2 mask-date'));
        $this->addElement('text','email',array('label'=>'E-mail: (*)','class'=>'txt2'));        
        // $this->addElement('text','telefone',array('label'=>'telefone','class'=>'txt2 mask-tel'));
        $this->addElement('text','dddtel',array('label'=>'Telefone: (*)','class'=>'txt2 mask-ddd','placeholder'=>'ddd'));
        $this->addElement('text','telefone',array('label'=>'Telefone: (*)','class'=>'txt2 mask-tel-no-ddd','placeholder'=>'número'));
        // $this->addElement('text','celular',array('label'=>'celular','class'=>'txt2 mask-tel'));
        $this->addElement('text','dddcel',array('label'=>'Celular:','class'=>'txt2 mask-ddd','placeholder'=>'ddd'));
        $this->addElement('text','celular',array('label'=>'Celular:','class'=>'txt2','placeholder'=>'número'));
        $this->addElement('text','cep',array('label'=>'CEP: (*)','class'=>'txt2 mask-cep'));
        $this->addElement('text','logradouro',array('label'=>'Endereço:','class'=>'txt2'));
        $this->addElement('text','numero',array('label'=>'Número:','class'=>'txt2'));
        $this->addElement('text','complemento',array('label'=>'Complemento:','class'=>'txt2'));
        // $this->addElement('select','local_id',array('label'=>'Bairro:','class'=>'txt2','multiOptions'=>Is_Array::utf8All($locais->getKeyValues())));
        $this->addElement('text','bairro',array('label'=>'Bairro:','class'=>'txt2'));
        $this->addElement('text','cidade',array('label'=>'Cidade:','class'=>'txt2'));
        $this->addElement('text','uf',array('label'=>'Estado:','class'=>'txt2','maxlength'=>2));
        $this->addElement('password','senha',array('label'=>'Senha: (*)','class'=>'txt2'));
        $this->addElement('password','senhac',array('label'=>'Confirme sua senha: (*)','class'=>'txt2'));
        
        // filtros / validações
        $this->getElement('cep')->addFilter('digits');
        $this->getElement('telefone')->addFilter('digits');
        $this->getElement('celular')->addFilter('digits');
        // $this->getElement('cpf')->setRequired()
        //      ->addFilter('StripTags')->addFilter('StringTrim')->addFilter('digits')->addValidator('NotEmpty')
        //      ->setAttrib('data-validate',true)->setAttrib('data-errmsg','CPF inválido');
        $this->getElement('nome')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha o nome corretamente');
        $this->getElement('telefone')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha o telefone corretamente');
        $this->getElement('cep')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha o CEP corretamente');
        $this->getElement('email')->setRequired()->addValidator('EmailAddress')
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','E-mail inválido');
        
        // remove decoradores
        $this->removeDecs();
    }
    
    public function requireSenha()
    {
        $this->getElement('senha')->setRequired()
            ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha sua senha');
        $this->getElement('senhac')->setRequired()
            ->addValidator('Identical',false,array('token'=>'senha'))
            ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Confirme sua senha');
        return $this;
    }
    
    public function addId($value=null)
    {
        $this->addElement('hidden','id',array('value'=>$value));
        return $this;
    }
    
    public function addStatus($value=null)
    {
        $this->addElement('checkbox','status_id',array('label'=>'Ativo','value'=>$value));
        return $this;
    }

    public function addVip($value=null)
    {
        $this->addElement('checkbox','vip',array('label'=>'Cliente VIP','value'=>$value));
        return $this;
    }
    
    public function isValid($values){
        // if(!Is_Cpf::isValid($values['cpf'])) return false;
        
        return parent::isValid($values);
    }

    public function checkErrorField()
    {
        $errors = array(); $fields = array();

        foreach($this->getErrors() as $k => $v){
            if((bool)$v) $errors[] = $k;
        }

        if(count($errors)){
            foreach($errors as $e){
                $elm = $this->getElement($e);
                $field = $elm->getLabel();
                $fields[] = reset(explode(':',$field));
            }
        }

        return $fields;
    }
}

