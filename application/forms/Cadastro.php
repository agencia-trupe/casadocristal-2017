<?php

class Application_Form_Cadastro extends ZendPlugin_Form
{
	public $edit = false;

    public function init()
    {
	// configurações do form
	$this->setMethod('post')
	     ->setAction(URL.'/cadastro')
	     ->setAttrib('id','frm-cadastro')
	     ->setAttrib('name','frm-cadastro');
	
	// elementos
	$this->addElement('text','nome',array('label'=>'nome','class'=>'txt'));
	$this->addElement('text','login');
	$this->addElement('text','senha');
	$this->addElement('text','senhar');
	$this->addElement('text','tel');
	$this->addElement('text','pais');
	$this->addElement('text','uf');
	$this->addElement('text','cidade');
	$this->addElement('text','data_nascimento');
	$this->addElement('text','cidade');
	$this->addElement('text','endereco');
	$this->addElement('text','sexo');
	$this->addElement('text','filhos');
	$this->addElement('text','contato_freq_criancas');
	$this->addElement('text','escolaridade_id');
	$this->addElement('text','atividade_profissional_id');
	$this->addElement('text','interesse');
	$this->addElement('text','optin_territorio');
	$this->addElement('text','optin_alana');
	$this->addElement('text','aceite');
	
	// filtros / validações
	$this->getElement('nome')->setRequired();
	if(!$this->edit) $this->getElement('senha')->setRequired();
	if(!$this->edit) $this->getElement('senhar')->setRequired();
	$this->getElement('uf')->setRequired();
	$this->getElement('cidade')->setRequired();
	$this->getElement('data_nascimento')->setRequired();
	$this->getElement('sexo')->setRequired();
	$this->getElement('filhos')->setRequired();
	$this->getElement('contato_freq_criancas')->setRequired();
	$this->getElement('escolaridade_id')->setRequired();
	$this->getElement('atividade_profissional_id')->setRequired();
	if(!$this->edit) $this->getElement('aceite')->setRequired();
	$this->getElement('login')->setRequired()
	     ->addValidator('EmailAddress')
	     ->addFilter('StripTags')
	     ->addFilter('StringTrim');
	
	// remove decoradores
	$this->removeDecs();
    }
	
	public function isValid($values)
	{
		return parent::isValid($values) /*&&
			   $values['senha'] == $values['senhar'] &&
			   $values['aceite'] == 1 &&
			   Is_Cpf::isValid($values['cpf'])*/;
	}
}

