<?php

class Application_Form_EnviarPorEmail extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')
             ->setAction(URL.'/index/enviar-por-email')
             ->setAttrib('id','frm-share-mail')
             ->setAttrib('name','frm-share-mail');
        
        // elementos
		$this->addElement('text','from_name',array('label'=>'nome','class'=>'txt'));
        $this->addElement('text','from_mail',array('label'=>'e-mail','class'=>'txt'));
		$this->addElement('text','to',array('label'=>'e-mail','class'=>'txt'));
        $this->addElement('textarea','mensagem',array('label'=>'mensagem','class'=>'txt'));
        
        // filtros / validações
        $this->getElement('from_name')->setRequired()
             ->addFilter('StripTags')
             ->addFilter('StringTrim');
        $this->getElement('from_mail')->setRequired()
             ->addValidator('EmailAddress')
             ->addFilter('StripTags');
        $this->getElement('to')->setRequired()
             ->addValidator('EmailAddress')
             ->addFilter('StripTags');
        
        // remove decoradores
        $this->removeDecs();
    }
}

