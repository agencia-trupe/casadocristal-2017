<?php

class Application_Model_Db_PortfolioSugestoes extends Zend_Db_Table
{
    protected $_name = "portfolio_sugestoes";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Portfolio');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Portfolio' => array(
            'columns' => 'portfolio_id',
            'refTableClass' => 'Application_Model_Db_Portfolio',
            'refColumns'    => 'id'
        )
    );
}
