<?php

class Admin_LocaisDeEntregaController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "LOCAIS DE ENTREGA";
        $this->view->section = $this->section = "locais-de-entrega";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path = $this->view->img_path = APPLICATION_PATH."/../..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../..".FILE_PATH."/".$this->section;
        
        // models
        $this->locais = new Application_Model_Db_LocalEntrega();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
    }

    public function indexAction()
    {
        $this->view->locais = $this->locais->fetchAll(null,'descricao');
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url));
            return;
        }
        
        $params = $this->_request->getParams();
        
        try {
            for($i=0;$i<sizeof($params['id']);$i++){
                $data = array();
                $row = $this->locais->fetchRow('id='.$params['id'][$i]); // verifica registro para atualização
                
                $data['descricao'] = utf8_decode($params['descricao'][$i]);
                $data['status_id'] = $params['status_id'][$i];
                $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
                $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
                
                if($row){
                    $up = 0;
                    if($row->descricao != $data['descricao']){ $row->descricao = $data['descricao']; $up++; }
                    if($row->status_id != $data['status_id']){ $row->status_id = $data['status_id']; $up++; }
                    if($up > 0){ $row->save(); }
                } else {
                    $this->locais->insert($data);
                }
            }
            
            $this->messenger->addMessage('Registros atualizados.');
            $this->_redirect('admin/'.$this->section.'/');
            //$this->_forward('index');
        } catch(Exception $e) {
            $this->messenger->addMessage($e->getMessage(),'error');
        }
    }
    
    public function delAction()
    {
        $id = $this->_getParam("id");
        
        try {
            $this->locais->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
    
}