<?php

class Admin_LoginController extends Zend_Controller_Action
{
    /**
     * Seta parâmetros iniciais
     */
    public function init(){
        $this->view->url = $this->_request->getBaseUrl()."/admin";
        $this->view->titulo = "LOGIN";
        
        $this->usuario = new Application_Model_Db_Usuario();
    }
    
    /**
     * Gera formulário e faz validação do login caso seja Post
     *
     * @see Aluno_LoginController::_process()
     */
    public function indexAction(){
        if(Admin_Model_Login::isLogged()) $this->_redirect('admin');
        
        $request = $this->getRequest();
        $f = new Admin_Form_Login();
        
        if($request->isPost()){
            $post = $request->getPost();
			
            if(!$f->isValid($post) || !$this->_process($post)){
                $this->view->message = "Login incorreto.";
				//$f->populate($post);
            } else {
                $redirect_url = Admin_Model_Login::getRedirectUrl();
                Admin_Model_Login::unsetRedirectUrl();
                $this->_redirect($redirect_url?$redirect_url:"admin");
            }
        }
        
		$this->view->form = $f->render();
    }
    
    /**
     * Processa login
     *
     * @see Aluno_LoginController::_getAuthAdapter()
     */
    protected function _process($values){
		if(!isset($values['role'])) $values['role'] = '0,1,3,4';
		
		$usuario = $this->usuario->setRole($values['role'])->findByLoginAtivo($values['login'],$values['senha']);
        
        if($usuario) {
            $adapter = Admin_Model_Login::_getAuthAdapter();
            $adapter->setIdentity($values['login']);
            $adapter->setCredential($values['senha']);
            
            $auth = Zend_Auth::getInstance();
            $auth->setStorage(new Zend_Auth_Storage_Session(SITE_NAME));
            $result = $auth->authenticate($adapter);
            if ($result->isValid()) {
                $user = $adapter->getResultRowObject();
                $auth->getStorage()->write($user);
                
                $sess_login = new Zend_Session_Namespace(SITE_NAME."_login");
                $sess_login->user = $user;
                $sess_login->acl = SITE_NAME;
                
                return true;
            }
        }
		return false;
	}
	
    /**
     * Ação de logout
     * >> Limpa o objeto Zend_Auth
     * >> Destrói a Session
     * >> Redireciona para pag. de login
     */
	public function logoutAction(){
		Admin_Model_Login::logout($this);
	}
}

