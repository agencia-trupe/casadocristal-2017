<?php

class Admin_ClientesController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "CLIENTES";
        $this->view->section = $this->section = "clientes";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../..".FILE_PATH."/".$this->section;
        
        // models
        $this->clientes = new Application_Model_Db_Clientes();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 10;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            if($post['search-by'] == 'cpf'){
                $post['search-txt'] = Is_Cpf::clean($post['search-txt']);
            }
            
            $where = $post['search-by']." like '%".utf8_decode(str_replace(" ","%",$post['search-txt']))."%'";
            $rows = $this->clientes->fetchAll($where,$post['search-by'],$limit,$offset);
            
            $total = $this->view->total = $this->clientes->count($where);
        } else {
            $rows = $this->clientes->fetchAll(null,"data_cad desc",$limit,$offset);
            $total = $this->view->total = $this->clientes->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; NOVO");
        $form = new Application_Form_MeusDados();
        $form->setAction($this->_url.'save');
        $form->addStatus('1');
        $form->addVip();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $this->view->id = $this->produto_id = $data['id'];
            $data['cpf'] =Is_Cpf::clean($data['cpf']);
            $data['data_nascimento'] =Is_Date::am2br($data['data_nascimento']);

            $data['dddtel']   = substr($data['telefone'],0,2);
            $data['telefone'] = substr($data['telefone'],2,strlen($data['telefone']));
            $data['dddcel']   = substr($data['celular'],0,2);
            $data['celular']  = substr($data['celular'],2,strlen($data['celular']));
            
            $form->addId();
        } else {
            $form->requireSenha();
            $data = array('status_id'=>'1');
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        
        $form = new Application_Form_MeusDados();
        $id = (int)$this->_getParam("id");
        $row = $this->clientes->fetchRow('id='.$id); // verifica registro
        $data = array_map('utf8_decode',$this->_request->getParams());
        
        if(!$row || (isset($data['senha']) && !empty($data['senha']))){
            $form->requireSenha();
        }
        
        try {
            if($form->isValid($data)){
                $data['cpf'] =Is_Cpf::clean($data['cpf']);
                $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
                $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
                $data['data_nascimento'] = Is_Date::br2am($data['data_nascimento']);
                $data['telefone'] = $data['dddtel'].Is_Str::removeCaracteres($data['telefone']);
                $data['celular'] = $data['dddcel'].Is_Str::removeCaracteres($data['celular']);
                $data['cep'] = Is_Str::removeCaracteres($data['cep']);
                
                if(isset($data['senha']) && !empty($data['senha'])){
                    $data['senha'] = md5($data['senha']);
                } else {
                    unset($data['senha']);
                }
                
                if($data['local_id'] == '__none__'){
                    $data['local_id'] = new Zend_Db_Expr('NULL');
                }
                
                // remove dados desnecessários
                $unsets = 'dddcel,dddtel,senhac,submit,module,controller,action';
                foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);
                
                ($row) ? $this->clientes->update($data,'id='.$id) : $id = $this->clientes->insert($data);
                
                $this->messenger->addMessage('Registro atualizado.');
                $data['id'] = $id;
                $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
                //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
            } else {
                $this->messenger->addMessage('Preencha todos os campos corretamente.','error');
                $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
            }
        } catch(Exception $e) {
            $this->messenger->addMessage($e->getMessage(),'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->clientes->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->clientes->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
        
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}