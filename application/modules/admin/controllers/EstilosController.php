<?php

class Admin_EstilosController extends ZendPlugin_Controller_Ajax
{
    
    public function init()
    {
        $this->view->titulo = "ESTILOS";
        $this->view->section = $this->section = "estilos";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../..".FILE_PATH."/".$this->section;
        
        // models
        $this->categorias = new Application_Model_Db_Estilos();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();

        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
    }
    
    public function indexAction()
    {
        $this->view->categorias = $this->categorias->fetchAll(null,'ordem');
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url));
            return;
        }
        
        $duplicate_count = 0;
        $duplicates = array();
        $params = $this->_request->getParams();
        
        try {
            for($i=0;$i<sizeof($params['id']);$i++){
                $data = array();
                $row = $this->categorias->fetchRow('id='.$params['id'][$i]); // verifica registro para atualização
                
                $data['descricao'] = utf8_decode($params['descricao'][$i]);
                $data['ordem']     = $params['ordem'][$i];
                $data['status_id'] = $params['status_id'][$i];
                $data['alias']     = Is_Str::toUrl($params['descricao'][$i]);
                $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
                $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
                
                if($row){
                    $up = 0;
                    if($row->alias != $data['alias']){
                        if($this->categorias->fetchRow('alias="'.$data['alias'].'"')){
                            $duplicates[] = utf8_encode('&rarr; <b>'.$row->descricao.'</b> alterado para <b>'.$data['descricao'].'</b>');
                            $duplicate_count++;
                        } else {
                            $row->descricao = $data['descricao'];
                            $row->alias = $data['alias'];
                            $up++;
                        }
                    }
                    if($row->ordem != $data['ordem']){ $row->ordem = $data['ordem']; $up++; }
                    if($row->status_id != $data['status_id']){ $row->status_id = $data['status_id']; $up++; }
                    if($up > 0){ $row->save(); }
                } else {
                    if($this->categorias->fetchRow('alias="'.$data['alias'].'"')){
                        $duplicates[] = "&rarr; ".utf8_encode($data['descricao']);
                        $duplicate_count++;
                    } else {
                        $this->categorias->insert($data);
                    }
                }
            }
            
            // se há registros duplicados, adiciona mensagem
            ($duplicate_count > 0) ?
                $this->messenger->addMessage($duplicate_count.' registros possuem duplicidade. Por favor, altere-os e salve novamente:<br/>'.implode('<br/>',$duplicates),'error') :
                $this->messenger->addMessage('Registros atualizados.');
        } catch(Exception $e) {
            $this->messenger->addMessage($e->getMessage(),'error');
        }
        
        $this->_redirect('admin/'.$this->section);
    }
    
    public function delAction()
    {
        $id = (int)$this->_getParam("id");
        
        try {
            $this->categorias->delete("id=".$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}

