<?php

class Admin_ConfiguracoesController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        $this->view->titulo = "CONFIGURAÇÕES";
        $this->view->section = $this->section = "configuracoes";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
        
        $this->img_path = $this->view->img_path = APPLICATION_PATH."/../..".IMG_PATH."/".$this->section;

        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
    }

    public function indexAction()
    {
        // action body
    }
    
    public function telefonesAction()
    {
        $this->view->titulo.= " &rarr; TELEFONES";
        $table = new Application_Model_Db_Telefones();
        $form  = new Admin_Form_Telefones();
        
        if($this->_request->isPost()){
            $post = $this->_request->getPost();
            
            switch($post['action']){
                case "new":
                    $data = $post;
                    $data['user_cad'] = $this->login->user->id;
                    $data['data_cad'] = date("Y-m-d H:i:s");
                    $data['numero']   = Is_Str::removeCaracteres($data['numero']);
                    unset($data['submit']);
                    unset($data['action']);
                    //$post = Is_Array::deUtf8All($post);
                    
                    try {
                        $table->insert($data);
                        //$form->reset();
                        $this->messenger->addMessage("Cadastrado com sucesso!");
                    } catch(Exception $e) {
                        $erro = strstr($e->getMessage(),"Duplicate") ?
                                "Já existe um registro semelhante, escolha outro." :
                                $e->getMessage();
                        $this->messenger->addMessage($erro,'erro');
                    }
                    break;
                case "edit":
                    try {
                        $line = $table->fetchRow("id=".(int)$this->_getParam('id'));
                        
                        $line->numero    = Is_Str::removeCaracteres($post['numero']);
                        $line->status_id = $post['status_id'];
                        $line->data_edit = date("Y-m-d H:i:s");
                        $line->user_edit = $this->login->user->id;
                        
                        $line->save();
                        $this->messenger->addMessage("Registro alterado com sucesso!","message");
                        
                        //$this->_setParam('id',null);
                    } catch(Exception $e) {
                        $erro = strstr($e->getMessage(),"Duplicate") ?
                                "Já existe um registro semelhante, escolha outro nome." :
                                $e->getMessage();
                        $this->messenger->addMessage($erro,'erro');
                    }
                    break;
            }
            
            $lines = $post['action']=="search" ?
                     $table->fetchAll($post['search-by']." like '%".utf8_decode($post['search-txt'])."%'",'id desc') :
                     $table->fetchAll(null,'id desc');
        } else {
            $lines = $table->fetchAll(null,'id desc');
        }
        
        if($this->_hasParam('id')){
            $form->getElement('submit')->setLabel("Alterar");
            $data = $table->fetchRow("id=".(int)$this->_getParam('id'));
            //Is_Var::dump((array)$data);
            $form->populate($data->toArray());
        }
        
        $this->view->lines  = Is_Array::utf8DbResult($lines);
        $this->view->form   = $form;
        $this->view->action = $this->_hasParam('id') ? "edit" : "new";
    }
    
    public function telefonesDelAction(){
        $id = $this->_getParam("id");
        $table = new Application_Model_Db_Telefones();
        
        try {
            $table->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function usuariosAction()
    {
        $this->view->titulo.= " &rarr; USUÁRIOS";
        $table = new Application_Model_Db_Usuario();
        $form  = new Admin_Form_Usuarios();
        
        if($this->_request->isPost()){
            $post = $this->_request->getPost();
            
            switch($post['action']){
                case "new":
                    if($form->isValid($post)){
                        $data = $post;
                        $data['user_cad'] = $this->login->user->id;
                        $data['data_cad'] = date("Y-m-d H:i:s");
                        $data['senha']    = md5($data['senha']);
                        unset($data['submit']);
                        unset($data['action']);
                        $data = Is_Array::deUtf8All($data);
                        
                        try {
                            $table->insert($data);
                            //$form->reset();
                            $this->messenger->addMessage("Cadastrado com sucesso!");
                            $form->reset();
                        } catch(Exception $e) {
                            $erro = strstr($e->getMessage(),"Duplicate") ?
                                    "Já existe um registro semelhante, escolha outro login." :
                                    $e->getMessage();
                            $this->messenger->addMessage($erro,'erro');
                        }
                    } else {
                        $this->messenger->addMessage("Preencha todos os dados corretamente.",'erro');
                        $form->populate($post);
                    }
                    break;
                case "edit":
                    if($form->isValid($post)){
                        try {
                            $line = $table->fetchRow("id=".(int)$this->_getParam('id'));
                            
                            $line->nome      = utf8_decode($post['nome']);
                            $line->login     = utf8_decode($post['login']);
                            $line->email     = utf8_decode($post['email']);
                            $line->status_id = $post['status_id'];
                            $line->data_edit = date("Y-m-d H:i:s");
                            $line->user_edit = $this->login->user->id;
                            
                            if($post['senha'] != ''){
                                $line->senha = md5($post['senha']);
                            }
                            
                            $line->save();
                            $this->messenger->addMessage("Registro alterado com sucesso!","message");
                            $form->populate($post);
                            //$this->_setParam('id',null);
                        } catch(Exception $e) {
                            $erro = strstr($e->getMessage(),"Duplicate") ?
                                    "Já existe um registro semelhante, escolha outro login." :
                                    $e->getMessage();
                            $this->messenger->addMessage($erro,'erro');
                        }
                    } else {
                        $this->messenger->addMessage("Preencha todos os dados corretamente.",'erro');
                        $form->populate($post);
                    }
                    break;
            }
            
            $lines = $post['action']=="search" ?
                     $table->fetchAll($post['search-by']." like '%".utf8_decode($post['search-txt'])."%'",'nome') :
                     $table->fetchAll(null,'nome');
        } else {
            $lines = $table->fetchAll(null,'nome');
        }
        
        if($this->_hasParam('id')){
            //$form->getElement('submit')->setLabel("Alterar");
            $data = $table->fetchRow("id=".(int)$this->_getParam('id'));
            //Is_Var::dump(Is_Array::utf8DbRow($data));
            $data = Is_Array::utf8DbRow($data);
            $form->populate((array)$data);
        }
        
        $this->view->lines  = Is_Array::utf8DbResult($lines);
        $this->view->form   = $form;
        $this->view->action = $this->_hasParam('id') ? "edit" : "new";
    }
    
    public function usuariosDelAction(){
        $id = $this->_getParam("id");
        $table = new Application_Model_Db_Usuario();
        
        try {
            $table->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function postDispatch()
    {
        $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}

