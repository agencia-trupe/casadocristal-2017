<?php

class Admin_MaillingListController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->titulo = "MAILLING LIST";
        $this->view->section = $this->section = "mailling-list";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
        
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../..".FILE_PATH."/".$this->section;

        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
    }

    public function indexAction()
    {
        $table = new Application_Model_Db_Mailling();
        
        if($this->_request->isPost()){
            $post = $this->_request->getPost();
            $post['search-txt'] = Is_Date::br2am($post['search-txt']);
            
            $lines = $table->fetchAll($post['search-by']." like '%".utf8_decode($post['search-txt'])."%'",
                                      $post['search-by'],20,0);
        } else {
            $lines = $table->fetchAll(null,array("data_cad desc"));
        }
        
        $this->view->lines = Is_Array::utf8DbResult($lines);
    }
}

