<?php

class Admin_AcompanhamentoController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        // if(APPLICATION_ENV!='development') $this->_redirect('admin/');
        Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "ACOMPANHAMENTO";
        $this->view->section = $this->section = "acompanhamento";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../..".FILE_PATH."/".$this->section;
        
        // models
        //$this->carrinho = new Application_Model_Carrinho();
        $this->pedidos = new Application_Model_Orcamentos();
        $this->pedidos_db = new Application_Model_Db_Orcamentos();
        $this->pedidos_status = new Application_Model_Db_OrcamentoStatus();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
    }

    public function indexAction()
    {
        // action body
    }
    
    public function pedidosAction()
    {
        $this->view->titulo.= " &rarr; PEDIDOS DE ORÇAMENTO";
        
        /* paginação */
        $records_per_page   = 10;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            if($post['search-by'] == 'cliente_cpf'){
                $post['search-txt'] = Is_Cpf::clean($post['search-txt']);
            }
            
            $where = $post['search-by'].
                    ($post['search-by']=='pedido_id' || $post['search-by']=='pedido_status_id' ?
                     (trim($post['search-txt']) == "" ? " like '%' " : " = ".$post['search-txt']) :
                     " like '%".utf8_decode(str_replace(" ","%",$post['search-txt']))."%'");
            $rows = $this->pedidos->getAll($where,"data_cad desc",$limit,$offset);
            
            $total = $this->view->total = null;
        } else if($this->_hasParam('id')) {
            $where = 'id='.$this->_getParam('id');
            $rows = $this->pedidos->getAll($where,"data_cad desc",$limit,$offset);
            $total = $this->view->total = $this->pedidos->count();
        } else {
            $rows = $this->pedidos->getAll(null,"data_cad desc",$limit,$offset);
            $total = $this->view->total = $this->pedidos->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->pedidos_status = ($this->pedidos_status->getKeyValues());
        $this->view->paginacao = $pagination;
        $this->view->rows = $rows;
    }
    
    public function pedidosSalvarAction()
    {
        if(!$this->_hasParam('id')){
            return array('error'=>'Id inválido');
        }

        $get = $this->_request->getParams();
        
        try {
            // return !$this->pedidos->updateStatus($get['id'],$get['status'],@$get['rastreio'],@$get['rastreio_mail']) ?
            return !$this->pedidos->updateStatus($get['id'],$get['status'],$get) ?
                    array('error'=>'Não foi possível salvar o registro') :
                    array('msg'=>'Registro salvo');
        } catch(Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function pedidosDelAction()
    {
        if(!$this->_hasParam('id')){
            return array('error'=>'Id inválido');
        }
        
        return !$this->pedidos->delete($this->_getParam('id')) ?
                array('error'=>'Não foi possível excluir o registro') :
                array('msg'=>'Registro excluído com sucesso');
    }

}

