<?php

class Admin_PortfolioController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        // Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "PORTFOLIO";
        $this->view->section = $this->section = "portfolio";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/..".FILE_PATH."/".$this->section;
        
        $this->view->MAX_FOTOS = 1;
        
        // models
        $this->portfolio = new Application_Model_Db_Portfolio();
        $this->tags = new Application_Model_Db_Tags();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();

        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 20;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where = $post['search-by']." like '%".utf8_decode(str_replace(" ","%",$post['search-txt']))."%'";
            $rows = $this->portfolio->fetchAll($where,$post['search-by'],$limit,$offset);
            
            $total = $this->view->total = $this->portfolio->count($where);
        } else {
            $rows = $this->portfolio->fetchAll(null,"data desc",$limit,$offset);
            $total = $this->view->total = $this->portfolio->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; NOVO");
        $form = new Admin_Form_Portfolio();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $this->view->id = $this->portfolio_id = $data['id'];
            $data['data'] = Is_Date::am2br($data['data']);
            $data['body'] = stripslashes($data['body']);
            $data['creditos'] = stripslashes($data['creditos']);
            
            $form->addElement('hidden','id');
            $this->view->fotos = $this->fotosAction();
            $this->view->sugestoes = $this->sugestoesAction();
            $this->view->sugestoes_list = $this->portfolio->getSugestoes($data['id']);
            $this->view->tags = $this->tagsAction();
            $this->view->tags_list = $this->portfolio->getTags($data['id']);
            if(isset($data['info'])) unset($data['info']);
        } else {
            $form->removeElement('body');
            $form->removeElement('creditos');
            $form->removeElement('categoria_id');
            $form->removeElement('estilo_id');
            $data = array('status_id'=>'1');
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/'.$this->section.'/new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->portfolio->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $post = $this->_request->getParams();
            $data = array_map('utf8_decode',$post);
            $data['data']        = Is_Date::br2am($data['data']);
            $data['alias']       = Is_Str::toUrl($post['titulo']);
            $data['user_edit']   = $this->login->user->id;
            $data['data_edit']   = date("Y-m-d H:i:s");
            
            if(!$row){
                $data['user_cad']  = $this->login->user->id;
                $data['data_cad']  = date("Y-m-d H:i:s");
            }
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->portfolio->update($data,'id='.$id) : $id = $this->portfolio->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $data = Is_Array::utf8All($data);
            $data['valor'] = str_replace('.',',',$data['valor']);
            $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>$data));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ?
                     'Já existe um portfolio com o mesmo título, escolha um diferente.' :
                     $e->getMessage();
            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->portfolio->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('denied','error','default',array('url'=>URL.'/admin/'.$this->section.'/'));return false; }
        
        $data = Is_Array::utf8All($row->toArray());
        //Is_Var::dump($data);
        $this->_forward('new',null,null,array('data'=>$data));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        $table = new Application_Model_Db_Portfolio();
        
        try {
            $table->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('portfolio_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f2.id asc');
        
        if(isset($this->portfolio_id)){
            $select->where('f2.portfolio_id = ?',$this->portfolio_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            $fotos->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        //echo $this->img_path;exit();
        $max_size = '5120'; // '2048'
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/'.$this->section.'/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size.'kb'))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.Is_File::formatBytes($max_size).'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $produtos_fotos = new Application_Model_Db_PortfolioFotos();
            $produto_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $produtos_fotos->insert(array("foto_id"=>$foto_id,"portfolio_id"=>$produto_id));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function saveDescricaoAction()
    {
        if(!$this->_hasParam('portfolio_id') ||
           !$this->_hasParam('foto_id') ||
           !$this->_hasParam('descricao')) {
            return array('error'=>'Acesso negado');
        }
        
        $f = new Application_Model_Db_Fotos();
        
        try{
            $f->update(
                array('descricao'=>$this->_getParam('descricao')),
                'id='.$this->_getParam('foto_id')
            );
            
            return array('msg'=>'Salvo.');
        } catch(Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function saveCapaAction()
    {
        if(!$this->_hasParam('portfolio_id') ||
           !$this->_hasParam('foto_id') ||
           !$this->_hasParam('flag')) {
            return array('error'=>'Acesso negado');
        }
        
        $pid = $this->_getParam('portfolio_id');
        $fid = $this->_getParam('foto_id');
        $flag = $this->_getParam('flag');
        
        $f = new Application_Model_Db_Fotos();
        $pf = new Application_Model_Db_PortfolioFotos();
        $p = new Application_Model_Db_Portfolio();
        
        try{
            if($flag==1){
                $pfs = $pf->fetchAll('portfolio_id='.$pid);
                
                if(count($pfs)){
                    $ids = array();
                    foreach($pfs as $pf) $ids[] = $pf->foto_id;
                    $f->update(array('flag'=>0),'id in ('.implode(',',$ids).')');
                }
            }
            
            $p->update(array('capa_id'=>((bool)$flag?$fid:null)),'id='.$pid);
            $f->update(array('flag'=>$flag),'id='.$fid);
            
            return array('msg'=>'Salvo.');
        } catch(Exception $e){
            return array('error'=>$e->getMessage());
        }
    }

    public function sugestoesAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('portfolio')/*->where('categoria_id=30')*/->order('titulo')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }

    public function sugestoesAddAction()
    {
        if($this->_hasParam('portfolio_id') && $this->_hasParam('sugestao_id')){
            $produtos_sugestoes = new Application_Model_Db_PortfolioSugestoes();
            try{
                $produtos_sugestoes->insert(array(
                    'portfolio_id'=>$this->_getParam('portfolio_id'),
                    'sugestao_id'=>$this->_getParam('sugestao_id')
                ));
                return array();
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"Duplicate") ? "Registro duplicado." : $e->getMessage();
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar sugestão.');
        }
    }
    
    public function sugestoesDelAction()
    {
        if($this->_hasParam('portfolio_id') && $this->_hasParam('sugestao_id')){
            $produtos_sugestoes = new Application_Model_Db_PortfolioSugestoes();
            $produtos_sugestoes->delete('portfolio_id="'.$this->_getParam('portfolio_id').'"'.
                                        'and sugestao_id="'.$this->_getParam('sugestao_id').'"');
            return array();
        } else {
            return array('error'=>'Não foi possível remover sugestão.');
        }
    }

    public function tagsAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('tags')/*->where('categoria_id=30')*/->order('alias')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }

    public function tagsAddAction()
    {
        if($this->_hasParam('portfolio_id')){
            $produtos_sugestoes = new Application_Model_Db_PortfolioTags();
            try{
                $tag_id = $this->_getParam('tag_id');

                if($this->_hasParam('tag_tag')){
                    $tag_tag = trim($this->_getParam('tag_tag'));
                    $tag_alias = Is_Str::toUrl(trim($tag_tag));

                    if($rowTag = $this->tags->fetchRow('alias="'.$tag_alias.'"')){
                        $tag_id = $rowTag->id;
                    } else {
                        $tag_data = array(
                            'tag' => Is_Str::toLower(utf8_decode($tag_tag)),
                            'alias' => $tag_alias
                        );
                        $tag_id = $this->tags->insert($tag_data);
                    }

                }

                $produtos_sugestoes->insert(array(
                    'portfolio_id'=>$this->_getParam('portfolio_id'),
                    'tag_id'=>$tag_id
                ));
                return array();
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"Duplicate") ? "Registro duplicado." : $e->getMessage();
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar tag.');
        }
    }
    
    public function tagsDelAction()
    {
        if($this->_hasParam('portfolio_id') && $this->_hasParam('tag_id')){
            $produtos_sugestoes = new Application_Model_Db_PortfolioTags();
            $produtos_sugestoes->delete('portfolio_id="'.$this->_getParam('portfolio_id').'"'.
                                        'and tag_id="'.$this->_getParam('tag_id').'"');
            return array();
        } else {
            return array('error'=>'Não foi possível remover tag.');
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }
}
