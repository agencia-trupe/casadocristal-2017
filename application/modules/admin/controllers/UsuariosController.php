<?php

class Admin_UsuariosController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        $this->view->titulo = "USUÁRIOS";
        $this->view->section = $this->section = "usuarios";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path = $this->view->img_path = APPLICATION_PATH."/..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/..".FILE_PATH."/".$this->section;
        
        $this->view->MAX_FOTOS = 1;

        // models
        $this->usuarios = $this->view->usuarios = new Application_Model_Db_Usuario();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();

        //$this->escolaridade = new Application_Model_Db_Escolaridade();
        //$this->atividade_profissional = new Application_Model_Db_AtividadeProfissional();

        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
    }

    public function indexAction()
    {
        $where = 'role in (1,2,3,4)';
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $rows = $post['search-action']=="search" ?
                    $this->usuarios->fetchAll($where.' and '.$post['search-by']." like '%".utf8_decode($post['search-txt'])."%'",'nome') :
                    $this->usuarios->fetchAll($where,'nome');
        } else {
            $rows = $this->usuarios->fetchAll($where,'nome');
        }
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; NOVO");
        $form = new Admin_Form_Usuarios();
        
        if($this->_hasParam('data')){
            $data = $this->view->data = $this->_getParam('data');
            $this->view->id = $this->usuario_id = $data['id'];
            $this->view->edit = true;
            $data['data_nascimento'] = Is_Date::am2br($data['data_nascimento']);
            $form->addElement('hidden','id');

            $this->view->id = $this->usuario_id = $data['id'];
            $this->view->fotos = $this->fotosAction();

            if((bool)$data['uf']){
                $cidade = $form->getElement('cidade');
                $cidade->setOptions(array('multiOptions'=>Is_F::getCidades($data['uf'])));
            }

        } else {
            $data = array('status_id'=>'1');
            $data['uf'] = 'SP';

            if($this->_hasParam('role')) $data['role'] = $this->_getParam('role');
        }
        
        $form->populate($data);
        $this->view->form = $form;

        //$this->view->escolaridade = Is_Array::utf8DbResult($this->escolaridade->fetchAll(null,array('parent_id','ordem')));
        //$this->view->atividades = Is_Array::utf8DbResult($this->atividade_profissional->fetchAll(null,array('parent_id','ordem')));
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->usuarios->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $post = $this->_request->getParams();

            if(isset($post['atividade_profissional_outros']) && trim($post['atividade_profissional_outros'])=='') unset($post['atividade_profissional_outros']);
            if(isset($post['escolaridade_outros']) && trim($post['escolaridade_outros'])=='') unset($post['escolaridade_outros']);
            if(isset($post['escolaridades'])) unset($post['escolaridades']);
            if(isset($post['atividades'])) unset($post['atividades']);
            // _d($post);

            $data = array_map('utf8_decode',$post);
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            //$data['data_nascimento'] = Is_Date::br2am($data['data_nascimento']);
            //$data['tel'] = Is_Cpf::clean($data['tel']);
            
            if(($row && $data['senha'] != '') || !$row){
                $data['senha'] = md5($data['senha']);
            } else {
                unset($data['senha']);
            }
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->usuarios->update($data,'id='.$id) : $id = $this->usuarios->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe um usuário com os dados enviados. Por favor, escolha um novo.' : $e->getMessage();
            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->usuarios->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->usuarios->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }


    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('usuarios_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f2.id asc');
        
        if(isset($this->usuario_id)){
            $select->where('f2.usuario_id = ?',$this->usuario_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            $fotos->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        //echo $this->img_path;exit();
        $max_size = '5120'; // '2048'
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/usuarios/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size.'kb'))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.Is_File::formatBytes($max_size).'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $produtos_fotos = new Application_Model_Db_UsuariosFotos();
            $produto_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $produtos_fotos->insert(array("foto_id"=>$foto_id,"usuario_id"=>$produto_id));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}