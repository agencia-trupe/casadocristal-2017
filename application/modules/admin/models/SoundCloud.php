<?
class Admin_Model_SoundCloud
{
	private $client_id;
	private $client_secret;
	private $redirect_url;
	private $session;
	public $client;
	public $me;
	public $tracks;

	/**
	 * @param string $client_id     - CLIENT_ID do SoundCloud
	 * @param string $client_secret - CLIENT_SECRET do SoundCloud
	 * @param string $redirect_url  - REDIRECT_URL do SoundCloud
	 */
	public function __construct($client_id=null,$client_secret=null,$redirect_url=null)
	{
		if($client_id===null || $client_secret===null || $redirect_url===null)
			throw new Exception('No parameters to create connection');

		$this->client_id     = $client_id;
		$this->client_secret = $client_secret;
		$this->redirect_url  = $redirect_url;
		$this->session       = new Zend_Session_Namespace(SITE_NAME.'_soundcloud');

		$this->createClient();
	}

	/**
	 * @return object - cliente da api soundcloud
	 */
	public function getClient()
	{
		return $this->client;
	}

	/**
	 * Seta cliente da api do SoundCloud
	 */
	public function createClient()
	{
		require_once('Services/Soundcloud/Exception.php');
        require_once('Services/Soundcloud/Version.php');
        require_once('Services/Soundcloud.php');

		$this->client = new Services_Soundcloud($this->client_id,$this->client_secret,$this->redirect_url);
		$this->client->setDevelopment(false);
		return $this->client;
	}

	/**
	 * Conecta ao SoundCloud e seta o token na Session
	 * 
	 * @return mixed - string - caso não seja um retorno da api, retorna url para conexão
	 *               - bool   - false caso consiga registrar o token
	 */
	public function connect()
	{
		// if($this->hasToken() && $this->needRefreshToken()) return $this->needRefreshToken();

		if(!isset($_GET['code'])){
			$authUrl = $this->client->getAuthorizeUrl();
			return $authUrl;
		}
		
		$accessToken = $this->client->accessToken($_GET['code']);
		$this->session->token = $accessToken['access_token'];
		$this->client->setAccessToken($this->session->token);

		try {
			$me = json_decode($this->client->get('me'));
        	$tracks = json_decode($this->client->get('users/'.$me->id.'/tracks'));

        	$this->me = $me;
        	$this->tracks = $tracks;
        	$this->session->me = json_encode($me);
        	$this->session->tracks = json_encode($tracks);

			return false;
		} catch(Services_Soundcloud_Invalid_Http_Response_Code_Exception $e){
			$this->me = null;
        	$this->tracks = null;
        	$this->session->me = null;
        	$this->session->tracks = null;

			return false;
		}
	}

	/**
	 * Checa se tem Token
	 */
	public function hasToken()
	{
		return (bool)$this->session->token;
	}

	/**
	 * Checa se precisa atualizar o token
	 * 
	 * @return mixed - string - caso não consiga conectar com a api, retorna url para conexão
	 *               - bool   - false caso token seja válido
	 */
	public function needRefreshToken()
	{
		try {
			$me = json_decode($this->client->get('me'));
			return false;
		} catch(Services_Soundcloud_Invalid_Http_Response_Code_Exception $e){
			$this->session->token = null;
			return $this->client->getAuthorizeUrl();
		}

	}

	/**
	 * get 'me'
	 */
	public function me()
	{
		return $this->session->me ? json_decode($this->session->me) : null;
	}

	/**
	 * get 'tracks'
	 */
	public function tracks()
	{
		return $this->session->tracks ? json_decode($this->session->tracks) : null;
	}

	public function upload($path,$title,$description=null)
	{
		$post = array(
		    'track[asset_data]' => '@'.$path,
		    'track[title]' => $title
		);

		if($description) $post['description'] = $description;

		try {
			$track = json_decode($this->client->post('tracks',$post));
			return $track;
		} catch(Services_Soundcloud_Invalid_Http_Response_Code_Exception $e){
			return false;
		}
	}

	public function update($id,$data=array())
	{
		$post = array();

		foreach ($data as $k => $v) $post['track['.$k.']'] = $v;

		try {
			$track = json_decode($this->client->put('tracks/'.$id,$post));
			return $track;
		} catch(Services_Soundcloud_Invalid_Http_Response_Code_Exception $e){
			return false;
		}
	}

	public function delete($id)
	{
		try {
			$track = json_decode($this->client->delete('tracks/'.$id));
			return $track;
		} catch(Services_Soundcloud_Invalid_Http_Response_Code_Exception $e){
			return false;
		}
	}

	public function resolveUrl($url)
	{
		$ch_url = 'http://api.soundcloud.com/resolve.json'.
				  '?url='.$url.
				  '&client_id='.SOUNDCLOUD_CLIENT_ID;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $ch_url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FILETIME, true);
		$output = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);

		return (int)$info['http_code']==200 ? 
				json_decode($output) :
				null;
	}

	public function getIdFromUrl($url)
	{
		$r = self::resolveUrl($url);
		return $r ? $r->id : null;
	}
}