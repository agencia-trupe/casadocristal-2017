<?php

class Admin_Form_LocalEntrega extends ZendPlugin_Form
{
    public function init()
    {
        $this->setMethod('post')->setAction(URL.'/admin/locais-de-entrega/save')->setAttrib('id','frm-usuarios')->setAttrib('name','frm-usuarios');
		
		$this->addElement('text','descricao',array('label'=>'Descrição','class'=>'txt'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo','class'=>'bt-left status','checked'=>true));
        
        $this->removeDecs();
    }
}