<?php

class Admin_Form_Home extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/home/save')
             ->setAttrib('id','frm-home')
             ->setAttrib('name','frm-home');
        
        // elementos
        // $this->addElement('textarea','titulo',array('label'=>'Título:','class'=>'txt'));
        $this->addElement('textarea','texto1',array('label'=>'Texto:','class'=>'txt'));
        $this->addElement('text','texto2',array('label'=>'Vídeo URL:','class'=>'txt'));
        // $this->addElement('textarea','texto2',array('label'=>'Texto 2:','class'=>'txt'));
        
        // atributos
        // $this->getElement('titulo')->setAttrib('rows',15);
        
        // filtros / validações
        // $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}