<?php

class Admin_Form_Clipping extends ZendPlugin_Form
{
    public function init()
    {
        // configurações
        $this->setMethod('post')->setAction(URL.'/admin/clipping/save')
             ->setAttrib('id','frm-clipping')
             ->setAttrib('name','frm-clipping');
        
        // elementos
		$this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
		$this->addElement('text','publicacao',array('label'=>'Publicação','class'=>'txt mask-date'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        //$this->getElement('descricao')->setAttrib('rows',1)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

