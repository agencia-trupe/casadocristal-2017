<?php

class Admin_Form_Portfolio extends ZendPlugin_Form
{
    public function init()
    {
		$section = 'portfolio';

        $categs = new Application_Model_Db_Categorias();
        $styles = new Application_Model_Db_Estilos();
		
        // configurações
        $this->setMethod('post')->setAction(URL.'/admin/'.$section.'/save')
             ->setAttrib('id','frm-'.$section.'')
             ->setAttrib('name','frm-'.$section.'');
		
        // elementos
        $this->addElement('text','cliente',array('label'=>'Cliente','class'=>'txt'));
        $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        $this->addElement('select','categoria_id',array('label'=>'Categoria','class'=>'txt','multiOptions'=>$categs->getKeyValues()));
        $this->addElement('select','estilo_id',array('label'=>'Estilo','class'=>'txt','multiOptions'=>$styles->getKeyValues()));
        $this->addElement('text','data',array('label'=>'Data','class'=>'txt mask-date'));
		// $this->addElement('text','url',array('label'=>'URL','class'=>'txt'));
        $this->addElement('textarea','body',array('label'=>'Descrição','class'=>'txt'));
        $this->addElement('textarea','creditos',array('label'=>'Créditos','class'=>'txt'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        //$this->getElement('descricao')->setAttrib('rows',1)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

