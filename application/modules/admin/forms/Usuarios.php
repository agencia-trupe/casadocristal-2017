<?php

class Admin_Form_Usuarios extends ZendPlugin_Form
{
    public function init()
    {
        $users = new Application_Model_Db_Usuario();

        $this->setMethod('post')->setAction(URL.'/admin/usuarios/save')->setAttrib('id','frm-usuarios')->setAttrib('name','frm-usuarios');
		
		$this->addElement('text','nome',array('label'=>'Nome','class'=>'txt bt-left'));
		$this->addElement('text','email',array('label'=>'E-mail','class'=>'txt bt-left status'));
		$this->addElement('text','login',array('label'=>'Login','class'=>'txt bt-left'));
		$this->addElement('password','senha',array('label'=>'Senha','class'=>'txt bt-left status'));
        // $this->addElement('select','role',array('label'=>'Tipo','class'=>'txt','multiOptions'=>$users->roles));
        $this->addElement('hidden','role',array('value'=>1));

        // dados de usuários
        //$this->addElement('text','pais',array('label'=>'País','class'=>'txt data-user'));
        //$this->addElement('select','uf',array('label'=>'UF','class'=>'txt data-user','multiOptions'=>Is_F::getEstados()));
        //$this->addElement('select','cidade',array('label'=>'Cidade','class'=>'txt data-user'));
        //$this->addElement('text','tel',array('label'=>'Telefone','class'=>'txt data-user mask-tel'));
        //$this->addElement('text','endereco',array('label'=>'Endereço','class'=>'txt data-user'));
        //$this->addElement('text','data_nascimento',array('label'=>'Nascimento','class'=>'txt mask-date data-user'));
        //$this->addElement('select','sexo',array('label'=>'Sexo','class'=>'txt data-user','multiOptions'=>array('0'=>'Feminino','1'=>'Masculino')));
        //$this->addElement('select','filhos',array('label'=>'Filhos','class'=>'txt data-user','multiOptions'=>array('0'=>'Não','1'=>'Sim')));
        //$this->addElement('select','contato_freq_criancas',array('label'=>'Contato c/ crianças','class'=>'txt data-user','multiOptions'=>array('0'=>'Não','1'=>'Sim')));
        //$this->addElement('hidden','escolaridade_id');
        //$this->addElement('hidden','atividade_profissional_id');
        //$this->addElement('textarea','interesse',array('label'=>'Interesse','class'=>'txt data-user'));
        //$this->addElement('checkbox','optin_territorio',array('label'=>'Opt-In Território','class'=>'data-user bt-left optin_territorio','checked'=>true));
        //$this->addElement('checkbox','optin_alana',array('label'=>'Opt-In Alana','class'=>'data-user bt-left optin_alana','checked'=>true));

        $this->addElement('checkbox','status_id',array('label'=>'Ativo','class'=>'bt-left status','checked'=>true));
        
        $this->getElement('nome')->setRequired();
        $this->getElement('email')->setRequired()->addValidator('EmailAddress');
        $this->getElement('login')->setRequired();
        
        $this->removeDecs();
    }
}